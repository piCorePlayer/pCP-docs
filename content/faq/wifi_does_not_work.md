---
title: Wifi does not work
description: null
date: 2024-05-06T11:38:26.900Z
lastmod: 2024-05-06T11:38:34.607Z
author: pCP Team
weight: 10
pcpver: 6.1.0
draft: false
categories:
  - FAQ
tags:
  - Wifi
---

## A list of common Wifi issues

- Wrong password.
- Wrong SSID.
- Wifi adapter does not support Wifi channel selected on Wifi router.
- USB Wifi adapter and built-in Wifi adapter both trying to run at the same time.
- Some audio cards interfere with Raspberry Pi built-in Wifi adapters.
- Wifi drivers are gradually changing from "wext" to "nl80211" standard.
- Kernel upgrades can break USB Wifi drivers. Unfortunately, we can not test all USB Wifi drivers prior to each piCorePlayer release.

{{< card style="warning" >}}Please report any issue.{{< /card >}}

## Diagnostics

- Check the Wifi MAC and IP addresses have been set by looking at [Wifi settings] > "Wifi information". For example:
```
	Wifi MAC: dc:a6:32:00:2a:4d
	Wifi IP: 192.168.1.110
```
- Check wifi diagnostics information by clicking on [Wifi settings] > "Set Wifi configuration" > [Dignostics].
- Check the boot log by clicking on [Main Page] > [Diagnostics] > [Logs] > "Boot" .

Extract from the boot log:

```text
Starting WiFi on wlan0...
udhcpc: started, v1.33.0
udhcpc: sending discover
udhcpc: sending discover
udhcpc: sending discover
udhcpc: sending select for 192.168.1.104
udhcpc: lease of 192.168.1.104 obtained, lease time 86400
deleting routers
route: SIOCDELRT: No such process
adding dns 192.168.1.50
adding dns 0.0.0.0
Starting wifi... Done.
```

You can see from the above that an IP address has successfully been obtained.

Sometimes the udhcpc discovery process is forked to the background. This will cause a delay in the "awaiting for network" in the boot process.

```text
Starting WiFi on wlan0...
udhcpc: started, v1.33.0
udhcpc: sending discover
udhcpc: sending discover
udhcpc: sending discover
udhcpc: no lease, forking to background
Starting WiFi... Done.
```

##### Note:

- multiple **udhcpc: sending discover** indicates DHCP is looking for an IP address
- **udhcpc: lease of 192.168.1.104 obtained, lease time 86400** indicates DHCP has allocated an IP address
- **adding dns 192.168.1.50** indicates a dns server has been set


## More information

- [Setup Wifi](/how-to/setup_wifi/)
- [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/)
- [Wifi does not work](/faq/wifi_does_not_work/)
- [Example wpa_supplicant configuration file](http://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf)
- [WPA Supplicant](http://w1.fi/wpa_supplicant/)
- [The Short List - Superstar USB WiFi Adapters for Linux](https://github.com/morrownr/USB-WiFi/blob/main/home/The_Short_List.md)
- [Utility for compiling out of tree kernel modules for pCP Kernels](https://github.com/piCorePlayer/pCP-Kernels)
