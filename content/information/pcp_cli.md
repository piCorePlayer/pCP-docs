---
title: piCorePlayer CLI
description: piCorePlayer Command Line Interface (CLI)
date: 2023-06-29T14:07:01.304Z
lastmod: 2023-06-29T14:07:04.105Z
author: pCP Team
weight: 20
pcpver: 9.0.0
toc: true
draft: false
tags:
  - CLI
---

{{< lead >}}
We have implemented a basic piCorePlayer Command Line Interface (CLI). These commands access some Squeezelite, LMS, piCore or piCorePlayer functions and can be used directly on the command line or used in scripts to enhance your system.
{{< /lead >}}

## Basic piCorePlayer CLI

$ `pcp help`
```
=========================================================================================
 Basic piCorePlayer CLI
-----------------------------------------------------------------------------------------
 Squeezelite/LMS
 ---------------
 - pcp play           : play current track in playlist
 - pcp stop           : stop current track
 - pcp pause          : pause current track
 - pcp up             : volume up
 - pcp down           : volume down
 - pcp next           : next track
 - pcp prev           : previous track
 - pcp rand           : generate random playlist
 - pcp power [on|off] : software power on or off
 - pcp volume [0-100] : set volume between 0 to 100
 - pcp rescan         : look for new and changed media files in connected LMS library
 - pcp wipecache      : clear connected LMS library and rescan
 - pcp mode           : display Squeezelite's current mode
-----------------------------------------------------------------------------------------
 piCore
 ------
 - pcp bu             : (b)ack(u)p
 - pcp sd             : (s)hut(d)own
 - pcp bs             : (b)ackup then (s)hutdown
 - pcp rb             : (r)e(b)oot
 - pcp br             : (b)ackup then (r)eboot
 - pcp leds [on|off]  : turn leds on or off
-----------------------------------------------------------------------------------------
 piCorePlayer
 ------------
 - pcp d0             : debug off
 - pcp d1             : debug on
 - pcp ic [all|pcp]   : (i)mage (c)reate (all) partitions or (pcp) partitions only
-----------------------------------------------------------------------------------------
 Jivelite
 --------
 - pcp kj             : (k)ill (j)ivelite
-----------------------------------------------------------------------------------------
 Squeezelite
 -----------
 - pcp sls            : (s)queeze(l)ite (s)tart
 - pcp slk            : (s)queeze(l)ite (k)ill
 - pcp slr            : (s)queeze(l)ite (r)estart
 - pcp slf            : (s)queeze(l)ite (f)orce
 - pcp sl             : (s)queeze(l)ite status
-----------------------------------------------------------------------------------------
```


## Example

```
tc@piCorePlayer:~$ pcp bu
[ INFO ] Backing up files to /mnt/mmcblk0p2/tce/mydata.tgz
Done.
[ OK ] Backup successful.
tc@piCorePlayer:~$
```


## More information

- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [piCorePlayer aliases](/information/pcp_aliases/)
