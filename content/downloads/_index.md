---
title: Downloads
description: piCorePlayer Downloads
date: 2024-06-07T10:19:40.138Z
lastmod: 2025-02-01T10:20:02.196Z
author: pCP Team
weight: 70
pcpver: 10.0.0
toc: false
draft: false
tags:
  - Download
download:
  - pcpversion: 10.0.0
    pcpdate: 2025-02-01
    url: false
    bit64: true
    realtime: false
    sha256: af9cbe27f0371ddbb6d9d2883afa39596161a68d9bbaec12619746b5e7619dc9
    sha25664: 51145a74e5c7b9c6d439f3c72e5b2161edbb99f9c77251b708a36909b90f5e3d
  - pcpversion: 9.2.0
    pcpdate: 2024-06-07
    url: false
    bit64: true
    realtime: false
    sha256: 2457f38062560beac5ee166ec6cd96335201b8f6bc1f47b026cd8d639c804bc9
    sha25664: dcaa8f78283e1e2133c92a0d92fd64b7a6177471048959406b8338e2c248ad9c
  - pcpversion: 9.0.1
    pcpdate: 2024-05-08
    url: false
    bit64: true
    realtime: false
  - pcpversion: 9.0.0
    pcpdate: 2024-03-22
    url: false
    bit64: true
    realtime: false
  - pcpversion: 8.2.0
    pcpdate: 2022-06-05
    url: false
    bit64: true
    realtime: false
  - pcpversion: 8.1.0
    pcpdate: 2021-12-05
    url: false
    bit64: true
    realtime: false
  - pcpversion: 8.0.0
    pcpdate: 2021-07-10
    url: false
    bit64: true
    realtime: false
  - pcpversion: 7.0.0
    pcpdate: 2020-12-23
    url: false
    bit64: true
    realtime: false
  - pcpversion: 6.1.0
    pcpdate: 2020-06-06
    url: false
    bit64: false
    realtime: true
  - pcpversion: 6.0.0
    pcpdate: 2020-03-07
    url: false
    realtime: true
  - pcpversion: 5.0.0
    pcpdate: 2019-05-26
    url: false
    realtime: true
  - pcpversion: 4.1.0
    pcpdate: 2018-11-24
    url: false
    realtime: true
  - pcpversion: 4.0.0
    pcpdate: 2018-09-01
    url: false
    realtime: true
  - pcpversion: 3.5.0
    pcpdate: 2018-03-24
    url: false
    realtime: true
  - pcpversion: "3.22"
    pcpdate: 2017-09-23
    url: false
    realtime: true
  - pcpversion: "3.21"
    pcpdate: 2017-07-19
    url: false
    realtime: true
  - pcpversion: "3.20"
    pcpdate: 2017-05-01
    url: false
    realtime: true
  - pcpversion: "3.11"
    pcpdate: 2017-02-08
    url: false
    realtime: true
  - pcpversion: "3.10"
    pcpdate: 2017-01-07
    url: false
    realtime: true
  - pcpversion: "3.02"
    pcpdate: 2016-09-25
    url: false
    realtime: false
  - pcpversion: "3.01"
    pcpdate: 2016-08-29
    url: img
    realtime: false
  - pcpversion: "3.00"
    pcpdate: 2016-08-14
    url: img
    realtime: false
  - pcpversion: "2.06"
    pcpdate: 2016-06-18
    url: img
    realtime: false
  - pcpversion: "2.05"
    pcpdate: 2016-05-06
    url: img
    realtime: false
  - pcpversion: "2.04"
    pcpdate: 2016-03-28
    url: img
    realtime: false
  - pcpversion: "2.03"
    pcpdate: 2016-03-19
    url: img
    realtime: false
  - pcpversion: "2.02"
    pcpdate: 2016-02-29
    url: img
    realtime: false
  - pcpversion: "2.01"
    pcpdate: 2016-02-06
    url: img
    realtime: false
  - pcpversion: "2.00"
    pcpdate: 2016-01-14
    url: img
    realtime: false
  - pcpversion: 1.22_RPi2
    pcpdate: 2015-12-12
    url: img
    realtime: false
  - pcpversion: 1.22_RPi
    pcpdate: 2015-12-12
    url: img
    realtime: false
  - pcpversion: 1.21g_RPi2
    pcpdate: 2015-10-31
    url: img
    realtime: false
  - pcpversion: 1.21g_RPi
    pcpdate: 2015-10-31
    url: img
    realtime: false
  - pcpversion: 1.21b_RPi2
    pcpdate: 2015-09-13
    url: img
    realtime: false
  - pcpversion: 1.21b_RPi
    pcpdate: 2015-09-13
    url: img
    realtime: false
  - pcpversion: 1.21a_RPi2
    pcpdate: 2015-09-10
    url: img
    realtime: false
  - pcpversion: 1.21a_RPi
    pcpdate: 2015-09-10
    url: img
    realtime: false
  - pcpversion: 1.19l
    pcpdate: 2015-05-17
    url: img
    realtime: false
  - pcpversion: 1.19l_RPi2
    pcpdate: 2015-05-17
    url: img
    realtime: false
  - pcpversion: 1.19k
    pcpdate: 2015-05-14
    url: img
    realtime: false
  - pcpversion: 1.19i_RPi2
    pcpdate: 2015-04-15
    url: img
    realtime: false
  - pcpversion: 1.19i
    pcpdate: 2015-04-15
    url: img
    realtime: false
  - pcpversion: 1.19_RPi2
    pcpdate: 2015-03-03
    url: img
    realtime: false
  - pcpversion: "1.19"
    pcpdate: 2015-03-03
    url: img
    realtime: false
  - pcpversion: 1.18b
    pcpdate: 2014-11-02
    url: img
    realtime: false
  - pcpversion: 1.18a
    pcpdate: 2014-10-27
    url: img
    realtime: false
  - pcpversion: "1.18"
    pcpdate: 2014-09-30
    url: img
    realtime: false
  - pcpversion: 1.17a
    pcpdate: 2014-08-24
    url: img
    realtime: false
  - pcpversion: "1.17"
    pcpdate: 2014-08-23
    url: img
    realtime: false
  - pcpversion: "1.16"
    pcpdate: 2014-06-08
    url: img
    realtime: false
  - pcpversion: 1.15ei
    pcpdate: 2014-05-07
    url: https://sourceforge.net/projects/picoreplayer/files/insitu/piCorePlayer1.15ei/piCorePlayer1.15ei.img/download
    realtime: false
  - pcpversion: 1.15bi
    pcpdate: 2014-04-28
    url: https://sourceforge.net/projects/picoreplayer/files/insitu/piCorePlayer1.15bi/piCorePlayer1.15bi.img/download
    realtime: false
---
