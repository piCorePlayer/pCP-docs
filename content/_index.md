---
title: piCorePlayer documentation
description: null
date: 2022-05-28T22:23:44.295Z
lastmod: 2022-09-23T13:04:33.534Z
author: pCP Team
weight: 1
pcpver: 6.1.0
toc: false
draft: false
---

## Welcome

{{< lead >}}
Our first attempt at Online Help was not as successful as expected, but it was a valuable lesson. So let's try again. Documentation is important but if it is a chore it will not get done. So this time, let's focus on simplicity and speed. Remove the barriers and it will happen.
{{< /lead >}}

## piCorePlayer Components

{{% components %}}
