---
title: Getting started
description: null
date: 2024-05-06T11:11:43.801Z
lastmod: 2024-05-07T03:08:11.850Z
author: pCP Team
weight: 10
pcpver: 9.0.0
toc: true
draft: false
categories:
  - Setup
  - How to
---

{{< lead >}}
This "getting started" will show you how to get a basic player up and running on your Raspberry Pi using piCorePlayer.

By default, if you:
- are using a wired ethernet
- are using DHCP
- have a functioning LMS server on your network
- are using a Raspberry Pi with a Headphones jack,

then piCorePlayer will just work through the Headphones jack on the Raspberry Pi without any user setup. piCorePlayer will appear on your Lyrion Media Server (LMS) as a Squeezelite player called `piCorePlayer`. So, plug in your headphones, select a song and press play.
{{< /lead >}}

{{< card style="info" >}}
Some models of the Raspberry Pi do not have a Headphones jack---see [Raspberry Pi - Models](/components/raspberry_pi/#models).
{{< /card >}}


## Steps

##### Step 1 - Download piCorePlayer

- The latest version of piCorePlayer is availiabile directly from the Raspberry pi Imager application---see [Raspberry Pi Imager](https://www.raspberrypi.com/software/)

   or

- Download piCorePlayer---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2 - Create SD card

- Create an SD card with a piCorePlayer image---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).
- Eject the SD card.

##### Step 3 - Boot piCorePlayer

- Insert the SD card into your Raspberry Pi.
- Connect the ethernet cable.
- Connect the power cable.
- Connect a monitor for diagnostics (optional).
- Turn the power on.

##### Step 4 - After boot

A Squeezelite player should appear in LMS named `piCorePlayer`. It can be controlled just like any other Squeezebox player, via:

- Lyrion Media Server (LMS) web interface via any browser (ie. Material skin)
- Software on your smartphone (ie. iPeng)
- Duet Controller
- Squeezebox Radio
- Squeezebox Touch
- piCorePlayer Simple Controls
- piCorePlayer with Jivelite
- Squeezeslave on PC
- etc

##### Step 5 - Test Headphones (optional)

{{< card style="info" >}}
Skip this step if your Raspberry Pi does not have a Headphones jack---see [Raspberry Pi - Models](/components/raspberry_pi/#models).
{{< /card >}}

- Plug in your headphones into the Headphones jack.
- Select a song and press play.

##### Step 6 - Determine the IP address

- Access the piCorePlayer web interface via the IP address shown on the boot screen---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).
- Type `http://your_IP_address` into your browser, or
- Type `http://pcp.local` into your browser where `pcp` is the current hostname.

##### Step 7 - Set system password.

- When the piCorePlayer web interface opens, you will be on a configuration page where you must set the password for the system.
- After setting the password, you can enable ssh if you want.

{{< card style="warning" >}}
Once your password is set, if you forget the password, you must have physical access to the device to reset the password.
{{< /card >}}

{{< card style="info" >}}
Default passwords can create easy access to attackers to create backdoors into your network. Prior to pCP 9.2.0, the images were shipped with a default password. With the current cybersecurity concerns. Starting with version 9.2.0 the default password has been removed from piCorePlayer images.
If you download an image older than 9.2.0, it is recommended that you set the password from the security page of the web interface.
{{< /card >}}

##### Step 8 - Guided Configuration

- You can skip the guided configuratio if you want to set things up manually.
- The current steps of the guided configuration will.
  - Check for critical updates.
  - Setup the Hostname and Squeezlite system name.
  - Setup ssh.
  - Setup ntpservers and ntpd.
  - Select light/dark theme.
  - Show which repo has been selected.
  - Show any autodetected audio devices.
  - Resize your pCP sd card installation.
- Reboot your system.

##### Step 9 - Set Audio output

- Click [Squeezelite Settings].
- Select the appropriate sound device under "Choose audio output" > "Audio output".
- Click [Save].
- Click [Yes] when requested to "Reboot piCorePlayer".

{{< card style="info" >}}
If you selected `USB audio` ensure you complete the "Output setting" field, it will be blank.
{{< /card >}}

##### Step 10 - Set player name (optional)

- Select [Squeezelite Settings] > "Change Squeezelite settings" > "Name of your player".
- Type the player name.
- Click [Save].
- Click [Yes] when requested to "Restart Squeezelite".

##### Step 11 - Set hostname (optional)

- Select [Tweaks] > "pCP System Tweaks" > "Host name".
- Type the host name.
- Click [Save].
- Click [Yes] when requested to "Reboot piCorePlayer".


## More information

- [Raspberry Squeezie](/projects/raspberry-squeezie/)
- [HOWTO: Building and Installing the Raspberry Pi 3 Touch Audio Streamer](https://archimago.blogspot.com/2017/03/howto-building-and-installing-raspberry.html)
- [Raspberry Pi - Models](/components/raspberry_pi/#models)
- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
