---
title: Setup Akkordion
description: Setup Digital Dreamtime Akkordion Music Player
date: 2023-06-09T23:26:47.234Z
lastmod: 2024-06-24T23:31:38.029Z
author: pCP Team
weight: 250
pcpver: 9.0.0-a2
toc: true
draft: false
categories:
  - Projects
  - Setup
tags:
  - Akkordion
---

{{< lead >}}
This project is to install piCorePlayer onto a Digital Dreamtime Akkordion Music Player which has a DAC based on the OEM IQAudIO DAC+ or DAC Zero module.
{{< /lead >}}

{{< lead >}}
This Akkordion is a rare example of an early Raspberry Pi based music streamer. It uses a Raspberry Pi 2B and a pre-HAT specification i2s DAC added to the GPIO port. This means the sound card overlay is not automatically loaded and requires a correctly configured config.txt. This Akkordion is routinely used as a test bed for piCorePlayer, so putting together these few notes will serve as a test procedure and maybe help others setup similar old hardware.
{{< /lead >}}


## What was used

### Hardware

- Akkordion
  - [Raspberry Pi 2B](/components/raspberry_pi/)
  - Generic 26-pin OEM sound card
- SanDisk Ultra 8GB SD card---see [SD card](/components/sd_card/).
- Generic Wifi with arial.

{{< card style="warning" >}}
This i2s sound card was designed before the Raspberry Pi 40-pin HAT standard.
{{< /card >}}

### Software

- piCorePlayer 9.0.0-a2 32-bit Kernel---see [piCorePlayer Downloads](/downloads/).


## Steps

##### Step 1 - Download piCorePlayer

- Download piCorePlayer 32-bit Kernel---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2 - Create SD card

- Create an SD card with piCorePlayer image---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).

##### Step 3 - Setup Wifi (if needed)

- Create wpa_supplicant.conf on boot partition---see [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/).
- Eject the SD card.

##### Step 4 - Boot piCorePlayer

- Insert the SD card into the Raspberry Pi 2B.
- Connect the power cable to the Raspberry Pi 2B.
- Turn the power on.

##### Step 5 - Determine the IP address

- Determine the IP address---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

##### Step 6 - Access piCorePlayer GUI

- Type `http://<IP address>` into web browser, **or**
- Type `http://pcp.local` into web browser.

##### Step 7 - Select Audio output device

- Select [Squeezelite Settings] > "Audio output device settings" > "Generic/simple TI5102 DAC".
- Click [Save].
- This will set "Audio output device settings" to "Generic/simple TI5102 DAC" and set "Change Squeezelite settings" to default values.


{{< card style="warning" >}}
Because the i2s sound card is pre 40-pin HAT, the Raspberry Pi will need to  be rebooted to load appropriate sound card drivers.
{{< /card >}}

##### Step 8 - Reboot

- Click [Reboot] when requested.

##### Step 9 - Check the Output setting

- Click [Squeezelite Settings] > "Change Squeezelite settings" > "Output setting" > "more>".
- Note the list of available `output devices`.

{{< highlight "text" "hl_lines=11 12 13" >}}
Available output devices (click to use):

hw: devices are normally the best choice, but try and decide for yourself:

- default
- plugequal
- equal
- hw:CARD=Headphones,DEV=0
- plughw:CARD=Headphones,DEV=0
- sysdefault:CARD=Headphones
- hw:CARD=sndrpihifiberry,DEV=0
- plughw:CARD=sndrpihifiberry,DEV=0
- sysdefault:CARD=sndrpihifiberry
{{< /highlight >}}


- Check for references to `sndrpihifiberry`.
- The presence of `sndrpihifiberry` shows a compatible overlay has been used in config.txt.
- Go to step 10.
- If the `sndrpihifiberry` sound card is **not** displayed, you will see the following instead.

{{< highlight "text" >}}
Available output devices (click to use):

hw: devices are normally the best choice, but try and decide for yourself:

- default
- plugequal
- equal
- hw:CARD=Headphones,DEV=0
- plughw:CARD=Headphones,DEV=0
- sysdefault:CARD=Headphones
{{< /highlight >}}

- Repeat steps 7 to 9 until a sound card appears.

##### Step 10 - Play music

- Select track/playlist.
- Click [Play].

##### Step 11 - Set player name

- Access piCorePlayer using the IP address in a browser (ie. http://192.168.1.xxx or http://pcp.local).
- Select [Squeezelite Settings] > "Change Squeezelite settings" > "Name of your player".
- Type player name `pCP Akkordion`.
- Click [Save].

##### Step 13 - Set hostname

- Select [Tweaks] > "pCP System Tweaks" > "Host name".
- Type host name `pCPAkkordion`.
- Click [Save].
- Click [Yes] when requested to "Reboot piCorePlayer".

{{< card style="success" >}}
Congratulations!
{{< /card >}}


## Testing optional overlays

##### Step 1 - Edit config.txt 

{{< card style="warning" >}}
A [Reboot] is required after changing config.txt for the overlay to take affect.
{{< /card >}}

- Edit config.txt---see [Edit config.txt](/how-to/edit_config_txt/).

\$ `m1`

\$ `c1`

\$ `vicfg`

From this:
{{< highlight "text" "linenos=table,hl_lines=6,linenostart=113" >}}
# onboard audio overlay
#Custom Configuration Area, for config settings that are not managed by pCP.
#pCP will retain these settings during insitu-update
#---Begin-Custom-(Do not alter Begin or End Tags)-----
#---End-Custom----------------------------------------
dtoverlay=hifiberry-dac

{{< /highlight >}}

To this:
{{< highlight "text" "linenos=table,hl_lines=6,linenostart=113" >}}
# onboard audio overlay
#Custom Configuration Area, for config settings that are not managed by pCP.
#pCP will retain these settings during insitu-update
#---Begin-Custom-(Do not alter Begin or End Tags)-----
#---End-Custom----------------------------------------
dtoverlay=akkordion-iqdacplus
{{< /highlight >}}

##### Step 2 - Reboot

- Click [Reboot] or type \$ `pcp br`.


## Overlays README

{{< card style="danger" >}}
The **akkordion-iqdacplus** overlay doesn't appear to work.
{{< /card >}}

{{< highlight "text" "linenos=table,hl_lines=4" >}}
Name:   akkordion-iqdacplus
Info:   Configures the Digital Dreamtime Akkordion Music Player (based on the
        OEM IQAudIO DAC+ or DAC Zero module).
Load:   dtoverlay=akkordion-iqdacplus,<param>=<val>
Params: 24db_digital_gain       Allow gain to be applied via the PCM512x codec
                                Digital volume control. Enable with
                                dtoverlay=akkordion-iqdacplus,24db_digital_gain
                                (The default behaviour is that the Digital
                                volume control is limited to a maximum of
                                0dB. ie. it can attenuate but not provide
                                gain. For most users, this will be desired
                                as it will prevent clipping. By appending
                                the 24db_digital_gain parameter, the Digital
                                volume control will allow up to 24dB of
                                gain. If this parameter is enabled, it is the
                                responsibility of the user to ensure that
                                the Digital volume control is set to a value
                                that does not result in clipping/distortion!)

{{< /highlight >}}


{{< table style="table-striped" >}}
| **dtoverlay**                 | **Works** |
|-------------------------------|-----------|
| dtoverlay=hifiberry-dac       | yes       |
| dtoverlay=akkordion-iqdacplus | no        |
| dtoverlay=iqaudio-dacplus     | no        |
| dtoverlay=iqaudio-dac         | no        |
| dtoverlay=rpi-dac             | no        |
|dtoverlay=rpi-dacplus          | no        |
| dtoverlay=i2s-dac             | yes       |
{{< /table >}}


## More information

- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Edit config.txt](/how-to/edit_config_txt/)
