---
title: Add a 5.5 inch Waveshare AMOLED display
description: null
date: 2022-06-17T21:39:23.820Z
lastmod: 2022-06-17T21:51:55.325Z
author: The Fan Club
weight: 107
pcpver: 7.0.0
toc: true
draft: false
categories:
  - Projects
tags:
  - Display
  - Waveshare
  - jivelite
  - AMOLED
---

{{< lead >}}
I nice write-up by **The Fan Club** - [How to setup piCorePlayer Jivelite for use with a Waveshare 5.5 inch AMOLED touchscreen](https://www.thefanclub.co.za/how-to/how-setup-picoreplayer-jivelite-use-waveshare-55-amoled-touchscreen).
{{< /lead >}}

![5.5 inch AMOLED display](display-1.jpg)


## More information

- [Waveshare 5.5 inch Touch AMOLED Display](https://www.waveshare.com/5.5inch-hdmi-amoled.htm)
- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [Edit config.txt](/how-to/edit_config_txt/)
- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [Raspberry Pi config.cfg](http://elinux.org/index.php?title=RPiconfig)
- [Waveshare 4.1 TFT + piCorePlayer + Jivelite](/projects/waveshare-tft-pcp-jivelite/)
- [Documentation / fb / fbcon.txt](https://mjmwired.net/kernel/Documentation/fb/fbcon.txt#72)
