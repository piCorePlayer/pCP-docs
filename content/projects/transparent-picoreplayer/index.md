---
title: Transparent pCP
description: Transparent piCorePlayer
date: 2024-07-04T21:48:08.285Z
lastmod: 2024-07-04T21:48:12.221Z
author: Babbelnet
weight: 260
pcpver: 9.0.0
toc: true
draft: false
categories:
    - Projects
---

{{< lead >}}
Here is my new project.

It was born purely as a hobby and to test my skill to do something like this.

I built the case with laser-cut clear acrylic glass to allow a view of all the wonderful technology inside.
The player contains a RPi Zero WH, Hifiberry Zero, 2.2 inch none-touchable display and two rotary encoders.

I had struggled a bit with the rotary encoders to find the right GPIOs and parameters in the script.

I also had struggled with the Hifiberry Zero. The description mentions only 4 GPIOs are needed to work, but you need a total of 9!

The jacks are easily accessible on the back through cutouts. The SD card can also be removed from the side.

Unfortunately, I messed up the front panel a bit when assembling it and still have to replace it.

But I'll do that with the next project.
{{< /lead >}}


## Hardware

- Raspberry Pi Zero WH
- Hifiberry Zero
- 2.2 inch none-touchable display
- two rotary encoders


## The Result

![Transparent piCorePlayer](transparent-1.png)
![Transparent piCorePlayer](transparent-2.png)
![Transparent piCorePlayer](transparent-3.png)

The POC with a RPi 3B+. Later I switched to a RPi Zero to save more space.

![Transparent piCorePlayer](transparent-4.png)

The lasercut acrylics and the other parts that were needed.

![Transparent piCorePlayer](transparent-5.png)


## Demonstration video

- [Transparent piCorePlayer Video](https://youtu.be/vYgkNVArYh8)

For those who want to rebuild this player or only parts of it, you can request the *.dxf Autocad-Files for cutting the acrylics or the GPIO script I used for the rotary encoders.

Be curious about my next project, which is already in my head.


## Rotary encoder setup

These extensions were necessary on pCP for Rotary encoders:
- nano.tcz
- pcp-sbpd.tcz
- pigpio.tcz

...and the related script:

{{< code lang="shell" >}}
#!/bin/sh

# start pigpiod daemon

pigpiod -t 0 -f -l -s 10

# give the daemon a moment to start up before issuing the sbpd command

sleep 2

# load uinput module, then set the permission to group writable, so you don't need to run sbpd with root permissions

sudo modprobe uinput

sudo chmod g+w /dev/uinput

# issue the sbpd command

# This command will cause encoder to move selection up or down; Press=Enter; long press=escape

sbpd e,23,24,KEY:KEY_UP-KEY_DOWN,4 e,5,6,KEY:KEY_EQUAL-KEY_MINUS,4 b,12,KEY:KEY_ENTER,2,0,KEY:KEY_ESC,250 b,17,PLAY,2,0,POWR,250​
{{< /code >}}

The command to use the script on pCP [Tweaks] Page:

![Transparent piCorePlayer](transparent-6.png)

The used GPIO's on the RPi:

![Transparent piCorePlayer](transparent-7.png)


## More information

- [Transparent piCorePlayer forum thread](https://forums.slimdevices.com/forum/user-forums/diy/1712258-transparent-picoreplayer)
- [Add a 2.2 inch ili9341 spi display](https://docs.picoreplayer.org/projects/add-a-2-2-inch-ili9341-display/)
- [Control Jivelite by rotary encoders and buttons](https://docs.picoreplayer.org/projects/control-jivelite-by-rotary-encoders-and-buttons/)
