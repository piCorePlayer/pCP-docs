---
title: Batch file to robocopy
description: Batch file to use robocopy to mirror music library
date: 2024-06-26T23:29:13.990Z
lastmod: 2024-06-26T23:31:01.105Z
author: pCP Team
weight: 170
pcpver: 8.0.0-b7
toc: true
draft: false
categories:
    - Projects
tags:
    - batch
    - robocopy
---

{{< lead >}}
A MSDOS batch file for Windows 10 to mirror copy LMS music library from one USB disk to another.
{{< /lead >}}

{{< card style="warning" >}}
Advanced users only. Using mirror copy programs in the wrong direction can quickly delete files. Use at your own risk!
{{< /card >}}

I use three NTFS formatted USB drives for my LMS music library. I required a method of ensuring each drive was an exact mirror copy. Windows 10 has `robocopy` which is a great program for creating and maintaining mirror copies. For this batch file to work, to following directories are required at the drive root level, `CDnew`, `CD`, `CD1` and `CD2`.

I don't expect this is useful for most piCorePlayer users, so it is not part of piCorePlayer, but just in case someone had a similar thought, I decided to publish this script to reduce some of the effort of learning MSDOS batch scripting. You will need to copy and paste the following into a batch file. It will require modifications to your system and modification of the batch file to suit your requirements. I imagine someone may find this batch file a useful starting point.


## Batch file

```batch
::=======================================================================================
:: mirrorCDtoCD.bat
::---------------------------------------------------------------------------------------
:: Written by:  Greg Erskine
:: Date:        9 June 2021
:: Version:     V1.0
::---------------------------------------------------------------------------------------
:: This batch file assists you to mirror D:\CD1 to E:\CD2.
::
::   - CDnew - Ripping PC - temporary - synced via robocopy
::   - CD1   - PC copy #1 - synced via robocopy
::   - CD2   - PC copy #2 - synced via robocopy
::   - CD    - RPi LMS copy - synced via WinSCP
::---------------------------------------------------------------------------------------

@echo off 
cls

::---------------------------------------------------------------------------------------
:: Check OS version (Windows XP or later)
::---------------------------------------------------------------------------------------
if not "%OS%"=="Windows_NT" goto :EOF

::---------------------------------------------------------------------------------------
:: Find all drive letters using command 'FSUTIL FSINFO DRIVES'.
:: Result format 'Drives: C:\ D:\ E:\ F:\'
:: Result format 'C:\ D:\ E:\ F:\'
::---------------------------------------------------------------------------------------
set "Drives="
for /F "tokens=1*" %%A in ('fsutil fsinfo drives') do (
	set "Drives=%%B"
)

echo.=======================================
echo   Drives found
echo.---------------------------------------
:: Result format 'C: D: E: F:'
set "Drives=%Drives:\=%"

:: Display drives found and type
for %%D in (%Drives%) do (
	fsutil fsinfo drivetype %%D
)
echo.

echo.=======================================
echo   CD locations found
echo.---------------------------------------
set "Drive="
for %%D in (%Drives%) do (
	for %%C in (CD CD1 CD2 CDnew) do (
		if exist %%D\%%C (
			echo %%D\%%C exists
		)
	)
)
echo.

:MENU
echo.=======================================
echo   Action: Source -^> Destination
echo.---------------------------------------
echo [1] - Mirror CD1 -^> CD2
echo [2] - Mirror CD2 -^> CD1
echo [3] - Copy CDnew -^> CD2
echo [X] - Exit batch file
echo.

:CHOICE
	choice /c 123x /n /m "Select option: "
	if %errorlevel%==1 goto :OPT1
	if %errorlevel%==2 goto :OPT2
	if %errorlevel%==3 goto :OPT3
	goto :EOF

:OPT1
	echo.[ INFO ] Option 1
	set "Source=CD1"
	set "Destination=CD2"
	set "Action=Mirroring"
	goto Y

:OPT2
	echo.[ INFO ] Option 2
	set "Source=CD2"
	set "Destination=CD1"
	set "Action=Mirroring"
	goto Y

:OPT3
	echo.[ INFO ] Option 3
	set "Source=CDnew"
	set "Destination=CD2"
	set "Action=Copying"
	goto Y

:Y

::---------------------------------------------------------------------------------------
:: Test for source drive and directory
::---------------------------------------------------------------------------------------
set "SourceDrive="
for %%S in (%Drives%) do (
	if exist %%S\%Source% (
		echo [ INFO ] Source %%S\%Source% exists.
		set "SourceDrive=%%S\%Source%"
	)
)
if "%SourceDrive%"=="" (
	echo [ ERR  ] Source %Source% does not exists.
	goto :EOF
)
echo [ INFO ] Set Source %SourceDrive%

::---------------------------------------------------------------------------------------
:: Test for destination drive and directory
::---------------------------------------------------------------------------------------
set "DestinationDrive="
for %%D in (%Drives%) do (
	if exist %%D\%Destination% (
		echo [ INFO ] Destination %%D\%Destination% exists.
		set "DestinationDrive=%%D\%Destination%"
	)
)
if "%DestinationDrive%"=="" (
	echo  [ ERR  ] Destination %Destination% does not exists.
	goto :EOF
)
echo [ INFO ] Set Destination %DestinationDrive%

::---------------------------------------------------------------------------------------
:: Generate log file name
::                   A   B  C  D
::  - Date format - Sun 09/05/2021
::                   A  B  C  D
::  - Time format - 11:00:59.30
::---------------------------------------------------------------------------------------
for /F "tokens=1-4 delims=/ " %%A in ("%Date%") do (
    set "Today=%%D%%C%%B"
)

for /F "tokens=1-3 delims=:." %%A in ("%Time: =0%") do (
    set "Now=%%A%%B%%C"
)

set "Log=C:\temp\%~n0-%Today%-%Now%.log"
echo [ INFO ] Log: %Log%

if "%Action%"=="Mirroring" (
	set "Options=/MIR /PURGE /R:3 /W:2 /Log:%Log%"
)

if "%Action%"=="Copying" (
	set "Options=/E /Log:%Log%"
)

if "%Action%"=="Attributes" (
	set "Options=/MIR /copy:ATS /dcopy:AT /R:3 /W:2 /Log:%Log%"
)
echo.[ INFO ] Options: %Options%

::---------------------------------------------------------------------------------------
:: Mirror or copy CD directory to CD directory
::---------------------------------------------------------------------------------------
:TOP
	echo.
	choice /c yn /n /m "%Action% %SourceDrive% to %DestinationDrive%. Continue Y/N?"
	if %errorlevel%==1 goto :COPY
	goto :EOF

:COPY
	robocopy %SourceDrive% %DestinationDrive% %Options%

	if %errorlevel% EQU 16 (
		echo.
		echo [16] Serious error. Robocopy did not copy any files.
		echo Either a usage error or an error due to insufficient access privileges
		echo on the source or destination directories.
	)
	if %errorlevel% EQU 15 (
		echo.
		echo [15] OKCOPY + FAIL + MISMATCHES + XTRA
	)
	if %errorlevel% EQU 14 (
		echo.
		echo [14] FAIL + MISMATCHES + XTRA
	)
	if %errorlevel% EQU 13 (
		echo.
		echo [13] OKCOPY + FAIL + MISMATCHES
	)
	if %errorlevel% EQU 12 (
		echo.
		echo [12] FAIL + MISMATCHES
	)
	if %errorlevel% EQU 11 (
		echo.
		echo [11] OKCOPY + FAIL + XTRA
	)
	if %errorlevel% EQU 10 (
		echo.
		echo [10] FAIL + XTRA
	)
	if %errorlevel% EQU 9 (
		echo.
		echo [9] OKCOPY + FAIL
	)
	if %errorlevel% EQU 8 (
		echo.
		echo [8] Some files or directories could not be copied.
		echo ^(copy errors occurred and the retry limit was exceeded^).
		echo Check these errors further.
	)
	if %errorlevel% EQU 7 (
		echo.
		echo [7] Files were copied, a file mismatch was present, and additional files were present.
	)
	if %errorlevel% EQU 6 (
		echo.
		echo [6] Additional files and mismatched files exist. No files were copied and no failures were encountered.
		echo This means that the files already exist in the destination directory.
	)
	if %errorlevel% EQU 5 (
		echo.
		echo [5] Some files were copied. Some files were mismatched. No failure was encountered.
	)
	if %errorlevel% EQU 4 (
		echo.
		echo [4] Some Mismatched files or directories were detected.
		echo Examine the output log. Housekeeping might be required.
	)
	if %errorlevel% EQU 3 (
		echo.
		echo [3] Some files were copied. Additional files were present. No failure was encountered.
	)
	if %errorlevel% EQU 2 (
		echo.
		echo [2] Some Extra files or directories were detected. No files were copied.
		echo Examine the output log for details.
	)
	if %errorlevel% EQU 1 (
		echo.
		echo [1] One or more files were copied successfully ^(that is, new files have arrived^).
	)
	if %errorlevel% EQU 0 (
		echo.
		echo [0] No errors occurred, and no copying was done.
		echo The source and destination directory trees are completely synchronized.
	)

:VIEWLOG
	echo.
	echo Refer to log file %Log% for details.
	notepad %Log%

:END
	echo.
	pause
```


## Output

Running the batch file will give you the following information and menu options.

{{< card style="danger" >}}
Don't mirror copy in the wrong direction, tears will result.
{{< /card >}}

```text
=======================================
  Drives found
---------------------------------------
C: - Fixed Drive
D: - Fixed Drive
E: - Fixed Drive

=======================================
  CD locations found
---------------------------------------
D:\CD2 exists
E:\CD1 exists

=======================================
  Action: Source -> Destination
---------------------------------------
[1] - Mirror CD1 -> CD2
[2] - Mirror CD2 -> CD1
[3] - Copy CDnew -> CD2
[X] - Exit batch file

Select option:
```


## Network Layout

<div class="mermaid">
	graph TD
		I[Internet] ---          WR([Wifi/Router])
		WR          ---          X[Switch]
		X           ---          P1[pCP Player 1]
		P1          ---          P1S[Touch Screen]
		X           ---          D[pCP LMS]
		D           ---     |CD| H[(USB HDD)]
		X           ---          PC1[PC Windows]
		PC1         ---    |CD1| PC1H[(USB HDD)]
		PC1         ---    |CD2| PC2H[(USB HDD)]
		WR          ---   |WiFi| PC2[Ripper Windows]
		PC2         ---    |CD2| PC2H[(USB HDD)]
		PC2         ---  |CDnew| PC3H[(INT HDD)]
		WR          ----  |WiFi| P2[pCP Player 2]
		P2          ---    |USB| DAC[DAC]
		WR          ----  |WiFi| P3[pCP Player 3]
		P3          ---   |HDMI| TV[TV Screen]
		WR          ----  |WiFi| P4[pCP Player 4]
		P4          ---   |HDMI| TV[TV Screen]
</div>


## Process

1. On `Ripper W10` rip CDs to `C:\CDnew`
1. Plug in USB HDD with `drive:\CD2` onto `Ripper W10`
1. Use option `[3] - Copy CDnew -> CD2`
1. Plug in USB HDD with `drive:\CD2` onto `PC W10`
1. Plug in USB HDD with `drive:\CD1` onto `PC W10`
1. Use option `[2] - Mirror CD2 -> CD1`
1. Use WinSCP sync to synchronize `CD1 -> CD`
1. Test `CD`
1. Unplug `CD1` and `CD2` USB HDDs and store offline

The result is three identically mirrored USB drives all of which have been used in the process guaranteeing we have three "tested good" copies. Two copies can now be stored off-line and off-site for recovery purposes. As these backup drives are not powered on, failure of these backup copies is virtually impossible.

In case of failure of `CD` USB HDD.

1. Replace USB HDD with `CD` with USB HDD with `CD1`
1. Rename directory from `CD1` to `CD`
1. Buy new drive and plug into `PC W10`
1. Create directory `drive:\CD1`
1. Plug in USB HDD with `drive:\CD2` onto `PC W10`
1. Use option `[2] - Mirror CD2 -> CD1` to create A new mirror
1. Unplug `CD1` and `CD2` and store offline


## More information

- [DosTips - The DOS Batch Guide](https://www.dostips.com/)
- [Rob van der Woudes Scripting Pages](https://www.robvanderwoude.com/batchstart.php)
- [Windows Batch Scripting](https://en.wikibooks.org/wiki/Windows_Batch_Scripting)
- [Using batch files](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-xp/bb490869(v%3dtechnet.10))
- [robocopy](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/robocopy)
- [Robocopy and a Few Examples](https://social.technet.microsoft.com/wiki/contents/articles/1073.robocopy-and-a-few-examples.aspx)
- [ROBOCOPY Exit Codes](https://ss64.com/nt/robocopy-exit.html)
