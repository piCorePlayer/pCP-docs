---
title: Add a 5 inch Waveshare display spi touch
description: null
date: 2022-07-29T22:06:29.128Z
lastmod: 2024-06-24T23:17:10.870Z
author: mini11
weight: 106
pcpver: 8.2.0
toc: true
draft: false
categories:
    - Projects
tags:
    - Display
    - Waveshare
    - jivelite
---

{{< lead >}}
Waveshare 5 inch HDMI Display, 800x480, resistive spi touch with piCorePlayer.
**Note** If upgrading from pCP version 9.2 or lower, or if installing fresh on pCP 10 or higher, be sure to read the important note below, otherwise jivelite will fail to write out its settings.
{{< /lead >}}

![5 inch display](display-1.jpg)

![5 inch display](display-2.jpg)


## What we need

- Raspberry Pi 2, 3, 4 or piZero WH
- 8GB Micro SD card
- Waveshare 5 inch display, resistive spi-touch with HDMI-adaptor
- Optional: Some jumper wires, female to female
- Powersupply, 5V, >= 2,5 A
- PC or Laptop, Putty installed


## Preparation

- Plug the display directly on the GPIO pins of your PI and connect the HDMI plug with an HDMI-adaptor

![5 inch display](display-3.jpg)

- If you need additional pins for other purpose, wire the pins 1, 2, 6, 19, 21, 22, 23, 26) and connect the HDMI plug with a HDMI cable.
 
{{< table style="table-striped" >}}
| Pin | Symbol | Discription                      |
|-----|--------|----------------------------------|
| 1   | 3,3 V  | Power positive (3.3V power input)|
| 2   | 5 V    | Power positive (5V power input)  |
| 6   | GND    | Ground                           |
| 19  | TP_SI  | SPI data input of Touch Panel    |
| 21  | TP_SO  | SPI data output of Touch Panel   |
| 22  | TP_IRQ | Touch Panel interrupt            |
| 23  | TP_SCK | SPI clock of Touch Panel         |
| 26  | TP_CS  | Touch Panel chip selection       |
{{< /table >}}


## Steps

##### Step 1 - Prepare SD card

- Put a fresh pCP image on to the SD card---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).
- While the SD card is still in the laptop/pc, edit the following three files:

{{< card style="info" >}}
You find these files in your Windows Explorer in root-section of the pcp_boot drive.
{{< /card >}}

![files](display-4.jpg)

- Enter wifi access data in `wpa_supplicant.conf.sample` and "Save as" `wpa_supplicant.conf`.

```text
# This is the details about your Wireless network. Configure as needed.
# Most will just need to change the ssid and psk.

network={
        ssid="your ssid"
        psk="your password"
        key_mgmt=WPA-PSK
        auth_alg=OPEN
}
```
	
- Add the following lines to `config.txt`, in the Custom Configuration area at the end of the file (between the Begin-Custom and End-Custom lines).

```text
#---Begin-Custom-(Do not alter Begin or End Tags)-----
gpu_mem=128
disable_splash=1
avoid_warnings=2
hdmi_group=2
hdmi_mode=87
hdmi_drive=1
hdmi_cvt 800 480 60 6 0 0 0
dtoverlay=ads7846,cs=1,penirq=25,penirq_pull=2,speed=50000,keep_vref_on=0,swapxy=0,pmax=255,xohms=150,xmin=200,xmax=3900,ymin=200,ymax=3900
#---End-Custom------------------------
```

- Comment out the `gpu_mem=16` and `lcd_rotate=2` default settings by putting an # at the beginning of the line.
- Save your changes.

##### Step 2 - Boot Raspberry Pi

- Put the SD card in your RPi and boot.
- Find the IP address of the RPi, and enter this in a browser---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).
- Once the RPi has booted up, this should bring up the pCP web interface.
	- Install Jivelite from the [Tweaks page] (this step includes an automatic backup, so accept the reboot	prompt.)
	- After the reboot do not configure jivelite – we’ll do this later!

##### Step 3 - Make a custom jivelite.sh startup script

**Note** This has changed for pCP version 10 and higher. The jivelite startup script is now considerably more sophisticated, and you may not need a custom jivelite script at all. If you do find you need one, be sure to include the line starting 'export HOME ...' as shown below.
- Login to your RPi via ssh (software=putty – user: tc, password: piCore) and insert the following command:
	- $ `tce-load -wi nano.tcz`
- This installs the `nano.tcz` editor extension.
	- $ `cat /proc/bus/input/devices`
- You see your connectet input-devices (ie. touchscreen, mouse or ir-reciever)

![devices](display-5.jpg)

- Take note of "event0" (If you use additional input devices (ie. IR-Receiver), the input device may be event1).

{{< card style="info" >}}
If you add additional input devices later, the event of your touchscreen may change and the touch won't work.
This affords adjusting your `jivelite.sh` script. 
{{< /card >}}

- Still connected by ssh, insert the following command:
	- $ `sudo nano /mnt/mmcblk0p2/tce/jivelite.sh`
- Put into the empty file (copy and paste) the following script:

{{< code lang="shell" >}}
#!/bin/sh

EVENTNO=$(cat /proc/bus/input/devices | awk '/ADS7846 Touchscreen/{for(a=0;a>=0;a++){getline;{if(/mouse/==1){ print $NF;exit 0;}}}}')

export JIVE_NOCURSOR=1
export TSLIB_TSDEVICE=/dev/input/event0
export SDL_MOUSEDRV=TSLIB

export HOME=/home/tc

while true; do
    /opt/jivelite/bin/jivelite
	sleep 3
done
{{< /code >}}

{{< card style="info" >}}
If your event was 1, change event0 to event1
{{< /card >}}

- For this purpose, we need the script to define the touchscreen as input-device and hide the cursor.
- Save your changes and exit nano (Ctrl+X and confirm by pressing y).
- Make the new custom `jivelite.sh` script executable with:
	- $ `sudo chmod +x /mnt/mmcblk0p2/tce/jivelite.sh`
- Save your changes by `pcp bu`

##### Step 5 - Calibrate Touchscreen

- Bring up Web-Gui of piCorePlayer, section [Tweaks] and set "Jivelite Autostart" to "no".
- Reboot your RPi.
- Login to your RPi via ssh (putty – user: tc, password: piCore)---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).
- Insert the following command:
	- $ `sudo TSLIB_FBDEVICE=/dev/fb0 TSLIB_TSDEVICE=/dev/input/event0 /usr/local/bin/ts_calibrate`

{{< card style="info" >}}
If your event was 1, change event0 to event1.
{{< /card >}}

- Click the 5 calibration points properly.
- Make your calibration persistent:
	- $ `pcp bu`
- Go to the WebGui of piCorePlayer, section [Tweaks] and set "Jivelite Autostart" to "yes".
- Reboot your RPi.
	- After the reboot, the touch of the display should work fine.

##### Step 6 - Configure Jivelite

{{< card style="info" >}}
If you use a RPi Zero without audio card, go to WebGui of piCorePlayer, section [Sqeezelite Settings] and change the default audio output to "none".
RPi Zero does not provide headphone output, squeezelite will not start with default settings.
{{< /card >}}

- Configure jivelite with your touchscreen. You can do that with a keyboard or an IR remote too.
- Choose your language and in the next menu select `GridSkin 800x480` or `jogglerskin`. Both work fine with that resolution.
- Configure the additional settings (time, screensaver, background and so on).
- Save  your changes in Jivelite Settings (Settings > piCorePlayer > Save Settings to SD Card)


## More information

- [5 inch Capacitive Touch Screen](https://www.waveshare.com/product/displays/lcd-oled/lcd-oled-1/5inch-hdmi-lcd.htm?___SID=U)
- [Add an IR receiver to piCorePlayer](https://docs.picoreplayer.org/projects/add-an-ir-receiver/)
- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [Edit config.txt](/how-to/edit_config_txt/)
- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [Raspberry Pi config.cfg](http://elinux.org/index.php?title=RPiconfig)
- [Waveshare 4.1 TFT + piCorePlayer + Jivelite](/projects/waveshare-tft-pcp-jivelite/)
- [Documentation / fb / fbcon.txt](https://mjmwired.net/kernel/Documentation/fb/fbcon.txt#72)
