---
title: 3D printed case
description: 3D printed case for touchscreen controlled piCorePlayer
date: 2022-10-05T19:55:43.010Z
lastmod: 2024-06-24T23:15:51.972Z
author: mfraser
weight: 230
pcpver: 8.2.0
toc: true
draft: false
categories:
  - Projects
tags:
  - 3D printing
  - Case
---

{{< lead >}}
I've designed my own case for piCorePlayer that has a 5" touchscreen and built in amp and speakers.
{{< /lead >}}

![3D printed case](3D-printed-case.webp)

{{< card style="warning" >}}
The buttons on top will work once I figure out why piCorePlayer doesn't start squeezelite if more than one button is enabled with SqueezeButtonPi.
{{< /card >}}


## More information

- [My 3D printed case for touchscreen controlled piCorePlayer - Forum thread](https://forums.slimdevices.com/showthread.php?116894-My-3D-printed-case-for-touchscreen-controlled-PiCorePlayer)
- [Raspberry Pi Squeezebox case - Printables](https://www.printables.com/model/283512-raspberry-pi-squeezebox-case)
