---
title: Add a 4.3 inch Waveshare DSI Display
description: Add a 4.3 inch Waveshare DSI Display, Touch, 800x480
date: 2025-01-23T04:19:11.345Z
lastmod: 2025-01-23T04:24:07.358Z
author: mini11
weight: 105
pcpver: 9.2.0
toc: true
draft: false
categories:
  - Projects
tags:
  - Display
  - Waveshare
  - Touch
  - jivelite
---

{{< lead >}}
Waveshare 4.3 inch DSI Touch Display, 800x480 with piCorePlayer.
{{< /lead >}}

![4.3 inch display](dsi_1.jpg)

![4.3 inch display](dsi_2.jpg)

{{< card style="info" >}}
This is one of the easiest ways to get a touch display running under pCP.
It may run in same way with bigger DSI-Displays, same resolution, but it isn't tested yet.
{{< /card >}}


## What we need

- Raspberry Pi 3/4
- 8 GB Micro SD card
- Waveshare 4.3 inch DSI Display
- Soundcard of your choice
- Power supply


## Steps

##### Step 1 - Connect the display to your Raspberry Pi

- Connect the display with the supplied flat-cable to your RPi.
- For easy use, you can fix the RPi on the backside of the Display.

![4.3 inch display](dsi_3.jpg)

- Connect your soundcard.

##### Step 2 - Prepare SD card

- Put a fresh pCP 9 image on to the SD card---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).

{{< card style="warning" >}}
The DSI Touch does not work with previous versions of piCorePlayer installed on RPi3 and older.
{{< /card >}}

- While the SD card is still in the laptop/pc, edit the following two files:

{{< card style="info" >}}
You find these two files in your Windows Explorer in root-section of the pcp_boot drive.
{{< /card >}}

- Enter wifi access data in wpa_supplicant.conf.sample and "save as" wpa_supplicant.conf.

{{< card style="info" >}}
This is the detail of your Wireless network. Configure as needed. Most will just need to change the ssid and psk.
{{< /card >}}

```text
network={
        ssid="your ssid"
        psk="your password"
        key_mgmt=WPA-PSK
        auth_alg=OPEN
```

{{< card style="info" >}}
If you connect your device by lan, skip wpa_supplicant.conf.
{{< /card >}}

- Add the following lines to config.txt, at the end of the file.

```text
disable_splash=1
avoid_warnings=2
dtoverlay=vc4-kms-v3d
dtoverlay=vc4-kms-dsi-7inch

```

- If existing, comment out the `gpu_mem=16` and `lcd_rotate=2` default settings by putting an # at the beginning of the line.
- Save your changes.

##### Step 3 - Boot Raspberry Pi

- Put the SD card in your RPi and boot.
- Find the IP address of the RPi, and enter it in a browser---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

Once the RPi has booted up, this should bring up the pCP web interface.

- Setup your Audio output device [Squeezelite Settings] (this step includes a reboot, do not accept - we'll have a reboot in next step)
- Install Jivelite from the [Tweaks page] (this step includes an automatic backup, so accept the reboot	prompt.)

After the reboot the display and touch should work.
  
- Configure Jivelite.
  - Choose your language and in the next menu select `GridSkin 800x480` or `Jogglerskin`. Both work fine with that resolution.
  - Configure the additional settings (time, background and so on).
  - Do not forget to save your changes in Jivelite Settings (Settings > piCorePlayer > Save Settings to SD-Card)


## More information

- [4.3 inch Waveshare DSI Display Capacitive Touch](https://www.waveshare.com/4.3inch-dsi-lcd.htm)
- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [Edit config.txt](/how-to/edit_config_txt/)
