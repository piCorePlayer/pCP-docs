---
title: Build LMS server
description: Build a simple LMS server
date: 2024-05-08T02:04:34.682Z
lastmod: 2024-06-24T23:23:04.058Z
author: pCP Team
weight: 70
pcpver: 9.0.0
toc: true
draft: false
categories:
    - Projects
tags:
    - LMS
---

{{< lead >}}
This project is to create a simple LMS server with the music library on an attached USB HDD. This is the LMS server I use every day for personal use and for testing piCorePlayer.
{{< /lead >}}


## What was used

### Hardware

- [Raspberry Pi 4B - 8GB](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/)
- [Official RPi 5.1V 3A PSU](https://www.raspberrypi.org/products/type-c-power-supply/)
- [Toshiba USB 2TB Hard Disk Drive](https://www.officeworks.com.au/shop/officeworks/p/toshiba-2tb-canvio-basics-portable-hard-drive-black-tobacv2tb)
- [SanDisk Ultra 32GB SD card](https://www.officeworks.com.au/shop/officeworks/p/sandisk-ultra-32gb-microsdxc-squa4-memory-card-sdsqua432)

### Software

- piCorePlayer 9.0.0
	- LMS

### Network Diagram

<div class="mermaid">
	graph TD
		Switch[8-port Network Switch]
		Switch --> |Ethernet cable| RPi[RPi4B - pCP LMS]
		RPi --- |USB cable| HDD[(USB HDD)]
</div>


## Steps

##### Step 1 - Download pCP

- Download latest piCorePlayer---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2 - Create SD card

- Create an SD card with the piCorePlayer image---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).
- Eject the SD card.

##### Step 3 - Boot pCP

- Insert the SD card into the Raspberry Pi.
- Connect the Ethernet cable.
- Connect the power cables.
- Connect the (NTFS formatted) USB hard drive.
- Turn the power on.

##### Step 4 - Determine the IP address

- Determine the IP address---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

##### Step 5 - Set static IP (optional)

- Set static IP on the DHCP sever (ie. http://192.168.1.51). The DHCP server is often on the router---see Your router manual.

##### Step 6 - Set player name

- Access pCP using IP in a browser (ie. http://192.168.1.51).
- Select [Squeezelite Settings] > "Change Squeezelite settings" > "Name of your player".
- Type player name.
- Click [Save].

##### Step 7 - Set hostname

- Select [Tweaks] > "pCP System Tweaks" > "Host name".
- Type host name.
- Click [Save].
- Click [OK] when requested to "Reboot piCorePlayer".

##### Step 8 - Resize filesystem

- Select [Main Page].
- Click "Advanced operations" > [Resize FS].
- Select "Whole SD Card".
- Click [Resize].

{{< card style="info" >}}
piCorePlayer will now reboot a couple of times, but after a couple of minutes piCorePlayer should refresh to the [Main Page].
{{< /card >}}

##### Step 9 - Set mode to Player/Server

- Click on the [Player/Server] tab found on the bottom left corner.

##### Step 10 - Mount the USB hard drive

- Select [LMS].
- Check what FS Type is displaying in "Pick from the following detected USB disks to mount".
- If you are using a non-default disk format, like NTFS, you will be prompted to `Please install extra file systems` in the UUID field.
    - Click "Install and Enable additional File Systems" > [Install].
- Type a mount point name (ie. /mnt/USB).
- Tick "Enabled" checkbox.
- Click [Set USB Mount].

##### Step 11 - Install LMS

- Select [LMS].
- Click [Install].

##### Step 12 - Start LMS

- Click [Start LMS].

##### Step 13 - Configure LMS

- Click [Configure LMS].
- Set "Media Folders". (ie. /mnt/USB/CD)
- Click [Apply].
	- A scan of the library should now commence.

{{< card style="info" >}}
An LMS scan for music will start. This can take some time. For example, 1300 albums will take about 10 minutes.
{{< /card >}}

{{< card style="danger" >}}
I have noticed sometimes a second scan is initiated that gets stuck at the `Pre-caching Artwork` stage. I haven't tracked down the cause, so if you see `Pre-caching Artwork` message taking a long time -- reboot. A `clear library and rescan everything` may then be necessary.
{{< /card >}}


## More information

- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
