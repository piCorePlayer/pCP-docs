---
title: Create custom kernel keytable
description: Create a custom kernel IR keytable
date: 2022-07-20T00:13:14.550Z
lastmod: 2024-06-24T23:28:20.086Z
author: mini11
weight: 110
pcpver: 8.0.0
toc: true
draft: false
categories:
  - Projects
tags:
  - IR remote NEC
  - TSOP4838
  - OS-0038N
  - keytables
  - jivelite
---

{{< lead >}}
Create keytables for IR remote.
{{< /lead >}}

- Today nearly every device is fitted with an IR remote, so why not piCorePlayer?
- If your display has no touch capability or you do not want to control your device by a smartphone, an IR remote can be very helpful.
- Since 4.19 linux kernel the original LIRC uinput drivers have been removed.
- We need to configure IR keytables to get IR remotes running with pCP.
- We use a cheap [NEC Remote](https://www.audiophonics.fr/en/infrared-remote-controls-and-receivers/38khz-nec-remote-with-navigation-pad-p-10901.html).

![IR receiver](keytables-1.jpg)

- You can create a custom kernel keytable with any supported IR remote that uses rc-5, rc-5-sz, jvc, sony, nec, sanyo, mce_kbd, rc-6, sharp or xmp protocol.
- I have successfully tested SEG, Western Digital, Haupauge, NEC and JustBoom IR remotes.


## What we need

- Raspberry Pi, piCorePlayer 7 or 8, Jivelite installed
- A display connected to RPi
- An IR remote
- A TSOP4838 or similar IR receiver like OS-0038N connected to RPi---see [Add an IR receiver to piCorePlayer](/projects/add-an-ir-receiver/)
- Powersupply, 5V, 2,5 A
- Putty installed on PC/Laptop
- Text editor installed on PC/Laptop


## Steps

##### Step 1 - Check compatibility

- Bring up the pCP web interface.
- Click [Tweaks] tab and scroll down to section "IR remote control".
- Click [IR page] and install IR tools (accept the reboot prompt).
- When the reboot has finished, login to your RPi via ssh (software=putty – user: tc, password: piCore) and enter the following command:
	- $ `sudo ir-keytable -t`
- Press some buttons, and you should see something like this:

![devices](keytables-2.jpg)

{{< card style="warning" >}}
If you do not see any output pressing your IR remote buttons:

- Check the configured GPIO of your IR receiver---see [Tweaks] > [IR page].
- Check everything is connected and configured correctly.
- Perhaps your IR remote is not supported.
{{< /card >}}

- Stop the process by pressing control+c.

##### Step 2 - Create a keytable

- The keytable file is a plain text file without a file extension. You can create it on your PC or laptop.
- Open your favourite text editor and insert the header:

```text
	# table: nec, type: nec
```

- Back in a ssh window, enter the following command:
	- $ `sudo ir-keytable -t`
- First press the home button. Note the scancode, go back to editor and insert it in line 2, column 1.
- In column 2 write down the key-command, here KEY_N.
- You can insert a description to column three, not necessary but helpful.
- Don't forget to comment out the description out with a #.
- Now go on, step by step, with every button of the remote. When finished, your file should look like this:

{{< code lang="shell" >}}
# table: nec, type: nec
0x728D	KEY_N			#go home/now playing
0x6897	KEY_C			#pause
0xEA15	KEY_POWER		#Power
0xC23D	KEY_UP
0x50AF	KEY_DOWN
0x609F	KEY_LEFT
0x708F	KEY_RIGHT
0x40BF	KEY_L			#OK
0x58A7	KEY_A			#Menu
0xB24D	KEY_ESC			#Back
0x0AF5	KEY_EQUAL		#Volume Up
0x4AB5	KEY_MINUS		#Volume Down
{{< /code >}}

{{< card style="info" >}}
- This is only an example for the NEC remote.
- If you use another remote, your scancodes may look different.
- Feel free, to to assign other jivelite key-commands.
- Change the key-commands in column two to suit your purpose.
- You find a complete list of [Jivelite key-commands](https://github.com/ralph-irving/tcz-lirc/blob/master/jivekeys.csv).
{{< /card >}}

- Save the file as a plain text, filename `jivelite`, without a file extension.

##### Step 3 - Load keytable file

- Bring up the pCP web interface.
- Click [Tweaks] tab and scroll down to section "IR remote control".

![ir-page](keytables-3.jpg)

- Click [IR page].
- Click [Browse] and search for your custom keytable.

![ir page](keytables-4.jpg)

- Load your keytable file.
- Your IR remote should now work without reboot.


## More information

- [Add an IR receiver to piCorePlayer](/projects/add-an-ir-receiver/)
- [Jivelite key-commands](https://github.com/ralph-irving/tcz-lirc/blob/master/jivekeys.csv)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
