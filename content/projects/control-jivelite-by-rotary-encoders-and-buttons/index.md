---
title: Control by rotary encoders and buttons
description: Control Jivelite by rotary encoders and buttons
date: 2022-07-13T09:31:51.211Z
lastmod: 2024-06-24T23:28:02.185Z
author: mini11
weight: 117
pcpver: 7.0.0
toc: true
draft: false
categories:
  - Projects
tags:
  - jivelite
  - sbpd
  - rotary encoders
  - pushbuttons
---

{{< lead >}}
Your display has no touch function and you want to control jivelite by hardware? Here is the solution.

How to control Jivelite by rotary encoders and pushbuttons.
{{< /lead >}}

Thanks to chill for the `sbpd-script.sh` script (pirate-audio amp).

![rotary](sbpd-1.jpg)

![pushbutton](sbpd-2.jpg)

- You can simply control jivelite by adding rotary encoders and/or pushbuttons to the GPIO's of your Raspberry Pi.


## What we need

- A running piCorePlayer with Jivelite installed and a display connected to your Raspberry Pi
- Some pushbuttons (normally open)
- One or two rotary encoders (for this purpose, do not use encoders with power supply!)
- Some jumper wires (female to bare wires)
- Soldering iron, solder
- Some heat shrink tubes


## Preparation

- The `sbpd-script.sh` handles with Jivelite key commands. Knowledge of these commands is an advantage.

{{< card style="info" >}}
For a complete list of Jivelite key commands---see [Jivelite key commands](https://github.com/ralph-irving/tcz-lirc/blob/master/jivekeys.csv).
{{< /card >}}

- Solder the jumper wires to rotary encoders and/or pushbuttons. Use some heat shrink tubes to get a stable connection and avoid short circuit.

![connections](sbpd-3.jpg)


## Wiring

- Wire pushbuttons and rotary encoders as shown here. This example matches with the `sbpd-script.sh` below.

![connections](sbpd-4.jpg)

{{< card style="info" >}}
If you have connected an I2S DAC, do not use the blue coloured GPIO's. The red GPIO's are reserved if there is an SPI display on CE1.
Do not use one GPIO for multiple purposes!
{{< /card >}}


## Steps

##### Step 1 - Install extensions

- Bring up the pCP web interface.
  - On [Main Page] scroll down to "Additional functions" and click on the [Extensions] button. Wait for green ticks.

![extensions](sbpd-5.jpg)

- Click on the [Available] tab, in the "Available extensions in the piCorePlayer repository" section.

![available](sbpd-6.jpg)

- Load the `nano.tcz` extension.
- Load the `pcp-sbpd.tcz` extension.
- Check if the `pigpio.tcz` is installed. If not, load the `pigpio.tcz` extension.

##### Step 2 - Make a spbd-script.sh

- Login to your RPi via ssh (software=putty – user: tc, password: piCore) and enter the following command:

```text
sudo nano /home/tc/sbpd-script.sh
```
- Copy the following text and paste it into an empty file:

{{< code >}}
#!/bin/sh

# start pigpiod daemon
pigpiod -t 0 -f -l -s 10

# wait for pigpiod to initialize - indicated by 'pigs t' exit code of zero

count=10 # approx time limit in seconds
while ! pigs t >/dev/null 2>&1 ; do
	if [ $((count--)) -le 0 ]; then
		printf "\npigpiod failed to initialize within time limit\n"
		exit 1
	fi
#	printf "\nWaiting for pigpiod to initialize\n"
	sleep 1
done
printf "\npigpiod is running\n"

# load uinput module - required to be able to send keystrokes
# then set the permission to group writable, so you don't need to run sbpd with root permissions
sudo modprobe uinput
sudo chmod g+w /dev/uinput

# The full list of Jivelite key commands can be found here:
# https://github.com/ralph-irving/tcz-lirc/blob/master/jivekeys.csv

# button 1						# button-section, defines the GPIO and key-commands
SW1=5 							# GPIO (BCM, not Board)
SH1=KEY:KEY_1					# key-command for SHORT press (here: preset 1)
LO1=KEY:KEY_2					# key-command for LONG press (here: preset 2)
LMS1=250 						# milliseconds for long press

# button 2
SW2=6
SH2=KEY:KEY_3
LO2=KEY:KEY_4
LMS2=250

# button 3
SW3=13
SH3=KEY:KEY_5
LO3=KEY:KEY_6
LMS3=250

# button rotary 1
SW4=17
SH4=KEY:KEY_SPACE				# key-command for SHORT press(play/pause)
LO4=KEY:KEY_LEFTBRACE			# key-command for LONG press(special menu)
LMS4=250

# button rotary 2
SW5=12
SH5=KEY:KEY_ENTER				# key-command for SHORT press(enter, OK)
LO5=KEY:KEY_ESC					# key-command for LONG press(back)
LMS5=250

# CMD="sbpd -v -f /home/tc/sbpd_commands.cfg \
CMD="sbpd -v \
b,$SW1,$SH1,2,0,$LO1,$LMS1 \	# b=button, $SW1=switchnumber of button-section
b,$SW2,$SH2,2,0,$LO2,$LMS2 \
b,$SW3,$SH3,2,0,$LO3,$LMS3 \
b,$SW4,$SH4,2,0,$LO4,$LMS4 \
b,$SW5,$SH5,2,0,$LO5,$LMS5 \
e,22,27,VOLU,2 \				# e=encoder, 22 and 27 are GPIO
e,23,24,KEY:KEY_UP-KEY_DOWN,4 "	# e=encoder, 23 and 24 are GPIO

echo $CMD
$CMD > /dev/null 2>&1 &
{{< /code >}}

- Save your changes and exit nano (Ctrl+X and confirm by pressing y).
- Make `sbpd-script.sh` executable:

```text
sudo chmod +x /home/tc/sbpd-script.sh
```

##### Step 3 - User command in pCP web-gui

- Go to pCP web interface and click [Tweaks] tab.
- Scroll to section User commands and add

```text
/home/tc/sbpd-script.sh
```

- to user command #1 (bottom of the 'Tweaks' page), then click [Save].

![usercommand](sbpd-7.jpg)

- That's it. After a reboot, your rotary encoders and pushbuttons should work as expected.


## Additional hints

- The script is an example. You can change the GPIO's in "button sections" for your purpose.
- For rotary encoders, edit the GPIO's in the command section of the `sbpd-script.sh`.
- If your rotary encoders work in wrong direction, swap the GPIO wiring.
- Most rotary encoders have a pushbutton. You can use it to trigger a key command.
- The numbers after SW1 – SW 5= are the GPIO of your RPi (see wiring table above).
- Every button triggers two jive actions (short and long press, SH and LO).
- The quantity of buttons is only limited by quantity of available GPIO's.
- The key command KEY:KEY_1 triggers a preset. For that purpose, you need to add presets in LMS Web interface for this player (settings/player/presets).
- Only commands by rotary? Do not use Grid Skins. They need additional left/right key commands.


## More information

- [Add a 1.54 inch Display](/projects/add-a-1-54-inch-spi-display/)
- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [Edit config.txt](/how-to/edit_config_txt/)
- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [Raspberry Pi config.cfg](http://elinux.org/index.php?title=RPiconfig)
- [Waveshare 4.1 TFT + piCorePlayer + Jivelite](/projects/waveshare-tft-pcp-jivelite/)
- [Documentation / fb / fbcon.txt](https://mjmwired.net/kernel/Documentation/fb/fbcon.txt#72)
