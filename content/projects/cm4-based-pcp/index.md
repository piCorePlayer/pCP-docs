---
title: CM4 based pCP
description: CM4 based piCorePlayer
date: 2022-04-18T21:55:02.105Z
lastmod: 2024-06-24T23:24:11.670Z
author: pCP Team
weight: 220
pcpver: 8.0.0b8
toc: true
draft: false
categories:
  - Projects
tags:
  - CM4
---

{{< lead >}}
The aim of this project is to use a Raspberry Pi Compute Module 4 and Compute Module 4 IO Board as a different form factor piCorePlayer. These are standard Raspberry Pi products that are guaranteed to be available till 2028. Do they work? Are there advantages?
{{< /lead >}}

![Compute Module 4 IO Board](cm4-cm4io.png)

At the time of writing, there seems to be shortage in the availability of the Compute Module 4 and Compute Module 4 IO Boards. I have waited months for them to be available at the same time and there was only one or two of the 32 CM4 models in stock. 

{{< card style="warning" >}}
I have used a Compute Module 4 (CM4) Lite, so the SD card on the Compute Module 4 IO Board is enabled and allows piCorePlayer to be loaded just like any other Raspberry Pi. If you are using a non-lite version of the CM4 you will need to load piCorePlayer into eMMC.
{{< /card >}}


## What was used

### Hardware

- [Compute Module 4 - lite, 2GB, Wifi](https://www.raspberrypi.org/products/compute-module-4/)
- [Compute Module 4 IO Board](https://www.raspberrypi.org/products/compute-module-4-io-board/)
- [Compute Module 4 Antenna Kit](https://www.raspberrypi.org/products/compute-module-4-antenna-kit/)
- 12VDC 5A Power Supply
- SanDisk Ultra 16GB SD card---see [SD card](/components/sd_card/)
- Topping E30 USB DAC---see [Topping E30](https://www.tpdz.net/productinfo/434825.html)

### Software

- piCorePlayer 8.0.0---see [piCorePlayer Downloads](/downloads/)


## Initial tests

### HDMI

After I unpacked the CM4 and CM4 IO Board, I plugged the CM4 onto the CM4 IO Board, connected my TV to one of the HDMI ports, grabbed an SD card from my RPi4B and applied powered. Well, it just booted as normal and worked just like any other Raspberry Pi. A bit of an anti-climax.

I thought, while I have the CM4 next to the TV I would see what happens if I tried the other HDMI port. I have never tested any HDMI ports on any RPi4 before. Unfortunately it didn't boot. It was stuck at the splash screen. The splash screen also was not quite the right colour, with the red and green faded. There is probably a fix to this issue but for the moment I will move on.

It should be noted that the dual HDMI Raspberry Pi's have the HDMI ports numbered HDMI0 and HDMI1 on the silk screen on the PCB. In ALSA software, the HDMI ports are referred to as b1, HDMI 1 and b2, HDMI 2. An area of potential confusion.

### USB

My main HIFI system is currently using a RPi4B with a Topping E30 USB DAC, so I was keen to try the CM4 in place of the RPi4B.

A quick swap of the RPi4B and CM4 and power on. Nothing. The Topping E30, which gets it power from one of the USB ports, did not power on.

After a bit of research I found you need to add `otg_mode=1` to config.txt to turn on USB power. Once I did that everything worked as expected.

### IS2

I have not tried any Audio HATs yet.

### Wifi

The built-in wifi seems less reliable than the other Raspberry Pi's to date. Anecdotal evidence suggests it is more susceptible to interference from the neighbours wifi, requiring a channel change, and the microwave causes more severe streaming hiccups.

I do have the external aerial to try, but I want to persist with the built-in aerial for a few days to get a bit of a benchmark.


## Steps

##### Step 1 - Download pCP

- Download piCorePlayer---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2 - Create SD card

- Create an SD card with piCorePlayer image---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).

##### Step 3 - Setup Wifi

- Create wpa_supplicant.conf on boot partition---see [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/).
- Eject the SD card.

##### Step 4 - Connect Topping E30 USB DAC

- Connect the 2 USB cables (supplied) from the Raspberry Pi USB2 ports to Topping E30.
- Topping E30 was used with default settings.

{{< card style="warning" >}}
"By default there will be no power on USB ports.
{{< /card >}}

- Add `otg_mode=1` to config.txt---see [Edit config.txt](/how-to/edit_config_txt/)
- Reboot.

##### Step 5 - Boot pCP

- Insert the SD card into the Raspberry Pi CM4.
- Connect power cable.
- Turn the power on.

##### Step 6 - Determine the IP address

- Determine the IP address---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

##### Step 7 - Access pCP GUI

- Type `http://<IP address>` into web browser, **or**
- Type `http://pcp.local` into web browser.

##### Step 8 - Select USB audio

- Select [Squeezelite Settings] > "Audio output device settings" > "USB audio".
- Click [Save].
- This will set "Audio output device settings" to "USB audio" and set "Change Squeezelite settings" to default values.

{{< card style="danger" >}}
"Output setting" will be blanked and will need to be set manually.
{{< /card >}}

##### Step 9 - Set Output setting

- Click [Squeezelite Settings] > "Change Squeezelite settings" > "Output setting" > "more>".
- Note the list of available `CARD=E30` devices.
- Click on `hw:CARD=E30,DEV=0`.
- This will set "Output setting" to `hw:CARD=E30,DEV=0`.
- Click [Save].

##### Step 10 - Reboot

- Click Reboot when requested.

##### Step 11 - Play music

- Select track/playlist.
- Play music.


## Optional

If you have more than one piCorePlayer, it is recommended that you continue to the following steps.

##### Step 12 - Set player name

- Access pCP using IP address in a browser (ie. http://192.168.1.xxx or http://pcp.local).
- Select [Squeezelite Settings] > "Change Squeezelite settings" > "Name of your player".
- Type player name `pCP CM4`.
- Click [Save].

##### Step 13 - Set hostname

- Select [Tweaks] > "pCP System Tweaks" > "Host name".
- Type host name `pCPCM4`.
- Click [Save].
- Click [Yes] when requested to "Reboot piCorePlayer".

##### Step 14 - Reboot

- Click Reboot when requested.


## Future tests

- RTC
- 4 wire fan


## More information

- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
