---
title: Add a 4 inch Waveshare display spi touch
description: null
date: 2022-06-21T21:45:13.560Z
lastmod: 2024-06-24T23:13:36.767Z
author: mini11
weight: 105
pcpver: 7.0.0
toc: true
draft: false
categories:
  - Projects
tags:
  - Display
  - Waveshare
  - jivelite
---

{{< lead >}}
Waveshare 4 inch resistive display, spi Touch, 480×800, HDMI, with piCorePlayer.
**Note** If upgrading from pCP version 9.2 or lower, or if installing fresh on pCP 10 or higher, be sure to read the important note below, otherwise jivelite will fail to write out its settings.
{{< /lead >}}

![4 inch display](display-1.jpg)

![4 inch display](display-2.jpg)


## Steps

##### Step 1 - Connect the display to your Raspberry PI

- Plug the display directly on the GPIO pins of your RPi and connect the HDMI plug with an HDMI-adaptor.

![4 inch display](display-3.jpg)

If you need additional pins for other purpose, wire the pins 1, 2, 6, 19, 21, 22, 23, 26) and connect the HDMI-Plug with a HDMI Cable.
 
{{< table style="table-striped" >}}
| Pin | Symbol | Discription                      |
|-----|--------|----------------------------------|
| 1   | 3,3 V  | Power positive (3.3V power input)|
| 2   | 5 V    | Power positive (5V power input)  |
| 6   | GND    | Ground                           |
| 19  | TP_SI  | SPI data input of Touch Panel    |
| 21  | TP_SO  | SPI data output of Touch Panel   |
| 22  | TP_IRQ | Touch Panel interrupt            |
| 23  | TP_SCK | SPI clock of Touch Panel         |
| 26  | TP_CS  | Touch Panel chip selection       |
{{< /table >}}

##### Step 2 - Prepare SD card

- Put a fresh pCP image on to the SD card---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).
- While the SD card is still in the laptop/pc:
	- Enter wifi credentials in wpa_supplicant.conf.sample at the end of the file and "save as" wpa_supplicant.conf.

```text
# This is the details about your Wireless network. Configure as needed.
# Most will just need to change the ssid and psk.

network={
        ssid="your ssid"
        psk="your password"
        key_mgmt=WPA-PSK
        auth_alg=OPEN
```
	- Add the following lines to config.txt, in the Custom Configuration area at the end of the file (between the Begin-Custom and End-Custom lines).

```text
#---Begin-Custom-(Do not alter Begin or End Tags)-----
disable_splash=1
avoid_warnings=2
gpu_mem=128 
hdmi_group=2
hdmi_mode=87
hdmi_cvt 480 800 60 6 0 0 0
dtoverlay=ads7846,cs=1,penirq=25,penirq_pull=2,speed=50000,keep_vref_on=0,swapxy=0,pmax=255,xohms=150,xmin=200,xmax=3900,ymin=200,ymax=3900
display_rotate=3
#---End-Custom------------------------
```

- Comment out the `gpu_mem=16` and `lcd_rotate=2` default settings by putting an # at the beginning of the line.
- Save your changes.

{{< card style="info" >}}
You find these two files in your Windows Explorer in root-section of the pcp_boot drive.
{{< /card >}}

##### Step 3 - Boot Raspberry Pi

- Put the SD card in your RPi and boot.
- Find the IP address of the RPi, and enter this in a browser---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).
- Once the RPi has booted up, this should bring up the pCP web interface.
	- On [Main Page] scroll down to "Additional functions" and click on "Extensions". Wait for green ticks, then click on the [Available] tab.
	
![devices](display-4.jpg)

- In the 'Available extensions in the piCorePlayer repository' section.
	
![devices](display-5.jpg)

- Load `nano.tcz` extension.
- Install Jivelite from the [Tweaks page] (this step includes an automatic backup, so accept the reboot	prompt.)
- After the reboot do not configure jivelite – we’ll do this later!

##### Step 4 - Make a custom jivelite.sh startup script
**Note** This has changed for pCP version 10 and higher. The jivelite startup script is now considerably more sophisticated, and you may not need a custom jivelite script at all. If you do find you need one, be sure to include the line starting 'export HOME ...' as shown below.

- Login to your RPi via ssh (software=putty – user: tc, password: piCore) and look, if your changes of step 2 work:
- Insert the following command:

\$ `cat /proc/bus/input/devices`
	
- You see your connected input-devices (ie. touchscreen, mouse or IR-receiver)

![devices](display-6.jpg)

- Take note of "Waveshare" and "event1" (If you do not use additional input devices (ie. IR-receiver), the input device may be event0).

##### Step 5 - Creating the jivelite.sh

- Still connected by ssh, insert the following command:

\$ `sudo nano /mnt/mmcblk0p2/tce/jivelite.sh`

- Put into the empty file (copy and paste) the following script:

{{< code lang="shell" >}}
#!/bin/sh
# Disable mouse pointer
export JIVE_NOCURSOR=1
# Set touch calibration file
TSLIB_CALIBFILE=/usr/local/etc/pointercal
# Jivelite interface framerate
export JIVE_FRAMERATE=22
# Set Touch input device 
export TSLIB_TSDEVICE=/dev/input/event1 # This might be event0 in some cases
export SDL_MOUSEDRV=TSLIB
# Define custom Skin size
# scale to 800x480. This could be any size to scale interface.
export JL_SCREEN_WIDTH=800
export JL_SCREEN_HEIGHT=480
# Set custom screen resolution
/usr/sbin/fbset -xres 800 -yres 480

export HOME=/home/tc

# Run Jivelite
while true; do
    /opt/jivelite/bin/jivelite >> /var/log/jivelite.log 2>&1
    sleep 3
done
{{< /code >}}

- Usually we do not need a script for that resolution in Jivelite. It is standard and many skins work with that resolution. For this purpose, we need the script to define the touchscreen as input-device and to hide the cursor.
- Save your changes and exit nano (Ctrl+X and confirm by pressing y).

{{< card style="info" >}}
- I don’t know, if the setting of the framerate is necessary – you can play with that.
- You can find the original jivelite.sh script at [THE FAN CLUB
dynamic design solutions](https://www.thefanclub.co.za/how-to/how-setup-picoreplayer-jivelite-use-waveshare-55-amoled-touchscreen).
{{< /card >}}

- Make the new custom jivelite.sh script executable with:

 \$ `sudo chmod +x /mnt/mmcblk0p2/tce/jivelite.sh`

- Go to Web-Gui of piCorePlayer, section [Tweaks] and set "Jivelite Autostart" to "no".
- Reboot your RPi.

##### Step 6 - Calibrate Touchscreen

- Login to your RPi via ssh (putty – user: tc, password: piCore)---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).
- Insert the following command:

 \$ `sudo TSLIB_FBDEVICE=/dev/fb0 TSLIB_TSDEVICE=/dev/input/event1 /usr/local/bin/ts_calibrate`

- Click the 5 calibration points properly.
- Make your calibration persistent:

\$ `pcp bu`

- Go to the WebGui of piCorePlayer, section [Tweaks] and set "Jivelite Autostart" to "yes".
- Reboot your RPi.
	- After the reboot, the touch of the display should work fine.

##### Step 7 - Configure Jivelite

- Connect a mouse or an IR-remote to the device to configure jivelite.
- Choose your language and in the next menu select `GridSkin 800x480` or `jogglerskin`. Both work fine with that resolution.
- Configure the additional settings (time, background and so on).
- Save  your changes in Jivelite Settings (Settings > piCorePlayer > Save Settings to SD-Card).


## More information

- [4 inch Capacitive Touch Display](https://www.waveshare.com/product/displays/lcd-oled/lcd-oled-2/4inch-hdmi-lcd.htm)
- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [Edit config.txt](/how-to/edit_config_txt/)
- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [Raspberry Pi config.cfg](http://elinux.org/index.php?title=RPiconfig)
- [Waveshare 4.1 TFT + piCorePlayer + Jivelite](/projects/waveshare-tft-pcp-jivelite/)
- [Documentation / fb / fbcon.txt](https://mjmwired.net/kernel/Documentation/fb/fbcon.txt#72)
