---
title: Slimmer
description: null
date: 2024-05-06T11:23:52.374Z
lastmod: 2024-06-24T23:32:57.950Z
author: pCP Team
weight: 40
pcpver: 9.0.0
toc: true
draft: false
categories:
    - Projects
tags:
    - Slimmer
---

{{< lead >}}
Slimmer is a user interface controller software for Lyrion Media Server. Using Slimmer and other open source software and some hardware components you can build a high quality & low-cost network audio player with a slick user interface.
{{< /lead >}}

![Now playing screen](nowplaying.jpg)
![Main menu](menumain.jpg)


## More information

- [Slimmer - GitHub](https://github.com/terba/slimmer/wiki/)
- [Announce: Slimmer - Forum](https://forums.slimdevices.com/showthread.php?105819-Announce-Slimmer&p=856854&viewfull=1#post856854)
