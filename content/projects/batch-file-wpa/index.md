---
title: Batch file for WPA
description: Batch file to create minimal wpa_supplicant.conf
date: 2021-06-28
author: pCP Team
weight: 160
pcpver: "8.0.0-b7"
toc: true
draft: false
categories:
- Projects
tags:
- wpa
- batch
- netusb
---

{{< lead >}}
A  MSDOS batch file for Windows 10 to create a basic `wpa_supplicant.conf` configuration file and/or a `netusb` file on the `PCP_BOOT` partition of your SD card.
{{< /lead >}}

{{< card style="warning" >}}
Advanced users only. Use at your own risk!
{{< /card >}}

I create a lot of piCorePlayer images on SD cards and needed a method to quickly and consistently add a minimal `wpa_supplicant.conf` configuration file to a freshly created SD card. After years of thinking about it, I decided it was time to create a batch file to quickly create this configuration file. While I was at it, I added an option to add a `netusb` file as well.

I don't expect this is useful for most piCorePlayer users, so it is not part of piCorePlayer, but just in case someone had a similar thought, I decided to publish this script to reduce some of the effort of learning MSDOS batch scripting. You will need to copy and paste the following into a batch file and then edit the SSID and PSK variables to suit.

```batch
::---------------------------------------------------------------------------------------
:: wpasupplicant.bat
::---------------------------------------------------------------------------------------
:: Written by:  Greg Erskine
:: Date:        4 June 2021
:: Version:     V1.0
::---------------------------------------------------------------------------------------
:: This batch file will create wpa_supplicant.conf and netusb on the 'PCP_BOOT'
:: partition of your SD card.
::---------------------------------------------------------------------------------------
@echo off

::---------------------------------------------------------------------------------------
:: Check if Windows XP or later
::---------------------------------------------------------------------------------------
if not "%OS%"=="Windows_NT" goto :EOF

::---------------------------------------------------------------------------------------
:: Modify SSID and PSK to suit
::---------------------------------------------------------------------------------------
set "SSID=ssid="yourSSID""
set "PSK=psk="yourPASSWORD""

::--------------------------------------------------------------------------------------- 
:: Find all drive letters using command 'FSUTIL FSINFO DRIVES'.
::  - 1. format 'Drives: C:\ D:\ E:\ F:\'
::  - 2. format 'C:\ D:\ E:\ F:\'
::  - 3. format 'C: D: E: F:'
::---------------------------------------------------------------------------------------
set "DRIVES="
for /F "tokens=1*" %%C in ('fsutil fsinfo drives') do (
	set "DRIVES=%%D"
)
set "DRIVES=%DRIVES:\=%"

::---------------------------------------------------------------------------------------
:: Display drives found and drive type
::---------------------------------------------------------------------------------------
for %%D in (%DRIVES%) do (
	fsutil fsinfo drivetype %%D
)
echo.

::---------------------------------------------------------------------------------------
:: Look for partition label 'PCP_BOOT'
::---------------------------------------------------------------------------------------
set "DRIVE="
for %%D in (%DRIVES%) do (
	vol %%D 2>NUL | find "PCP_BOOT" >NUL
	if not errorlevel 1 set "DRIVE=%%D"
)

if "%DRIVE%"=="" (
	goto :NOTFOUND
) else (
	goto :FOUND
)

::---------------------------------------------------------------------------------------
:: PCP_BOOT partition not found, cannot continue
::---------------------------------------------------------------------------------------
:NOTFOUND
	echo PCP_BOOT partition not found.
	echo Insert piCorePlayer SD card.
	echo Ignore all format warning messages.
	echo.
	choice /c yn /n /m "Exit? y"
	goto :EOF
::---------------------------------------------------------------------------------------

::---------------------------------------------------------------------------------------
:: PCP_BOOT partition found, write wpa_supplicant.conf
::---------------------------------------------------------------------------------------
:FOUND
	echo PCP_BOOT partition found on %DRIVE%
	echo.
	choice /c yn /n /m "Write wpa_supplicant.conf to %DRIVE% ? y/n"
	if %ERRORLEVEL% equ 1 goto :WRITE1
	goto :NETUSB

:WRITE1
	echo %SSID% > %DRIVE%\wpa_supplicant.conf
	echo %PSK% >> %DRIVE%\wpa_supplicant.conf
::---------------------------------------------------------------------------------------

::---------------------------------------------------------------------------------------
:: PCP_BOOT partition found, write netusb
::---------------------------------------------------------------------------------------
:NETUSB
	echo.
	choice /c yn /n /m "Write netusb to %DRIVE% ? y/n"
	if %ERRORLEVEL% equ 1 goto :WRITE2
	goto :EOF

:WRITE2
	echo. > %DRIVE%\netusb
	goto :EOF
::---------------------------------------------------------------------------------------
```

```text
C: - Fixed Drive
D: - Fixed Drive
E: - Fixed Drive
F: - Removable Drive
G: - Removable Drive

PCP_BOOT partition found on F:

Write wpa_supplicant.conf to F: ? y/n Y

Write netusb to F: ? y/n N
```


## More information

- [DosTips - The DOS Batch Guide](https://www.dostips.com/)
- [Rob van der Woude's Scripting Pages](https://www.robvanderwoude.com/batchstart.php)
- [Windows Batch Scripting](https://en.wikibooks.org/wiki/Windows_Batch_Scripting)
- [Using batch files](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-xp/bb490869(v%3dtechnet.10))
