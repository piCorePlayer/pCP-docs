---
title: RPi0W with HDMI TV and Jivelite
description: Raspberry Pi Zero W with HDMI TV and Jivelite
date: 2023-01-01T19:55:03.361Z
lastmod: 2024-06-24T23:31:03.634Z
author: pCP Team
weight: 240
pcpver: 8.2.0
toc: true
draft: false
categories:
        - Projects
tags:
        - HDMI
        - Jivelite
---

{{< lead >}}
This project is to install piCorePlayer onto a Raspberry Pi Zero W and connect it to a Panasonic TV via HDMI. Also, we would like to add Jivelite for displaying "Now Playing" information.
{{< /lead >}}

{{< card style="warning" >}}
Since the introduction of the Raspberry Pi 4B, the HDMI configuration has changed and is continually being developed. This has resulted in a simple piCorePlayer configuration becoming a lot more complicated.  
{{< /card >}}


## What was used

### Hardware

- [Raspberry Pi Zero W](https://www.raspberrypi.com/products/raspberry-pi-zero-w/)
	- Cheap
	- Single core so 32-bit only
	- No ethernet connector
	- No Headphones Jack
	- No default sound card
	- Only one USB connector
- SanDisk Ultra 8GB SD card---see [SD card](/components/sd_card/).
- Panasonic TV
- Keyboard

### Software

- piCorePlayer 8.2.0 32-bit Kernel---see [piCorePlayer Downloads](/downloads/).
	- Jivelite

### Network Diagram

<div class="mermaid">
	graph TD
		Router[Wifi router]
		Router --> |Wifi| RPi[Raspberry Pi Zero W - pCP]
		RPi --- |HDMI| TV[Panasonic TV]
</div>


## Steps

##### Step 1 - Download piCorePlayer

{{< card style="warning" >}}
The Raspberry Pi Zero W has a single core processor, so you must use the 32-bit Kernel version of piCorePlayer.
{{< /card >}}

- Download piCorePlayer 32-bit Kernel---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2 - Create SD card

- Create an SD card with piCorePlayer image---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).

##### Step 3 - Setup Wifi

{{< card style="warning" >}}
The Raspberry Pi Zero W does not have a wired ethernet connector, so you must enable wifi.
{{< /card >}}

- Create wpa_supplicant.conf on boot partition---see [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/).
- Eject the SD card.

##### Step 4 - Boot piCorePlayer

- Insert the SD card into the Raspberry Pi Zero W.
- Connect the Panasonic TV to Raspberry Pi HDMI connector.

{{< card style="warning" >}}
The Raspberry Pi Zero W has 2 USB connectors. For power, you must you the USB connector marked `PWR` closest to the edge of the PCB.
{{< /card >}}

- Connect the power cable to the Raspberry Pi.
- Turn the power on.

{{< card style="warning" >}}
The Raspberry Pi Zero W has no default sound card so you will get the following boot error message:

`The Headphones audio device is not compatible with Rpi<Zero><Compute><400> Boards. Audio will need configured.`

Because there is no sound card, squeezelite will also fail to start.
{{< /card >}}

##### Step 5 - Determine the IP address

- Determine the IP address---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

##### Step 6 - Access piCorePlayer GUI

- Type `http://<IP address>` into web browser, **or**
- Type `http://pcp.local` into web browser.

##### Step 7 - Select USB audio

- Select [Squeezelite Settings] > "Audio output device settings" > "HDMI audio".
- Click [Save].
- This will set "Audio output device settings" to "HDMI audio" and set "Change Squeezelite settings" to default values.

##### Step 8 - Set Output setting

{{< card style="danger" >}}
The "Output setting" will be set to `sysdefault:CARD=b1,DEV=0` which will not work. You must reset "Output setting" to `sysdefault:CARD=b1`. Note the `,DEV=0` is removed.
{{< /card >}}

- Click [Squeezelite Settings] > "Change Squeezelite settings" > "Output setting" > "more>".
- Note the list of available `output devices`.

{{< highlight "text" "hl_lines=10" >}}
Available output devices (click to use):

hw: devices are normally the best choice, but try and decide for yourself:

- default
- plugequal
- equal
- hw:CARD=b1,DEV=0
- plughw:CARD=b1,DEV=0
- sysdefault:CARD=b1
{{< /highlight >}}

{{< card style="warning" >}}
The `CARD=b1` options will only appear if the HDMI cable is plugged in.
{{< /card >}}

- Click on `sysdefault:CARD=b1`.
- This will set the "Output setting" to `sysdefault:CARD=b1`.
- Click [Save].

##### Step 9 - Reboot

- Click [Reboot] when requested.

##### Step 10 - Play music

- Select track/playlist.
- Click [Play].

##### Step 11 - Set player name

- Access piCorePlayer using the IP address in a browser (ie. http://192.168.1.xxx or http://pcp.local).
- Select [Squeezelite Settings] > "Change Squeezelite settings" > "Name of your player".
- Type player name `pCP HDMI`.
- Click [Save].

##### Step 13 - Set hostname

- Select [Tweaks] > "pCP System Tweaks" > "Host name".
- Type host name `pCPHDMI`.
- Click [Save].
- Click [Yes] when requested to "Reboot piCorePlayer".

##### Step 14 - Reboot

- Click [Reboot] when requested.

{{< card style="success" >}}
Stop here if you don't need Jivelite.
{{< /card >}}

##### Step 15 - Resize FS

- Select [Main Page] > [Resize FS].
- Select [100 MB].
- Click [Resize].

##### Step 16 - Load Jivelite

- Select [Tweaks] > "Jivelite Settings" > [Install].

##### Step 17 - Reboot

- Click [Reboot] when requested.

{{< card style="danger" >}}
You should see the piCorePlayer logo screen but Jivelite will not continue to load. This is a result of the development of HDMI drivers since the Raspberry Pi 4B.
{{< /card >}}

##### Step 18 - Configure config.txt

{{< card style="warning" >}}
config.txt needs changing to accommodate the display of Jivelite.
{{< /card >}}

- Edit config.txt---see [Edit config.txt](/how-to/edit_config_txt/).

\$ `m1`

\$ `c1`

\$ `vicfg`

- Comment out `dtparam=audio=on`.
- Comment out `audio_pwm_mode=2`.
- Remove comment from `#dtoverlay=vc4-kms-v3d`.

From this:
{{< highlight "text" "linenos=table,hl_lines=2 3 10,linenostart=79" >}}
# onboard audio overlay
dtparam=audio=on
audio_pwm_mode=2

# For Jivelite use with HDMI screens
# Comment out the lines above
#   gpu_mem=xx
dtparam=audio=on
# Then uncomment the following line.
#dtoverlay=vc4-kms-v3d
#
{{< /highlight >}}

To this:
{{< highlight "text" "linenos=table,hl_lines=2 3 10,linenostart=79" >}}
# onboard audio overlay
#dtparam=audio=on
#audio_pwm_mode=2

# For Jivelite use with HDMI screens
# Comment out the lines above
#   gpu_mem=xx
dtparam=audio=on
# Then uncomment the following line.
dtoverlay=vc4-kms-v3d
#
{{< /highlight >}}

##### Step 19 - Reboot

- Click [Reboot] or type \$ `pcp br`.

##### Step 20 - Configure Jivelite

- Plug in keyboard.
- On the Jivelite screen:
	- Select Language.
	- Select Skin.
		- HD Grid Skin (1920x1080)
		- HD Skin (1920x1080)	

##### Step 21 - Save Jivelite configuration

- Jivelite does not automatically save the Jivelite changes, so you must do a backup.
- On the Jivelite screen:
	- Select [Settings] > [piCorePlayer] > “Save Settings to SD Card”.
	- For alternative backup options---see [piCorePlayer backup](/information/pcp_backup/)

##### Step 22 - Fix HDMI audio out

{{< card style="warning" >}}
We now have Jivelite displaying nicely but HDMI sound output has been broken.

Note that the ALSA CARD name has changed from `b1` to `vc4hdmi`. This will cause the problems with Squeezelite.
{{< /card >}}

- Click [Squeezelite Settings] > "Change Squeezelite settings" > "Output setting" > "more>".
- Note the list of available `output devices`.

{{< highlight "text" "hl_lines=6" >}}
Available output devices (click to use):

hw: devices are normally the best choice, but try and decide for yourself:

- sysdefault
- iec958
- default
- plugequal
- equal
- hw:CARD=vc4hdmi,DEV=0
- plughw:CARD=vc4hdmi,DEV=0
- sysdefault:CARD=vc4hdmi
- front:CARD=vc4hdmi,DEV=0
- iec958:CARD=vc4hdmi,DEV=0
{{< /highlight >}}

- Click on `iec958`.
- This will set the "Output setting" to `iec958`.
- Click [Save].

{{< card style="success" >}}
Congratulations!
{{< /card >}}


## Jivelite keyboard mapping

##### Extract From InputToActionMap.lua

It can be beneficial to use a keyboard for debugging process. Here is an extract from `InputToActionMap.lua` that shows the mapping of the keyboard to LMS commands.

```text
charActionMappings.press = {
--BEGIN temp shortcuts to test action framework
        ["["]  = "go_now_playing",
        ["]"]  = "go_playlist",
        ["{"]  = "go_current_track_info",
        ["`"]  = "go_playlists",
        [";"]  = "go_music_library",
        [":"]  = "go_favorites",
        ["'"]  = "go_brightness",
        [","]  = "shuffle_toggle",
        ["."]  = "repeat_toggle",
        ["|"]  = "sleep",
        ["Q"]  = "power",
--END temp shortcuts to test action framework

--alternatives for common control buttons avoiding keyboard modifiers
        ["f"]  = "go_favorites",
        ["s"]  = "sleep",
        ["q"]  = "power",
        ["k"]  = "power_on",
        ["i"]  = "power_off",
        ["t"]  = "go_current_track_info",
        ["n"]  = "go_home_or_now_playing",
        ["m"]  = "create_mix",
        ["g"]  = "stop",
        ["d"]  = "add_end",
        ["y"]  = "play_next",
        ["e"]  = "scanner_rew",
        ["r"]  = "scanner_fwd",
        ["u"]  = "mute",
        ["o"]  = "quit",

-- original
        ["/"]   = "go_search",
        ["h"]   = "go_home",
        ["J"]   = "go_home_or_now_playing",
        ["D"]   = "soft_reset",
        ["x"]   = "play",
        ["p"]   = "play",
        ["P"]   = "create_mix",
        [" "]   = "pause",
        ["c"]   = "pause",
        ["C"]   = "stop",
        ["a"]   = "add",
        ["A"]   = "add_end",
        ["W"]   = "play_next",
        ["M"]   = "mute",
        ["\b"]  = "back", -- BACKSPACE
        ["\27"] = "back", -- ESC
        ["j"]   = "back",
        ["l"]   = "go",
        ["S"]   = "take_screenshot",
        ["z"]  = "jump_rew",
        ["<"]  = "jump_rew",
        ["Z"]  = "scanner_rew",
        ["b"]  = "jump_fwd",
        [">"]  = "jump_fwd",
        ["B"]  = "scanner_fwd",
        ["+"]  = "volume_up",
        ["="]  = "volume_up",
        ["-"]  = "volume_down",
        ["0"]  = "play_preset_0",
        ["1"]  = "play_preset_1",
        ["2"]  = "play_preset_2",
        ["3"]  = "play_preset_3",
        ["4"]  = "play_preset_4",
        ["5"]  = "play_preset_5",
        ["6"]  = "play_preset_6",
        ["7"]  = "play_preset_7",
        ["8"]  = "play_preset_8",
        ["9"]  = "play_preset_9",
        [")"]  = "set_preset_0",
        ["!"]  = "set_preset_1",
        ["@"]  = "set_preset_2",
        ["#"]  = "set_preset_3",
        ["$"]  = "set_preset_4",
        ["%"]  = "set_preset_5",
        ["^"]  = "set_preset_6",
        ["&"]  = "set_preset_7",
        ["*"]  = "set_preset_8",
        ["("]  = "set_preset_9",
        ["?"]  = "help",

        --development tools -- Later when modifier keys are supported, these could be obscured from everyday users
        ["R"]  = "reload_skin",
        ["}"]  = "debug_skin",
        ["~"]  = "debug_touch",

}
```


## More information

- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Edit config.txt](/how-to/edit_config_txt/)
