---
title: GPIO
description: GPIO
date: 2024-05-06T11:20:19.536Z
lastmod: 2024-06-24T23:28:44.898Z
author: Greg Erskine
weight: 1010
pcpver: 8.2.1
toc: true
draft: true
categories:
  - Projects
tags:
  - GPIO
---

{{< lead >}}
GPIO
{{< /lead >}}


pcp   |   pcp_lms_functions   |  LMS Command Line Interface

LMS Command Line Interface

pcp_lms_functions - a library of LMS functions used by piCorePlayer programs. Only a subset of the LMS Command Line Interface is implemented in the pcp_lms_functions library.

pcp - piCorePlayer CLI - a simple shell script that uses a few of the common functions in pcp_lms_functions. Only a subset of functions in pcp_lms_functions are implemented in the pCP CLI---see [piCorePlayer CLI](/information/pcp_cli/) 


## LMS mode command

From the Lyrion Media Server Command Line Interface documentation:

```text
<playerid> mode ?

The "mode" command allows to query the player state and returns one of "play", "stop" or "pause". If the player is off, "mode ?" returned value is undefined.

Example:

Request: "04:20:00:12:23:45 mode ?<LF>"
Response: "04:20:00:12:23:45 mode stop<LF>"
```


```
          | IMODE   | [PLAY]  | [PAUSE] | [STOP]  |
  |-------|---------|---------|---------|---------|
1 | MODE  | play    | play    | pause   | stop    |
2 | MODE  | pause   | play    | play    | stop    |
3 | MODE  | stop    | play    | play    | stop    |

IMODE   = initial mode
[PLAY]  = play button or command
[PAUSE] = pause button or command
[STOP]  = stop button or command
```

##### Test 1.
```
$ pcp play
$ pcp mode
play
$ pcp play
$ pcp mode
play

$ pcp play
$ pcp mode
play
$ pcp pause
$ pcp mode
pause

$ pcp play
$ pcp mode
play
pcp stop
$ pcp mode
stop
```

##### Test 2.
```
$ pcp pause
$ pcp mode
pause
$ pcp play
$ pcp mode
play

$ pcp pause
$ pcp mode
pause
$ pcp pause
$ pcp mode
play

$ pcp pause
$ pcp mode
pause
$ pcp stop
$ pcp mode
stop
```

##### Test 3.
```
$ pcp stop
$ pcp mode
stop
$ pcp play
$ pcp mode
play

$ pcp stop
$ pcp mode
stop
$ pcp pause
$ pcp mode
play

$ pcp stop
$ pcp mode
stop
$ pcp stop
$ pcp mode
stop
```


## LMS power command

From the Lyrion Media Server Command Line Interface documentation:

```text
<playerid> power <0|1|?|>

The "power" command turns the player on or off. Use 0 to turn off, 1 to turn on, ? to query and no parameter to toggle the power state of the player.
For remote streaming connections, the command does nothing and the query always returns 1.

Examples:

Request: "04:20:00:12:23:45 power 1<LF>"
Response: "04:20:00:12:23:45 power 1<LF>"

Request: "04:20:00:12:23:45 power ?<LF>"
Response: "04:20:00:12:23:45 power 1<LF>"
```

```
          | IMODE     | [POWER ON]  | [POWER OFF] | [POWER] (toggle) |
  |-------|-----------|-------------|-------------|------------------|
1 | MODE  | play      | play        | stop        | stop             |
2 | MODE  | pause     | pause       | stop        | stop             |
3 | MODE  | stop      | stop        | stop        | stop             |

IMODE       = initial mode
[POWER ON]  = power on command
[POWER OFF] = power off command
[POWER]     = power command
```

##### Test 1
```
$ pcp play
$ pcp mode
play
$ pcp power on
$ pcp mode
play

$ pcp play
$ pcp mode
play
$ pcp power off
$ pcp mode
stop

$ pcp play
$ pcp mode
play
$ pcp power
$ pcp mode
stop
$ pcp power
$ pcp mode
play
```

##### Test 2
```
$ pcp pause
$ pcp mode
pause
$ pcp power on
$ pcp mode
pause

$ pcp pause
$ pcp mode
pause
$ pcp power off
$ pcp mode
stop

$ pcp pause
$ pcp mode
pause
$ pcp power
$ pcp mode
stop
$ pcp power
$ pcp mode
pause
```

##### Test 3
```
$ pcp stop
$ pcp mode
stop
$ pcp power on
$ pcp mode
stop

$ pcp stop
$ pcp mode
stop
$ pcp power off
$ pcp mode
stop

$ pcp mode
stop
$ pcp stop
$ pcp mode
stop
$ pcp power
$ pcp mode
stop
$ pcp power
$ pcp mode
stop
```


## Controls

- pCP CLI---see [piCorePlayer CLI](/information/pcp_cli/)
- jivelite  Power On/Off button
- Material  Power On/Off button
- LMS GUI   Power On/Off button
- pCP GUI   play/pause


## More information

- [Feature request picoreplayer: Play state to GPIO](https://forums.slimdevices.com/showthread.php?110277-Feature-request-picoreplayer-Play-state-to-GPIO)
- [piCorePlayer CLI](/information/pcp_cli/)
