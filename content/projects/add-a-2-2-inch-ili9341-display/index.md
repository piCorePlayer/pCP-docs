---
title: Add a 2.2 inch display
description: Add a 2.2 inch ili9341 spi display
date: 2022-07-18T21:06:04.704Z
lastmod: 2024-06-24T23:16:39.684Z
author: mini11
weight: 100
pcpver: 8.2.0
toc: true
draft: false
categories:
    - Projects
tags:
    - Display
    - ili9341
    - spi
    - jivelite
---

{{< lead >}}
2.2 inch ili9341 spi LCD display, 320×240, with piCorePlayer.
{{< /lead >}}

![2.2 inch display](display-1.jpg)
![2.2 inch display](display-2.jpg)

{{< card style="info" >}}
It's a little bit tricky get those displays running under piCorePlayer. 
Other spi displays like Waveshare or Spotpear, that are fitted with the same ili9341 chip, may work too with this overlay.
I tested it with [Spotpear 2.4 inch spi LCD](https://www.spotpear.com/index/product/detail/id/81.html).
{{< /card >}}

- This display is perfect, if you want to build a small device like player or net radio showing the title or broadcast information.
- Together with a RPi Zero WH and a HiFiBerry MiniAMP, you get a complete solution.


## What we need

- Raspberry Pi 2B, 3, 4, or Zero WH
- 8GB Micro SD card
- Cheap 2.2 inch spi display, 320x240, ili9341, no touch
- Some jumper wires, female to female
- Power supply, 5V, 2,5A
- PC or laptop, with putty installed


## Steps

##### Step 1 - Connect the display

- Connect the display to your RPi with jumper wires.

![connections](display-3.jpg)

{{< card style="info" >}}
It's recommended to connect BLK pin with a resistor >= 56 Ohms.
You can connect BLK and VCC to the same pin (17), make a bridge.
{{< /card >}}

##### Step 2 - Prepare SD card

- Put a fresh pCP8 image on to the SD card---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).
- While the SD card is still in the laptop/pc, edit the following three files:

{{< card style="info" >}}
You find these three files in your Windows Explorer in root-section of the pcp_boot drive.
{{< /card >}}

![boot](display-4.jpg)

- Enter wifi access data in `wpa_supplicant.conf.sample` and "save as" `wpa_supplicant.conf`.

```text
# This is the details about your Wireless network. Configure as needed.
# Most will just need to change the ssid and psk.

network={
        ssid="Your wifi"
        psk="some password"
        key_mgmt=WPA-PSK
        auth_alg=OPEN
}
```

{{< card style="warning" >}}
Do not delete the quotation marks!
{{< /card >}}

- Open `cmdline.txt` file and add these parameters to the end. All of the parameters in `cmdline.txt` must be on one continuous line, with a space between each command.

```text
fbcon=map:10 fbcon=font:VGA8x16
```

- Add the following lines to config.txt, in the Custom Configuration area at the end of the file (between the Begin-Custom and End-Custom lines).

```text
#---Begin-Custom-(Do not alter Begin or End Tags)-----
disable_splash=1
avoid_warnings=2
dtoverlay=ili9341
#---End-Custom------------------------
```

- Comment out the `lcd_rotate=2` default settings by putting an # at the beginning of the line.
- Save your changes.

##### Step 3 - Install additional extensions

- Put the SD card in your RPi and boot.
- Find the IP address of the RPi---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).
- Login to your RPi via ssh (software=putty – enter the IP address, click connect, user: tc, password: piCore) and enter the following commands:

```text
tce-load -wi pcp-jivelite_default-qvga240squareskin.tcz
```
- Installs the `pcp-jivelite_default-qvga240squareskin.tcz` extension. The skin is perfect to configure Jivelite with this display.
- Otherwise Jivelite will start the Joggler skin as default. This will not work at all with this display.

```text
tce-load -wi nano.tcz
```
- Installs the `nano.tcz` editor extension.

```text
tce-load -wi dtc.tcz
```
- Installs the `dtc.tcz` extension. We need it to compile the overlay.

##### Step 4 - Create a custom overlay

- To get those displays running under pCP 8, we need an overlay. The overlays coming with pCP do not match.
- Still connected by ssh, enter the following commands:
	- $ `m1`
	- $ `c1`
	- $ `cd overlays`
	- $ `sudo nano ili9341.dts`
 - Copy the following script and paste it into the empty document:

{{< code lang="shell" >}}
/dts-v1/;
/plugin/;

/ {
	compatible = "brcm,bcm2835";

	// Enable the SPI controller
	fragment@0 {
		target = <&spi0>;
		__overlay__ {
			status = "okay";
		};
	};

	// Enabling the SPI controller also enables spidev on cs0, so we have disable it
	fragment@1 {
		target = <&spidev0>;
		__overlay__ {
			status = "disabled";
		};
	};

	fragment@2 {
		target = <&spi0>;
		__overlay__ {
			/* needed to avoid dtc warning */
			#address-cells = <1>;
			#size-cells = <0>;

			display: display@0{
				// MIPI DBI compatible driver
				// This driver is used because it can ignore an illegal rotate value
				// and doesn't have a set_gamma() function.
				compatible = "ilitek,ili9341";
				reg = <0>; // Chip Select 0

				spi-max-frequency = <32000000>; // 32MHz

				reset-gpios = <&gpio 27 1>; // reset is active low hence the 1
				dc-gpios = <&gpio 22 0>;

				width = <320>;
				height = <240>;
				buswidth = <8>; // Most controllers use this value

				// This is a hack to prevent fb_s6d02a1.c:set_var() from issuing
				// command 0x36 after the init sequence has been applied and thus
				// changing the config.
				rotate = <1>;

				// see drivers/staging/fbtft/fb_ili9341.c:init_display() for how this looks in the driver
				// Command 0x36 is taken from the fb_ili9341.c:set_var() function.
				init = <0x1000001
					0x2000005
					0x1000028
					0x10000cf 0x00 0x83 0x30
					0x10000ed 0x64 0x03 0x12 0x81
					0x10000e8 0x85 0x01 0x79
					0x10000cb 0x39 0x2c 0x00 0x34 0x02
					0x10000f7 0x20
					0x10000ea 0x00 0x00
					0x10000c0 0x26
					0x10000c1 0x11
					0x10000c5 0x35 0x3e
					0x10000c7 0xbe
					0x100003a 0x55
					0x1000036 0x28
					0x10000b1 0x00 0x1b
					0x1000026 0x01
					0x10000f2 0x08
					0x1000026 0x01
					0x10000e0 0x1f 0x1a 0x18 0x0a 0x0f 0x06 0x45 0x87 0x32 0x0a 0x07 0x02 0x07 0x05 0x00
					0x10000e1 0x00 0x25 0x27 0x05 0x10 0x09 0x3a 0x78 0x4d 0x05 0x18 0x0d 0x38 0x3a 0x1f
					0x10000b7 0x07
					0x10000b6 0x0a 0x82 0x27 0x00
					0x1000011
					0x2000064
					0x1000029
					0x2000064>;

				debug = <0x4000000>; // print init commands to the kernel log
			};
		};
	};
};
{{< /code >}}

- Save your changes and exit nano (Ctrl+X and confirm by pressing y).
- Enter the following command:
	- $ `sudo dtc -@ -I dts -O dtb -o ili9341.dtbo ili9341.dts`
- This compiles the ili9341.dts file to ili9341.dtbo (overlay), means, you can use it in config.txt.
- Finished compiling, enter these commands:
	- $ `cd`
	- $ `u1`
	- $ `pcp bu`

##### Step 5 - Resize filesystem

- Bring up the pCP web interface by entering the IP address in your browser.
	- On [Main Page] scroll down and click [Resize FS].
	- In [Resize FS] menu choose 100 MB and click [Resize].
	
![resize_fs](display-5.jpg)

- This will take a couple of minutes and piCorePlayer will reboot a number of times.

##### Step 6 - Install Jivelite

- Back in pCP web interface, click on the [Tweaks] tab, find section "Jivelite" and "set Framebuffer" to "/dev/fb1".

![framebuffer](display-6.jpg)

- Install Jivelite (this step includes an automatic backup, so accept the reboot prompt.)
- After a reboot you should see the boot information. It takes about 10 seconds until the display shows boot info.

##### Step 7 - Configure Jivelite

- Connect a keyboard to your Raspberry Pi.
- You can do this with an IR remote too---see [Add an IR receiver to piCorePlayer](/projects/add-an-ir-receiver/).
- Set your language, choose QVGA – Square Skin and configure the additional settings (time, background and so on).
- Save your changes in Jivelite Settings (Settings > piCorePlayer > Save Settings to SD card).
- This display has no touch function, you will need other hardware to control jivelite.
- You can control by Smartphone (Squeezer App), with LMS Web-GUI, Jivelite for Windows or IR receiver connected to the device---see [More information](#more-information).
- It is even possible, to control jivelite by momentary buttons and/or rotary encoders---see [Control Jivelite by rotary encoders and buttons](/projects/control-jivelite-by-rotary-encoders-and-buttons/).


## Result

- All steps done? The result should look like this:

![now playing](display-7.jpg)


## More information

- [Add an IR receiver to piCorePlayer](/projects/add-an-ir-receiver/)
- [Control Jivelite by rotary encoders](/projects/control-jivelite-by-rotary-encoders-and-buttons/)
- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [Edit config.txt](/how-to/edit_config_txt/)
- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [Raspberry Pi config.cfg](http://elinux.org/index.php?title=RPiconfig)
- [Waveshare 4.1 TFT + piCorePlayer + Jivelite](/projects/waveshare-tft-pcp-jivelite/)
- [Documentation / fb / fbcon.txt](https://mjmwired.net/kernel/Documentation/fb/fbcon.txt#72)
