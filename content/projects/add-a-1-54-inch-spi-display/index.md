---
title: Add a 1.5 inch spi display
description: Add a 1.5 inch st7789 spi display
date: 2022-06-30T22:54:31.560Z
lastmod: 2024-06-24T23:16:10.619Z
author: mini11
weight: 90
pcpver: 8.0.0
toc: true
draft: false
categories:
  - Projects
tags:
  - Display
  - st7789
  - spi
  - jivelite
---

{{< lead >}}
1.5 inch st7789 spi LCD display, 240×240, with piCorePlayer.
{{< /lead >}}

![1.5 inch display](display-1.jpg)

{{< card style="info" >}}
This display comes with the pins unsoldered. You have to solder them yourself.
{{< /card >}}

- This display is perfect, if you want to build a small device-like player or net radio showing the title or broadcast information.
- Together with a RPi Zero W and a HiFiBerry MiniAMP or small mono DAC/amp Audio Amp SHIM (pic below), you get a complete solution.

![Audio amp shim](display-2.jpg)


## What we need

- Raspberry Pi 3, 4, or Zero WH
- 8GB Micro SD card
- Cheap 1.54 inch LCD display, 240x240
- Some jumper wires, female to female
- Powersupply, 5V, 2,5 A


## Steps

##### Step 1 - Connect the display

{{< card style="info" >}}
The display's interface is spi, even if pCP shows information normally used for i2c displays (SDA and SCL).
{{< /card >}}

- Connect the display to your RPi (see picture below).

![connections](display-3.jpg)

- As I do not need backlight adjustment, I didn’t connect the BLK pin.

##### Step 2 - Prepare SD card

- Put a fresh pCP8 image on to the SD card---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).
- While the SD card is still in the laptop/pc, edit the following three files:

{{< card style="info" >}}
You find these three files in your Windows Explorer in root-section of the pcp_boot drive.
{{< /card >}}

![files](display-4.jpg)

- Enter wifi access data in `wpa_supplicant.conf.sample` and "save as" `wpa_supplicant.conf`.

```text
# This is the details about your Wireless network. Configure as needed.
# Most will just need to change the ssid and psk.

network={
        ssid="Your wifi"
        psk="some password"
        key_mgmt=WPA-PSK
        auth_alg=OPEN
}
```

{{< card style="warning" >}}
Do not delete the quotation marks!
{{< /card >}}

- Open `cmdline.txt` file and add these parameters to the end. All of the parameters in `cmdline.txt` must be on one continuous line, with a space between each command.

```text
fbcon=map:10 fbcon=font:VGA8x16
```
- Add the following lines to `config.txt`, in the Custom configuration area at the end of the file (between the Begin-Custom and End-Custom lines).

```text
#---Begin-Custom-(Do not alter Begin or End Tags)-----
dtoverlay=st7789v-spi,dc_pin=9,reset_pin=25,cs=1,rotation=90
gpio=25=op,dh
#---End-Custom------------------------
```

- Unfortunately in pCP7 the overlay `st7789v-spi.dtbo` is not available, so you have to install it.
- Put the SD card in the RPi and boot the device.
- Login to your RPi via ssh (software=putty – user: tc, password: piCore) and enter the following commands:

```text
m1
c1
cd overlays/
wget https://repo.picoreplayer.org/insitu/piCorePlayer7.0.0/st7789v-spi.dtbo
cd
u1
```
- Reboot the device.

{{< card style="info" >}}
In piCorePlayer 8 the overlay `st7789v-spi.dtbo` is available, skip the overlay installation.
{{< /card >}}

##### Step 3 - Install additional extensions

- Put the SD card in your RPi and boot.
- Find the IP address of the RPi, and enter this in a browser---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).
- Once the RPi has booted up, this should bring up the pCP web interface.
	- On [Main Page] scroll down to "Additional functions" and click on "Extensions". Wait for green ticks.

![extensions](display-5.jpg)

- Click on the [Available] tab. In the "Available extensions in the piCorePlayer repository" section.

![available](display-6.jpg)

- Load the `nano.tcz` extension.
- Load the `pcp-jivelite_default-qvga240squareskin.tcz` extension. Otherwise Jivelite will start the Joggler skin as default. This does not work at all with your display.
- Click on the [Tweaks] tab, find section Jivelite and set Framebuffer to /dev/fb1.

![framebuffer](display-7.jpg)

- Install Jivelite (this step includes an automatic backup, so accept the reboot prompt.)
- After a reboot you should see the boot information. It takes about 10 seconds until the display shows text.

##### Step 4 - Configure Jivelite

- Connect a mouse, a keyboard or an IR-remote to the device to configure jivelite.
- Set your language and configure the additional settings (time, background and so on).
- Save your changes in Jivelite Settings (Settings > piCorePlayer > Save Settings to SD card)
- This display has no touch function, you will need other hardware to control jivelite.
- You can control by Smartphone (Squeezer App), with LMS Web-GUI, Jivelite for Windows or IR-Receiver connected to the device.
- It is even possible, to control the jivelite by momentary buttons and/or rotary encoders---see [Control Jivelite by rotary encoders and buttons](/projects/control-jivelite-by-rotary-encoders-and-buttons/).


## Result

- All steps done? The result should look like this:

![now playing](display-8.jpg)

![information](display-9.jpg)


## More information

- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [Edit config.txt](/how-to/edit_config_txt/)
- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [Raspberry Pi config.cfg](http://elinux.org/index.php?title=RPiconfig)
- [Waveshare 4.1 TFT + piCorePlayer + Jivelite](/projects/waveshare-tft-pcp-jivelite/)
- [Documentation / fb / fbcon.txt](https://mjmwired.net/kernel/Documentation/fb/fbcon.txt#72)
