---
title: DIY Red pCP Streamer
description: DIY Red piCorePlayer Streamer
date: 2024-07-07T22:57:43.687Z
lastmod: 2024-07-07T23:20:13.548Z
author: Babbelnet
weight: 265
pcpver: 9.0.0
toc: true
draft: false
categories:
    - Projects
---

{{< lead >}}
Hi folks,

I want to share my new project with you.

The housing is made of a material mix similar to my last 2 projects. Aluminum for the frontpanel and lasercut acrylic glass for the case. The red colored acrylic glass harmonizes very well with the anodized aluminum.

The Waveshare Display is great. It has a very wide horizontal and vertical viewing angle with nice colors and with pCP it works out of the box. The size is the same as the Squeezebox Touch.

The large volume knob also turns the Streamer on or off by short press or paused it by long press. The 3 push buttons are for 6 Presets (i.e. Radiostations or Playlists). Sure, this all can be done with the Touch display too, but these things gives you a much better UX. By pressing one of the push buttons the Streamer turns automatically on and play immediately what you want.

The size of the Streamer without feet is ~ 20 x 9 x 12 cm (w x h x d) and sounds very good.

Now is enough, this is my last project for now (I think ).
Although ... I still have an Hifiberry Amp and 2 Dayton Audio chassis lying around. Hm… maybe a new DIY pCP Boombox? ​
{{< /lead >}}


## Hardware

A selfmade Streamer that contains:

- Raspberry Pi 3B+
- Hifiberry DAC+
- Waveshare 4.3 inch 800 x 480 QLED Touchscreen with DSI Interface
- KY040 Rotary Encoder for Volume and On/Off
- 3 Pushbuttons for Presets


## Software

- piCorePlayer Software


## The Result

![DIY Red pCP Streamer](diy-red-pcp-streamer-1.png)

![DIY Red pCP Streamer](diy-red-pcp-streamer-2.png)

![DIY Red pCP Streamer](diy-red-pcp-streamer-3.png)

![DIY Red pCP Streamer](diy-red-pcp-streamer-4.png)

The POC. Works like a charm.

![DIY Red pCP Streamer](diy-red-pcp-streamer-5.png)


## Demonstration video

- [DIY Red piCorePlayer Streamer Video](https://www.youtube.com/watch?v=fh66wq_uc3I)


## More information

- [DIY Red pCP Streamer](https://forums.slimdevices.com/forum/user-forums/diy/1717174-diy-red-pcp-streamer)
- [A photo of your Squeezebox setup](https://forums.slimdevices.com/forum/user-forums/system-photos/20206-a-photo-of-your-squeezebox-setup-please/page92#post1717177)
- [Control Jivelite by rotary encoders and buttons](https://docs.picoreplayer.org/projects/control-jivelite-by-rotary-encoders-and-buttons/)
- [Transparent piCorePlayer](/projects/transparent-picoreplayer/)
