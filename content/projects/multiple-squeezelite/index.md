---
title: Multiple squeezelite
description: Setup multiple squeezelite
date: 2022-12-15T11:44:46.260Z
lastmod: 2022-12-15T20:42:43.312Z
author: pCP Team
weight: 1030
pcpver: 8.2.0
toc: true
draft: true
categories:
  - Projects
tags:
  - squeezelite
---

There is a requirement sometimes to have multiple instances of squeezelite running on a single Raspberry Pi.


I know you say make unique MAC address but to clarify your 2nd instance should be unique from the first but the first can be unset (ie from pi hardware)

Don't use double quotes around the player name when using User commands.



{{< card style="warning" >}}
piCorePlayer was not initially written handle multiple instances of squeezelite, so the consequences are not fully understood.
{{< /card >}}




### Steps

##### Step 1

- 


##### Step 1

- Access piCorePlayer via ssh---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).

##### Step 2

- $ `mount /mnt/mmcblk0p1`

##### Step 3

- $ `cd /mnt/mmcblk0p1`

##### Step 4

- $ `vi config.txt`

##### Step 5

- $ `reboot`




## More information

- [Two Squeezelite instances on one player ?](https://forums.slimdevices.com/showthread.php?107003-Two-Squeezelite-instances-on-one-player)
- []