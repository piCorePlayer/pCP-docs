---
title: Add an IR receiver
description: Add an IR receiver to piCorePlayer
date: 2022-07-14T20:48:49.942Z
lastmod: 2024-06-24T23:15:36.462Z
author: mini11
weight: 109
pcpver: 8.2.0
toc: true
draft: false
categories:
  - Projects
tags:
  - ir
  - jivelite
  - JustBoom
  - TSOP4838
  - OS-0038N
---

{{< lead >}}
Add an IR receiver and setup a JustBoom IR remote.
{{< /lead >}}

![tsop4838](remote-1.jpg)

![os-0038n](remote-2.jpg)

![JustBoom](remote-3.jpg)

- Today nearly every device is fitted with an IR remote, so why not a piCorePlayer?
- If your display has no touch or you do not want to control your device by a smartphone, an IR remote is very helpful.


## What we need

- Raspberry Pi
- piCorePlayer 8 with Jivelite installed
- A running display connected to RPi
- An TSOP4838 or similar IR receiver like OS-0038N
- 3 jumper wires, female to bare wire
- Soldering iron, solder, some heat shrink tube
- A JustBoom IR remote

{{< card style="info" >}}
TSOP4838 IR receivers are sold in differing designs!
{{< /card >}}


## Steps

##### Step 1 - Connect the IR receiver

- If you want to connect an IR receiver without soldering, use something like this:

![tsop4838-pcp](remote-4.jpg)

- Alternatively connect the pins to some female jumper wires. Plug the pins into the jumper wire.

![tsop4838](remote-5.jpg)

- Isolate the mid pin with a small heat shrink tube. Then put a larger heat shrink tube over the complete construction.

![tsop4838](remote-6.jpg)

{{< card style="info" >}}
This is no durable connection! It's OK for testing the device.
{{< /card >}}

- Solder the blank wires to the IR receiver.
- You should use some heat shrink tube to avoid short circuits.

![tsop4838](remote-7.jpg)

- Connect the female side of the jumper wire to pin 1 (3,3 V), 6 (GND)  and 22 (D-out) of the RPi.

![raspberry](remote-8.jpg)

{{< card style="info" >}}
If pin 1 is used, you can connect pin 17 instead for 3.3 volts. For GND all GND pins are OK.
If pin 22 is used for other purpose, you can connect the data out to another GPIO.
{{< /card >}}

##### Step 2 - Install Kernel Keytables

- Since 4.19 Linux kernel the original LIRC uinput drivers are removed. We need to configure IR keytables.
- With your favourite browser bring up piCorePlayer web GUI and click [Tweaks].
- Scroll down to section IR remote control and click [IR page].

![ir-page](remote-9.jpg)

{{< card style="info" >}}
If LIRC is installed, remove it.
{{< /card >}}

- Install Kernel keytables.

![ir-page](remote-10.jpg)

- By default, IR GPIO 25 (pin 22) is set.

![ir-page](remote-11.jpg)

- Change the default IR GPIO according to your connected data-output IR GPIO.
- Reboot your device.


##### Step 3 - Install Keytable for JustBoom remote

- piCorePlayer provides a keytable for this device. You can install it.
- Login to your RPi via ssh (software=putty – user: tc, password: piCore) and enter the following commands:

```text
	$ sudo ir-keytable -p all -c
```
- This clears the old keytable.

```text
	$ cd /usr/local/etc/keytables
	$ sudo cp /usr/local/share/pcp-irtools/files/justboomir jivelite
	$ sudo ir-keytable -c -w jivelite
	$ pcp bu
```

- After that, the remote should work without a reboot.

## Options

- Another possibility to get the JustBoom IR remote running, is to create a kernel keytable.
- Execute step 1 and step 2.
- On your PC, open your favourite editor. Copy the following text and paste it into a new document.

```text
# table: justboomir, type: rc-5
0x1010	KEY_LEFTBRACE	#special menu
0x1012	KEY_C			#Mute
0x1011	KEY_POWER		#Power
0x101a	KEY_UP
0x101b	KEY_DOWN
0x1013	KEY_LEFT
0x1014	KEY_RIGHT
0x1015	KEY_L			#OK
0x1016	KEY_A			#Menu
0x1017	KEY_ESC			#Back
0x1019	KEY_EQUAL		#Volume Up
0x1018	KEY_MINUS		#Volume Down
```

- Save as plain text file, filename `jivelite`, without an extension.
- The syntax of the file is quite easy.
- Line 1 (header) contains a descriptive name and the type (remote protocol) of your remote.
- In column one the scancodes are listed, you find the key-codes in column two.
- In column three are descriptions of the key-codes, not really necessary but sometimes helpful.
- Bring up piCorePlayer web GUI and click [Tweaks].
- Scroll down to IR remote control and click [IR page].

![ir-page](remote-12.jpg)

- Click [Browse], search and double click on the created text file `jivelite`.
- Click [Upload].

That's it - the remote should now work without a reboot.


{{< card style="info" >}}
Feel free, to to assign other jivelite key-commands. Change the key-commands in column two to your purpose.
You find a complete list of here: [Jivelite Key-Commands](https://github.com/ralph-irving/tcz-lirc/blob/master/jivekeys.csv).
{{< /card >}}


## More information

- [JustBoom IR remote](https://shop.justboom.co/products/justboom-ir-remote)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [Raspberry Pi config.cfg](http://elinux.org/index.php?title=RPiconfig)
- [LIRC RPi gpio IR support for piCorePlayer](https://forums.slimdevices.com/showthread.php?105117-lirc-rpi-gpio-IR-support-for-picoreplayer)
- [tcz-lirc - github](https://github.com/ralph-irving/tcz-lirc)
- [LIRC - Linux Infrared Remote Control](https://www.lirc.org/)
- [Howto create a linux kernel IR remote keytable for jivelite](https://forums.slimdevices.com/showthread.php?110640-Howto-create-a-linux-kernel-IR-remote-keytable-for-Jivelite-on-piCorePlayer-5&highlight=)
- [squeezelite / ir.c - github](https://github.com/ralph-irving/squeezelite/blob/master/ir.c)
