---
title: LMS Monitor
description: LMS Monitor 2020
date: 2024-05-06T11:21:10.823Z
lastmod: 2024-05-06T11:21:22.973Z
author: shunte88
weight: 210
pcpver: 9.0.0
toc: true
draft: false
categories:
  - Projects
tags:
  - LMS
  - Display
---

{{< lead >}}
OLED information display control program for piCorePlayer or other Raspberry Pi and Lyrion Media Player (LMS) based audio device. Thanks shunte88.
{{< /lead >}}

![LMS Monitor 2020](lms-monitor.jpg)


## More information

- [Announce: LMS Monitor 2020 - Forum](https://forums.slimdevices.com/showthread.php?111790-ANNOUNCE-LMS-Monitor-2020)
- [LMS Monitor 2020 - GitHub](https://github.com/shunte88/LMSMonitor)
