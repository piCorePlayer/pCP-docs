---
title: Using ser2Net
description: Using the ser2net extension with piCorePlayer
date: 2022-02-05T09:41:33.583Z
lastmod: 2024-06-24T23:33:56.212Z
author: olivier.com
weight: 200
pcpver: 8.1.0
toc: true
draft: false
categories:
    - Projects
tags:
    - ser2net
---

{{< lead >}}So I achieved success with building my extension and it is working like a charm.

I will give a little walkthrough for people who want to build and use the ser2net extension with piCore (and specially with with piCorePlayer in order to control old DENON AVR)
{{< /lead >}}

![ser2net cable](cable.jpg)


## What was used

### Hardware

- Raspberry Pi 2
- DENON AVR
- [USB to serial adapter](https://www.amazon.fr/gp/product/B07MYYCMJT)

### Software

- piCorePlayer 8.1.0---see [piCorePlayer Downloads](/downloads/)


## Steps

### Step 1 - Assemble hardware

- Plug in your USB to serial adapter.
- Insert the SD card into the Raspberry Pi.
- Connect the Ethernet cable if applicable.
- Connect the power cable(s).
- Turn the power on.

### Step 2 - Add extensions to piCorePlayer

- In the pCP web interface go to [Main Page] in "Beta" mode > [Extensions].

- Under the [Available] tab, choose and [Load] the following extensions:
	- compile.tcz
	- squashfs-tools.tcz
	- usbutils
	- usb-serial-X.X.X-pcpCore-v7.tcz (match your set up)

- Reboot piCorePlayer via [Main Page] > [Reboot]

### Step 3 - ssh into piCorePlayer

- Access piCorePlayer via ssh---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).
- Type `dmesg | grep tty`

```txt
usb 1-1.4: FTDI USB Serial Device converter now attached to ttyUSB0
```

- Take note of the USB device name (ie. ttyUSB0) from the output of the previous command.

### Step 4 - Change to tmp directory

- Type `cd /tmp`

### Step 5 - Download build script

- Type `wget http://tinycorelinux.net/8.x/armv7/tcz/src/ser2net/ser2net.build`

```shell
#!/bin/sh
#
######################################################
# Build script                                       #
#                                                    #
# See .info for details                              #
######################################################

######################################################
# Configure extension creation parameters            #
######################################################

SRCNAM=ser2net-2.10.0.tar.gz
WRKDIR=ser2net-2.10.0
EXTNAM=ser2net
TMPDIR=/tmp/ser2net

######################################################
# Prepare extension creation                         #
######################################################

# Remove dirs and files left from previous creation

rm -r -f $WRKDIR

rm -r -f $TMPDIR
rm -r -f $TMPDIR-doc

# Crete temporary directory

mkdir -p $TMPDIR

######################################################
# Compile extension                                  #
######################################################

# Export variables needed for compilation

export CFLAGS="-O2 -pipe -march=armv6zk -mtune=arm1176jzf-s -mfpu=vfp"
export CXXFLAGS="-O2 -pipe -fno-exceptions -fno-rtti -march=armv6zk -mtune=arm1176jzf-s -mfpu=vfp"

# Unpack source in current directory

tar -xf $SRCNAM

# Configure it

cd $WRKDIR
./configure --prefix=/usr/local

# Compile

make -j4

# Install in base temp dir

make install DESTDIR=$TMPDIR

# Delete compilation work directory

cd ..
rm -r -f $WRKDIR

# Adjust directory access rights

find $TMPDIR/ -type d | xargs chmod -v 755;

# Strip executables

find $TMPDIR | xargs file | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded

# Move files to doc extension

mkdir -p $TMPDIR-doc/usr/local
mv $TMPDIR/usr/local/share $TMPDIR-doc/usr/local

###################################################
# Create base extension in temp dir               #
###################################################

cd $TMPDIR
cd ..
mksquashfs $TMPDIR $EXTNAM.tcz
cd $TMPDIR
find usr -not -type d > $EXTNAM.tcz.list
mv ../$EXTNAM.tcz .

# Create md5 file

md5sum $EXTNAM.tcz > $EXTNAM.tcz.md5.txt

# Cleanup temp directory

rm -r -f usr

###################################################
# Create doc extension in temp dir                #
###################################################

cd $TMPDIR-doc
cd ..
mksquashfs $TMPDIR-doc $EXTNAM-doc.tcz
cd $TMPDIR-doc
find usr -not -type d > $EXTNAM-doc.tcz.list
mv ../$EXTNAM-doc.tcz .

# Create md5 file

md5sum $EXTNAM-doc.tcz > $EXTNAM-doc.tcz.md5.txt

# Cleanup temp directory

rm -r -f usr
```

### Step 6 - Make some modifications to ser2net.build script

- Type `sudo vi ser2net.build`
- Modifiy the source name and folder name variables to match new version of ser2net

```txt
SRCNAM=ser2net-3.5.4.tar.gz
WRKDIR=ser2net-3.5.4
```

- Disable the uucp locking
	- add `--with-uucp-locking=no` option after `./configure --prefix=/usr/local`
- Make the build optimized for your RPi hardware:

```txt
Replace:
export CFLAGS="-O2 -pipe -march=armv6zk -mtune=arm1176jzf-s -mfpu=vfp"
export CXXFLAGS="-O2 -pipe -fno-exceptions -fno-rtti -march=armv6zk -mtune=arm1176jzf-s -mfpu=vfp"
```

```txt
With:
export CFLAGS="-O2 -pipe -march=native -mfpu=vfp"
export CXXFLAGS="-O2 -pipe -fno-exceptions -fno-rtti -march=native -mfpu=vfp"
```

- Save and quit

### Step 7 - Make script executable

- Type `sudo chmod 775 ser2net.build`

### Step 8 - Download ser2net sources

- Type `wget https://sourceforge.net/projects/ser2net/files/ser2net/ser2net-3.5.4.tar.gz/download`

### Step 9 - Build the extension

- Type `./ser2net.build`

### Step 10 - Move your new extension on the optional extension folder

- Type `mv /tmp/ser2net/ser2net.tcz /etc/sysconfig/tcedir/optional/ser2net.tcz`
- Type `mv /tmp/ser2net/ser2net.tcz.list /etc/sysconfig/tcedir/optional/ser2net.tcz.list`
- Type `mv /tmp/ser2net/ser2net.tcz.md5.txt /etc/sysconfig/tcedir/optional/ser2net.tcz.md5.txt`

### Step 11 - Add the extension loading at boot

- Type `sudo echo ser2net.tcz >> /etc/sysconfig/tcedir/onboot.lst`

### Step 12 - Add the ser2net conf file

```txt
sudo touch /etc/ser2net.conf
sudo echo protocol,src_ip,port_to_reach_onRpi:raw:0:/dev/ttyUSB0:9600 8DATABITS NONE 1STOPBIT >> /etc/ser2net.conf
```

- Replace with your params
	- for my configuration I use:

```txt
protocol ipv4
src_ip 0.0.0.0 (all source)
port_to_reach_onRpi 11111 (because I like 1)
```

{{< card style="info" >}}
If you want more information about this conf file you can check man file of ser2net.
{{< /card >}}

### Step 13 - Save settings for future reboots

- Type `sudo echo etc/ser2net.conf >> /opt/.filetool.lst`

- Do a backup:
	- Type `pcp bu`

### Step 14 - Add ser2net execution at boot

- Type `sudo vi /opt/bootlocal.sh`
- Add `ser2net` at the end of the file.
- Save and quit.

- Do a backup and reboot:
	- Type `pcp br`


## More information

- [Serial to Network Proxy (ser2net)](http://ser2net.sourceforge.net/)
- [Ser2net - piCore forum development thread](http://forum.tinycorelinux.net/index.php/topic,25512.0.html)
- [Announce: Denon AVP Control Plugin - Squeezebox forum development thread ](https://forums.slimdevices.com/showthread.php?60114-Announce-Denon-AVP-Control-Plugin&p=1044977&viewfull=1#post1044977)