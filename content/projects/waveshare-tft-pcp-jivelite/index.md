---
title: pCP + Waveshare + jivelite
description: piCorePlayer + Waveshare 3.5" TFT + jivelite
date: 2024-06-25T21:29:24.097Z
lastmod: 2024-06-25T21:38:45.539Z
author: nowhinjing
weight: 60
pcpver: 6.1.0
toc: true
draft: false
categories:
  - Projects
tags:
  - Display
---

{{< lead >}}
As promised I (nowhinjing) have written up the torturous process of getting piCorePlayer + Jivelite to work on a small TFT screen.
{{< /lead >}}


## More information

- [piCorePlayer + Waveshare 3.5 inch TFT + jivelite](https://www.pughx2.com/picore61)

{{< card style="warning" >}}
nowhinjing's website seems to be no longer available.
{{< /card >}}

- [piCorePlayer + Waveshare 3.5 inch TFT + jivelite - Wayback](https://web.archive.org/web/20230314090810/https://www.pughx2.com/picore61)
- [Develpoment thread](http://forums.slimdevices.com/showthread.php?107366-picoreplayer-3-11-waveshare-3-5-TFT-jivelite-Raspberry-Pi-2B)
