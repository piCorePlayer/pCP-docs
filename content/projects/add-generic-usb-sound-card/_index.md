---
title: Add a generic USB sound card
description: null
date: 2022-05-29T04:18:24.858Z
lastmod: 2024-06-24T23:18:56.801Z
author: pCP Team
weight: 1020
pcpver: 8.1.0
toc: true
draft: true
categories:
  - Projects
tags:
  - USB
  - DAC
---

{{< lead >}}
This project is to add a generic USB sound card to piCorePlayer.
{{< /lead >}}


## What was used

### Hardware

- [Raspberry Pi 4B - 1GB](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/)
- [Raspberry Pi 15.3W USB-C Power Supply](https://www.raspberrypi.org/products/type-c-power-supply/)
- SanDisk Ultra 8GB SD card---see [SD card](/components/sd_card/)
- Generic USB DAC

### Software

- piCorePlayer 8.2.0---see [piCorePlayer Downloads](/downloads/).

### Assumptions

- USB sound card is powered up and not in standby **before** powering up piCorePlayer.
- USB hot swap will not work 100%
- Squeezelite will not start if the USB sound card is not powered on and recognised during the boot process.
- piCorePlayer checks for the configured sound card during the boot process. It takes 20 seconds for the check process times out if the configured sound card is not available.


## Steps

##### Step 1 - Download pCP

- Download piCorePlayer---see [Download piCorePlayer](/how-to/download_picoreplayer/).

##### Step 2 - Create SD card

- Create an SD card with piCorePlayer image---see [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/).

##### Step 3 - Connect generic USB DAC

- Plug the generic USB DAC into the Raspberry Pi USB2 port.
- Connect power cable (if required).

##### Step 4 - Boot pCP

- Insert the SD card into the Raspberry Pi.
- Connect power cable.
- Turn the power on the USB DAC (if separate from Raspberry Pi).
- Turn the power on the Raspberry Pi.

##### Step 5 - Determine the IP address

- Determine the IP address---see [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/).

##### Step 6 - Access pCP GUI

- Type `http://<IP address>` into web browser, **or**
- Type `http://pcp.local` into web browser.

##### Step 7 - Select USB audio

- Select [Squeezelite Settings] > "Audio output device settings" > "USB audio".
- Click [Save].
- This will set "Audio output device settings" to "USB audio" and set "Change Squeezelite settings" to default values.

##### Step 8 - Set Output setting

{{< card style="danger" >}}
The "Output setting" will be blanked and will need to be set manually.
{{< /card >}}

- Click [Squeezelite Settings] > "Change Squeezelite settings" > "Output setting" > "more>".
- Note the list of available `CARD=xxxxx` devices.

##### Example of a generic USB DAC

{{< highlight "text" "hl_lines=12" >}}
Available output devices (click to use):

hw: devices are normally the best choice, but try and decide for yourself:

null
default
plugequal
equal
hw:CARD=b1,DEV=0
plughw:CARD=b1,DEV=0
sysdefault:CARD=b1
hw:CARD=Device,DEV=0
plughw:CARD=Device,DEV=0
sysdefault:CARD=Device
front:CARD=Device,DEV=0
surround21:CARD=Device,DEV=0
surround40:CARD=Device,DEV=0
surround41:CARD=Device,DEV=0
surround50:CARD=Device,DEV=0
surround51:CARD=Device,DEV=0
surround71:CARD=Device,DEV=0
iec958:CARD=Device,DEV=0
{{< /highlight >}}

##### Example of a Topping E30 DAC

{{< highlight "text" "hl_lines=10" >}}
Available output devices (click to use):

hw: devices are normally the best choice, but try and decide for yourself:

- null
- sysdefault
- default
- plugequal
- equal
- hw:CARD=E30,DEV=0
- plughw:CARD=E30,DEV=0
- sysdefault:CARD=E30
- front:CARD=E30,DEV=0
- surround21:CARD=E30,DEV=0
- surround40:CARD=E30,DEV=0
- surround41:CARD=E30,DEV=0
- surround50:CARD=E30,DEV=0
- surround51:CARD=E30,DEV=0
- surround71:CARD=E30,DEV=0
- iec958:CARD=E30,DEV=0
- hw:CARD=Headphones,DEV=0
- plughw:CARD=Headphones,DEV=0
- sysdefault:CARD=Headphones
{{< /highlight >}}

{{< card style="danger" >}}
If your USB DAC is not displayed in this list it has **not** been detected. Try a reboot.
{{< /card >}}

- Click on `hw:CARD=xxxxx`.
- This will set "Output setting" to `hw:CARD=xxxxx`.
- Click [Save].

##### Step 9 - Reboot

- Click [Reboot] when requested.

##### Step 10 - Play music

- Select track/playlist.
- Play music.


## Optional

If you have more than one piCorePlayer on your network, it is highly recommended that you continue to the following steps.

##### Step 11 - Set player name

- Access pCP using IP address in a browser (ie. http://192.168.1.xxx or http://pcp.local).
- Select [Squeezelite Settings] > "Change Squeezelite settings" > "Name of your player".
- Type player name `pCP generic DAC`.
- Click [Save].

##### Step 12 - Set hostname

- Select [Tweaks] > "pCP System Tweaks" > "Host name".
- Type host name `pCPgenericDAC`.
- Click [Save].
- Click [Yes] when requested to "Reboot piCorePlayer".

##### Step 13 - Reboot

- Click [Reboot] when requested.


## Diagnostics

##### Check that your USB DAC has been recognised

- Select  [Main Page] > [Diagnostics] > [Logs] > "dmesg".
- Click [Show].
- Search for `xxxxx`.

##### Example of a typical generic USB DAC

```text
[17400915.615285] usb 1-1.1: new full-speed USB device number 3 using xhci-hcd
[17400915.718945] usb 1-1.1: New USB device found, idVendor=1908, idProduct=2070, bcdDevice= 1.00
[17400915.718962] usb 1-1.1: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[17400915.718975] usb 1-1.1: Product: USB PnP Sound Device
[17400915.718987] usb 1-1.1: Manufacturer: Generic
```

##### Example of a Topping E30 DAC

```text
[    1.661609] usb 1-1.3: new high-speed USB device number 3 using xhci_hcd
[    1.792056] usb 1-1.3: New USB device found, idVendor=152a, idProduct=8750, bcdDevice= 1.08
[    1.792063] usb 1-1.3: New USB device strings: Mfr=1, Product=3, SerialNumber=0
[    1.792070] usb 1-1.3: Product: E30
[    1.792076] usb 1-1.3: Manufacturer: Topping
```

- Check that the "CARD=xxxxx" devices are available.
- Click [Squeezelite Settings] > "Change Squeezelite settings" > "Output setting" > "more>".
- Search for `CARD=xxxxx`.

```text
Available output devices (click to use):

hw: devices are normally the best choice, but try and decide for yourself:

- null
- sysdefault
- default
- plugequal
- equal
- hw:CARD=E30,DEV=0
- plughw:CARD=E30,DEV=0
- sysdefault:CARD=E30
- front:CARD=E30,DEV=0
- surround21:CARD=E30,DEV=0
- surround40:CARD=E30,DEV=0
- surround41:CARD=E30,DEV=0
- surround50:CARD=E30,DEV=0
- surround51:CARD=E30,DEV=0
- surround71:CARD=E30,DEV=0
- iec958:CARD=E30,DEV=0
- hw:CARD=Headphones,DEV=0
- plughw:CARD=Headphones,DEV=0
- sysdefault:CARD=Headphones
```


## Default Raspberry Pi sound card names

Here is a list of the Raspberry Pi default card names. You r USB DAC will *not* be on of these

- Headphones
- b1
- b2
- vc4hdmi


## More information

- [Download piCorePlayer](/how-to/download_picoreplayer/)
- [Burn piCorePlayer onto a SD card](/how-to/burn_pcp_onto_a_sd_card/)
- [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/)
- [Determine your piCorePlayer IP address](/how-to/determine_your_pcp_ip_address/)
- [LMS could not re-establish any USB connection after USB Dac is disconnected](https://forums.slimdevices.com/showthread.php?109934-LMS-could-not-reestablish-any-USB-connection-after-USB-Dac-is-disconnected/page2)

