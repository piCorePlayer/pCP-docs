---
title: My CNX100 Beater
description: My Cambridge Audio CNX100 Beater
date: 2024-07-07T23:52:46.969Z
lastmod: 2024-07-07T23:52:50.771Z
author: JoAnywhere
weight: 280
pcpver: 9.0.0
toc: true
draft: false
categories:
    - Projects
---

{{< lead >}}
All,
I'm not really sure it 'beats' a Cambridge Audio CXN100, but it sounds pretty awesome, and will be in a great case soon.
{{< /lead >}}


## Hardware

- Raspberry Pi 3B+
- This DAC board: https://www.aliexpress.com/item/1005004698522980.html
- This screen: https://www.aliexpress.com/item/1005006739026067.html
- This PSU: https://www.aliexpress.com/item/1005006635266334.html
- This USB module https://www.aliexpress.com/item/1005004639110047.html (not yet installed - I have no standoffs lying around that are the right length, but a mate does  ).
- Genuine TI NE5532AP OpAmp to replace the clone on on the DAC board.
- I also purchased a longer DSI cable (for the main screen) and longer JST cables (for the DAC knob and DAC screen). These are needed for when I put in in a case.

The RPi is connected to the DAC board via I2S. When I connected it, and set the audio output in SqueezeLite it worked first time!


## Things it can do

- Stream through the RPi using LMS and Squeezelite. This includes Qobuz, Internet Radio, and Bluetooth from my phone. This all gets routed to the DAC board via I2S.
- Allow me to connect my CD player to the DAC via the coax input on the DAC.
- Allow me to connect my TV to the DAC via the optical input on the DAC.
- Leave me with a USB input spare for god knows what.
- Have a hard wired LAN connection to the RPi to ensure flawless streaming (Wi-Fi has been fine, but I'd prefer LAN connection).


## Two pictures attached

- My test desk rig, not the final form (hence why it's using the 3.5mm jack for output into my desktop speakers).

![Bench Test Rig](bench-test-rig.jpg)

You can see at the time I was streaming Miles Davis's Quintet/Sextet at 192kHz/24bit.

- The layout for the front panel that I am making.

![Front Panel](front-panel.png)

I purchased a busted Sony STR-DH790 and have gutted the interior.


## Steps

- Attach USB module to DAC.
- Shift power to RPi from USB to GPIO. It means I don't have all the wasted space on the side of the RPi. This in turn lets me layout the PSU and two boards to broadly speaking align with their front panel controls.
- Fit components into case. This will require me to create a new base plate as the Sony has a long of stiffening ridges etc.
- Drill out back panel appropriately to allow all inputs and outputs to come through (thank god for a drill press).
- Create new front panel out of 25mm Oak veneered Birch plywood (fun with a plunge router ahead!!)
- 3D Print a few brackets for attaching front panel.
- Assemble.
- Move it to the Living Room.

I've got a few other things on, but hopefully in the next 3-4 weeks it'll all be finished.

If anyone wants more info just ask, I'll tell you as much as I can.


## Case preparations

I'm not trying to turn this into an instructible, but I thought people might be interested in where the project is up to.

On Saturday my mate and I got stuck in again.

It started with needing to remove a high part of the chassis from the donor case. Once that was done, we cut a Perspex base plate and made some shims to level it where needed. Conveniently, the low points where we wanted to stabilise the Perspex were 3mm to low. How thick was the Perspex? 3mm. Now that was handy!

Then we mocked up a layout of the PSU, RPi, and DAC. Once we decided we were happy, we drilled holes and attached the PSU (screwed from the underside of the Perspex).

Next step was modifying the back panel to accommodate the ports for the DAC and the RPi. We got that done with sub millimetre clearances. There was no need to be so accurate other than a degree of pride in our workmanship.

Then we very carefully marked the base plate, drilled, and attached the electronics. Again, done to sub millimetre clearances.

A good solid 6 hours of using bandsaw, drill, drill press, nibbler, Dremel, files, and various other workshop weapons.

![Case](case-1.png)
![Case](case-2.png)


## More information

- [My CXN100 beater](https://forums.slimdevices.com/forum/user-forums/diy/1713969-my-cxn100-beater)
