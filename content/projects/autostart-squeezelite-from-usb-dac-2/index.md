---
title: Autostart Squeezelite
description: Autostart Squeezelite - take 2
date: 2021-01-15
lastmod: 2024-06-24T23:22:00.797Z
author: null
weight: 125
pCPver: 8.2.0
toc: true
draft: true
categories:
  - Projects
tags:
  - USB
  - DAC
---

{{< lead >}}
How to autostart Squeezelite when you power-on your USB DAC.
{{< /lead >}}


## Steps

##### Step 1 - Access piCorePlayer via ssh

- Login into ssh shell---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).
- ???? Turn off your DAC and then it turn on.

##### Step 2 - Download script

\$ `wget "https://raw.githubusercontent.com/uudruid74/picoreplayer-usbconnectd/main/usbconnectd"`

##### Step 3 - Make the script executable

- Type \$ `chmod 755 usbconnectd` to make the script executable.

##### Step 4

- Add `/home/tc/usbconnectd` to User command

##### Step 5


##### Step 6

- Type `pcp br` to backup and reboot.

Now, if your DAC is turn-off and squeezelite is down, when you turning on the DAC squeezelite get start.


## More information

- [Development thread]()
- [Documentation thread]()
