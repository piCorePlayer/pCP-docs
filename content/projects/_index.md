---
title: Projects
description: null
date: 2021-01-25T00:00:00.000Z
lastmod: 2022-06-09T05:08:37.243Z
author: pCP Team
weight: 60
pcpver: 6.1.0
toc: true
draft: false
---

{{< lead >}}
Here is a group of projects done by various people over the years using piCorePlayer. Unlike How to's that should be reasonably up to date, Projects are a snap shot in time when they were created and may gradually drift out of date. There is no guarantee that they are currently accurate. Projects should be considered a source of inspiration and great starting point for your own project.

Thanks to those that have taken the time to document their piCorePlayer projects.

If you discover some projects I have missed please let us know.
{{< /lead >}}


## Submitted Projects

{{< childpages >}}
