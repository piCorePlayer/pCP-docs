---
title: piCorePlayer 1.11c
description:
date: 2014-02-04
author: pCP Team
weight: 1
pcpver: "1.11c"
toc: true
draft: false
---

piCorePlayer 1.11c is ready... get it from the [Downloads](/downloads/) section.

### Changes

- Fixed the bug with not saving the HOST name after a reboot.
- Fixed a few other not saving bugs.
- Fixed a few graphical glitches.

### Note

piCorePlayer 1.11 is still only able to play up to 48kHz via HDMI - I'm looking into this and hope to fix it soon.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
