---
title: piCorePlayer 2.05
description:
date: 2016-05-06
author: pCP Team
weight: 1
pcpver: "2.05"
toc: true
draft: false
---

### Download

Please try the new version [piCorePlayer 2.05](https://repo.picoreplayer.org/insitu/piCorePlayer2.05/piCorePlayer2.05.img).

The pCP Team is happy to release this new version, with special focus on LMS integration and managing attached USB-disk.

### Changes

- Added a [LMS update] button, so it is very easy to update LMS to the most recent nightly version.
- Added options to mount hard disks.
- Added options to move the cache and preferences to mounted drive - otherwise piCorePlayer would be using your SD card, but it might be safer to use a mounted disk.
- Added a pop-up that offers to expand the partition if it is too small.
- Improved the update process, so that it will be future proof if anything changes in pCP.
- Fixed the VU-meter problem.
- A lot of minor improvements throughout the scripts.

Have a look at the [LMS] configuration page (select [Beta] mode on the [Main] page).

The pCP Team<br>
Paul, Greg, Ralphy and Steen
