---
title: piCorePlayer 8.1.0
description:
date: 2021-12-05
author: pCP Team
weight: 1
pcpver: "8.1.0"
toc: true
draft: false
---

The pCP Team has released an new version of piCorePlayer.

For support---see this topic on [Squeezebox Forum](https://www.picoreplayer.org/announce.8.0.0).

### Significant changes

- Kernel 5.10.77
  - Removed out of tree Realtek wifi drivers from Kernel.  If you see a warning on the insitu update, you should not proceed until you build your driver---see [pCP Kernels](https://github.com/piCorePlayer/pCP-Kernels).
- RPi Firmware Nov 4, 2021
- Adds full support for RPi02W
- Based on piCore 13
- OpenSSL 1.1.1k
- Squeezelite 1.9.9-1391
- Various bug fixes and web interface updates.

### Notes

- Adding a file named `netusb` to the boot partition will automatically load/configure net-usb kernel modules.
- Adding `wpa_supplicant.conf` to the boot partition will automatically enable Wifi---see [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet).

### Download

New images or insitu update are available. The released versions can be downloaded from here.

- [piCorePlayer 8.1.0 - 32 bit Version](https://repo.picoreplayer.org/insitu/piCorePlayer8.1.0/piCorePlayer8.1.0.zip)
- [piCorePlayer 8.1.0 - 64 bit Version](https://repo.picoreplayer.org/insitu/piCorePlayer8.1.0/piCorePlayer8.1.0-64Bit.zip)

The pCP Team<br>
Paul, Greg, Ralphy and Steen
