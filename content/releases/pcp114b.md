---
title: piCorePlayer 1.14b
description:
date: 2014-03-24
author: pCP Team
weight: 1
pcpver: "1.14b"
toc: true
draft: false
---

**piCorePlayer 1.14b** is ready... get it from the [Downloads](/downloads/) section.

### Changes

- Minor bug fixed - if you had CODEC in your ALSA settings piCorePlayer would crash - fixed now.
- A new option. If you provide a configuration file: newconfig.cfg in the FAT partition it will be copied and used for configuration - and then deleted. You can easily add this file to the FAT partition from any computer with a SD card reader. You can use the web GUI to make the newconfig file from a working piCorePlayer.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
