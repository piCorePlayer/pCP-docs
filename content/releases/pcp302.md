---
title: piCorePlayer 3.02
description:
date: 2016-09-25
author: pCP Team
weight: 1
pcpver: "3.02"
toc: true
draft: false
---

Sorry for the long wait before we could release a fixed version---please try the new version [piCorePlayer 3.02](https://repo.picoreplayer.org/insitu/piCorePlayer3.02/piCorePlayer3.02.img).

### Changes

- Shairport-sync issue - fixed.
- Several extensions updated.
- HiFiBerryDAC+ Pro supported.
- Kernel updated to 4.4.20.
- filesystem extension available again.
- rpi-vc extension available again.
- New web page to control adding/removing extensions (using "Advanced mode" and selecting "Extensions").
- And a lot of smaller and larger improvements.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
