---
title: piCorePlayer 1.21a
description:
date: 2015-09-10
author: pCP Team
weight: 1
pcpver: "1.21a"
toc: true
draft: false
---

New **piCorePlayer 1.21a** ready.

**Note** But first I owe you all an apology. Unfortunately all 1.21 users will find that you are unable to do in situ update - sorry about that.

- If you are using **piCorePlayer 1.21** You will need to burn the 1.21a img directly to the SD card.
- If you are using **piCorePlayer 1.19** or older you can update from the web pages as always.

### Changes

- Kernel updated with the most recent fixes (4.1.6) especially this https://github.com/raspberrypi/linux/pull/1125 which I think indicate that the kernel is ready for fbtft users.
- Build with fbtft support - (module package will come later).
- host name setting is fixed (now a reboot is needed in order to change host name).
- Hiface USB module included.
- Build with IRDA support - modules still need to be included in a separate package (will come later).
- Various bug fixes related to Wifi, ALSA settings, in situ update etc.

### Note

If you are comfortable editing a file in piCorePlayer you can fix the in situ update problem in piCorePlayer 1.21. Otherwise you simply download piCorePlayer 1.21a_RPi or piCorePlayer 1.21a_RPi2 and burn it to your SD card.

Find and edit line #59 using vi editor in the insitu.cgi file located here: /home/tc/www/cgi-bin/insitu.cgi

from

`sudo -P "$UPD_PCP"/boot "$INSITU_DOWNLOAD"/"$INSITU"/"$INSITU"_boot.tar.gz`

to

`sudo` **wget** `-P "$UPD_PCP"/boot "$INSITU_DOWNLOAD"/"$INSITU"/"$INSITU"_boot.tar.gz`

The pCP Team<br>
Paul, Greg, Ralphy and Steen
