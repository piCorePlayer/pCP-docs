---
title: piCorePlayer 1.15d
description:
date: 2014-05-04
author: pCP Team
weight: 1
pcpver: "1.15d"
toc: true
draft: false
---

Sorry for the disaster with the piCorePlayer 1.15c - It played very well using USB - but I did not test anything else - and as you all know it was inferior in all other aspects. So I'm removing it from download.

So please go to **piCorePlayer 1.15d**.

### Changes

- kernel updated to 3.14.2 with the latest USB-fixes, it plays via USB at least as good as the 1.15c version.
- Analog audio is working.
- I2S-Audio cards are working.
- WiFi is working.
- HDMI is working (I hope please test).

This is probably the best piCorePlayer ever.

Now I will focus on developing some sort of in situ update of piCorePlayer, so we don't have to have access to the player when we want to update.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
