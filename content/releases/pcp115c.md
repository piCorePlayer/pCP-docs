---
title: piCorePlayer 1.15c
description:
date: 2014-05-01
author: pCP Team
weight: 1
pcpver: "1.15c"
toc: true
draft: false
---

Please use the new version: **piCorePlayer 1.15c**.

### Changes

- Improved memory flow in the Raspberry Pi kernel and firmware now enabling DMA_CMA. The problem was that when the USB DMA engine fetches data for transfer, chunks of old 32-byte data get transmitted, resulting in crackling USB audio.
- Now High-speed isochronous transactions should be accelerated as well - so those of you with USB DACs that had problems, please try this version and report your findings.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
