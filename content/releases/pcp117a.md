---
title: piCorePlayer 1.17a
description:
date: 2014-08-24
author: pCP Team
weight: 1
pcpver: "1.17a"
toc: true
draft: false
---

A new bug fixed version is ready: **piCorePlayer 1.17a**

### Changes

- The physical MAC address was not shown in [Squeezelite settings] web page.
- The timezone changes was not saved.
- Update to latest Triode's Squeezelite player resulted in a wrong version.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
