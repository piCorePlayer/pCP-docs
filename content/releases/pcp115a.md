---
title: piCorePlayer 1.15a
description:
date: 2014-04-26
author: pCP Team
weight: 1
pcpver: "1.15a"
toc: true
draft: false
---

Please try the new version **piCorePlayer 1.15a**.

### Changes

- Kernel updated to 3.14.1 - which should have improved USB audio playback? Please report.
- Added missing firmware for TP-WN725 v2 Wifi adapter - sorry for that.
- Removed power saving option for Edimax Wifi (8192cu) - so hopefully it will now stay on forever.
- Fixed the option to skip the Wifi connection during boot if Wifi has been disabled.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
