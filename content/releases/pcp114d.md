---
title: piCorePlayer 1.14d
description:
date: 2014-03-24
author: pCP Team
weight: 1
pcpver: "1.14d"
toc: true
draft: false
---

**piCorePlayer 1.14d** is ready... get it from the [Download](https://www.picoreplayer.org/main_downloads.shtml) section.

### Changes

- Fixed a bug with dwc_otg.speed - so there was no effect of the tweak. If you had problems with pops and clicks then the tweak would have had no effect. Please try the new version.
- Squeezelite updated to version 1.16.
- Fixed Triode's download link to version 1.6 - so now both Ralphy's and Triode's Squeezelite builds are up to date.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
