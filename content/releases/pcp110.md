---
title: piCorePlayer 1.10
description:
date: 2014-01-04
author: pCP Team
weight: 1
pcpver: "1.10"
toc: true
draft: false
---

**piCorePlayer 1.10** is ready.

Hi all, I'm happy to announce a new version of piCorePlayer, which is a major improvement over previous versions.

It is build on cutting edge linux kernel 3.12.6 which have support for I2S-connections. That means that you will be able to let the DAC communicate directly with the Raspberry Pi. There are already a few I2S DACs available, however at the moment piCorePlayer only support the **HiFiBerryDAC**, you can read more about [HiFiBerry](http://www.hifiberry.com/).

This kind of connection is much better than the USB connection, so I suspect much better results using these kind of DAC's compared with USB based DACs.

So it connects to the Raspberry Pi like this (image taken from the [HiFiBerryweb page](http://www.hifiberry.com/)):

In order to support the other I2S DACs I will need access to them, and at the moment only Daniel from [Crazy Audio](http://www.crazy-audio.com/) gave me access to a prototype of the HiFiBerry DAC. I have been playing with it for a few days and I really like it. It fits nicely on the Raspberry Pi and the audio streaming from it is nice to listen to.

If you would like other I2S DACs supported please donate and indicate which one I should focus on.

Furthermore, I have included support for more WiFi adapters and this time I feel confident that several of the Ralink adapter should work - so please report your findings.

Ralphy is supporting piCorePlayer by building the newest versions of Triode's Squeezelite with build-in support of WMA and ALAC together with upsample support. In addition, piCorePlayer can update the Squeezelite directly from Ralphy's server. So thank you very much for that.

Finally, I have almost finished building a small WEB based configuration menu, so within a few days I hope to release a new version (where the only difference will be the inclusion of a small WEB server to easily configure the piCorePlayer).

This release has been the most difficult one to make - so I hope you will enjoy it.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
