---
title: piCorePlayer 1.11b
description:
date: 2014-02-03
author: pCP Team
weight: 1
pcpver: "1.11b"
toc: true
draft: false
---

**piCorePlayer 1.11b** is ready - it is the same as piCorePlayer 1.11 with some bugs have been dealt with... get it from the [Downloads](/downloads/) section.

### Changes

- Now saves all your changes.
- Now Squeezelite reliable starts after a reboot.
- Now the overclocking options are saved and used.
- All options are used (the codec selection was not used before).
- The reboot page has been changed, in order to avoid accidental reboot.
- The pages will refresh when using the "back" button in your browser, so your selected options are shown.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
