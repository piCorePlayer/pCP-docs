---
title: piCorePlayer 3.20b5
description: null
date: 2017-04-02
lastmod: 2022-07-20T23:21:45.332Z
author: pCP Team
weight: 1
pcpver: 3.20b5
toc: true
draft: false
---

Hi, the pCP Team is nearing the completion of the release of a new piCorePlayer 3.20. Because of the many significant kernel changes, improvements and bug fixes we decided to offer a public beta. Anyone is free to try the beta, but if you find a problem please post diagnostic data.

Both version are ready for testing.

### Notes

1.  **piCorePlayer3.20beta5** which is a normal version like all previous pCP releases. The kernel and modules are mostly vanilla versions from the official Raspberry Pi Linux source with only a couple of tweaks.
2.  **piCorePlayer3.20beta5-Audio** version. This version contain all the same improvements as the above normal version. But in addition we have added all the audio related patches that Clive (aka JackOfAll) made for the raspberry kernel. So here we have support for more I2S DACs and we support higher sample rates etc.

This version is only recommended for I2S soundcards and hardwired network. The optimizations for audio has been known to interfere with Wifi, and for sure will make USB soundcards unstable.

### Kernel/Firmware changes

- Kernel 4.9.17.
- RPi firmware 2017/03/31.
- config.txt changes to increase usable memory on RPi2/3 by 16MB.
- config.txt changes to decrease videocore memory usage.
- RPi Zero-W Support.
- Allo Boss and Piano-Plus Support.
- Changed 8192cu driver (same version that OSMC uses).
- 8812au driver included.

### Other pCP changes

- Squeezelite updated to v1.86-945.
- ffmpeg updated to 3.17.
- wiringpi shared libraries included by default. (Used to be static linked to Squeezelite).
- flac updated to 1.3.2.
- utf8 support for fat32 partitions.
- New card configuration system, will help with speed on [Squeezelite Settings] page. Also allows us to add more specific card information.
- Set `--nomysqueezebox` option from web interface.
- Default LMS install is 7.9.0 released version.
- Great updates to pCP jivelite package. Made possible by mherger.
- pCP Help web site---see http://picoreplayer.sourceforge.net. This is still a work in progress, where we can easily create web pages with HOW TO's and other information. If you have something you would like included, creates a simple web page and let us know.
- And last but not least, corrections to the insitu update process.

Before you get to excited about the last improvement, We need to make you aware that the images for piCorePlayer 3.11 and prior used a 25MB boot partition. With the new kernel and overlays piCorePlayer 3.20 uses all but 200kB (99%) of this space. piCorePlayer 3.20 images will increase to a 32MB boot partition to hopefully future proof that for a while.

The insitu upgrade will not be offered for the beta phase, but we will make it available at the time of the release. There will be a hotfix needed to make this available.....we will post that fix later.

### Download

The betas can be downloaded here.

- [piCorePlayer 3.20 beta 5](https://repo.picoreplayer.org/beta/)

The pCP Team<br>
Paul, Greg, Ralphy and Steen
