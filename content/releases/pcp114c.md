---
title: piCorePlayer 1.14c
description:
date: 2014-03-24
author: pCP Team
weight: 1
pcpver: "1.14c"
toc: true
draft: false
---

**piCorePlayer 1.14c** is ready... get it from the [Downloads](/downloads/) section.

### Changes

- Bug fix version - a firmware problem with Ralink Wifi adapters

The pCP Team<br>
Paul, Greg, Ralphy and Steen
