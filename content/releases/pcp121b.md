---
title: piCorePlayer 1.21b
description:
date: 2015-09-13
author: pCP Team
weight: 1
pcpver: "1.21b"
toc: true
draft: false
---

Thanks to all your bug reports, Greg and I are releasing a new version **piCorePlayer 1.21b**.

### Changes

- The update process required a reboot in order for the I2S-DACs to be loaded. Resulting in "Squeezelite not running" after first boot - this is now done automatically the first time you start after an in situ update.
- This also fixed a host name problem.
- The newconfig.cfg file was not removed after an in situ update, which probably is the cause of some of the inconsistent problems we are seeing - should be fixed now.
- ALSA is patched to 192kHZ again.

**19 September 2015**

Hi all. Just want to show you a new I2S DAC that plays without any problems with the latest version of piCorePlayer. It is a newly build version using a Sabre E9023 chip. It can be purchased from [Audiophonics](http://www.audiophonics.fr/). I like it very much. The sound is very good but also if you notice they have build it so that the connectors are pointing in the same direction as the USB and LAN connectors. This will make it so much easier to put this combo into a nice looking case.

You can read about it here: [Audiophonics I-Sabre DAC ES9023 V2 TCXO Raspberry Pi 2.0 A+ B+ / I2S](http://www.audiophonics.fr/fr/kits-modules-diy-dac/audiophonics-i-sabre-dac-es9023-v2-tcxo-raspberry-pi-20-a-b-I2S-p-10176.html)

[![New
Audiophonics I-Sabre DAC ES9023 V2 TCXO
Raspberry Pi 2.0 A+ B+ / I2S](http://www.audiophonics.fr/12247-thickbox_default/audiophonics-i-sabre-dac-es9023-v2-tcxo-raspberry-pi-20-a-b-I2S.jpg)](http://www.audiophonics.fr/12247-thickbox_default/audiophonics-i-sabre-dac-es9023-v2-tcxo-raspberry-pi-20-a-b-I2S.jpg)

The pCP Team<br>
Paul, Greg, Ralphy and Steen
