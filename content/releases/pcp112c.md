---
title: piCorePlayer 1.12c
description:
date: 2014-02-22
author: pCP Team
weight: 1
pcpver: "1.12c"
toc: true
draft: false
---

piCorePlayer 1.12c is ready... get it from the [Downloads](/downloads/) section.

### Changes

- Fixed a bug about starting WiFi after reboot - just missing a stupid ";" was enough to break the logic in the start script. Sorry.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
