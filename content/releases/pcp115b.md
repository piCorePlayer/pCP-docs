---
title: piCorePlayer 1.15b
description:
date: 2014-04-28
author: pCP Team
weight: 1
pcpver: "1.15b"
toc: true
draft: false
---

Please try the new version: **piCorePlayer 1.15b**. I really hope those of you having trouble with your USB cards will find this version better.

### Changes

- Updated the Raspberry Pi firmware to the so-called "Next" branch which should help fixing USB audio problems.
- Added these start-up command to the cmdline.txt file which enable the new USB driver:
    - dwc_otg.fiq_enable=1
    - dwc_otg.fiq_fsm_enable=1
    - dwc_otg.fiq_fsm_mask=0x1
- Unfortunately I was unable to use dwc_otg.fiq_fsm_mask=0x7, which should help **High-speed isochronous acceleration** - it produced constant crackling noise using USB audio.
- Fixed the script that did different stuff after reboot - now the errors of not finding specific files are not displayed anymore.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
