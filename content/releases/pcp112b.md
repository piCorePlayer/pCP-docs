---
title: piCorePlayer 1.12b
description:
date: 2014-02-19
author: pCP Team
weight: 1
pcpver: "1.12b"
toc: true
draft: false
---

piCorePlayer 1.12b is ready... get it from the [Downloads](/downloads/) section.

### Changes

- Fixed a bug about starting HDMI after reboot.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
