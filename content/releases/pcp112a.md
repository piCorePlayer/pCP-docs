---
title: piCorePlayer 1.12a
description:
date: 2014-02-18
author: pCP Team
weight: 1
pcpver: "1.12a"
toc: true
draft: false
---

piCorePlayer 1.12a is ready... get it from the [Downloads](/downloads/) section.

### Changes

- HDMI streaming now once again possible with 192kHz.
- Various improvements in the web pages (now the radio buttons show your selected output).
- The start of Wifi, Dropbear and Squeezelite after reboot has been rearranged.
- A bug in saving the SSID and password for Wifi has been fixed.
- Kernel updated to 3.12.9
- Firmware updated (Should have fixed some HDMI and USB audio problems).

The pCP Team<br>
Paul, Greg, Ralphy and Steen
