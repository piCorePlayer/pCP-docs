---
title: piCorePlayer 1.12d
description:
date: 2014-03-03
author: pCP Team
weight: 1
pcpver: "1.12d"
toc: true
draft: false
---

piCorePlayer 1.12d is ready... get it from the [Downloads](/downloads/) section.

It is the same kernel, but has some bugfixes and some nice additions to the web GUI.

### Bug fixes

- Now use dos2unix on the file newconfig.cfg on the USB stick, so that it can be used as config file for piCorePlayer. (Thanks to Gordon for the suggestion).
- Now saves the newly copied config file from the USB stick, so that it survives a reboot.

### Improvements

- In the [Main page] I added an option so you can save your current config file to an attached USB stick. It will be saved as "newconfig.cfg" so you can use it directly after an upgrade or in another piCorePlayer.
- In the [Tweaks] page, added an option to toggle `dwc_otg_speed=1` on and off in your cmdline.txt file. This has been suggested to solve some crackling noise from certain USB DACs (especially USB DACs based on the C-MEDIA chip).

The pCP Team<br>
Paul, Greg, Ralphy and Steen
