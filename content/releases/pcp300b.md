---
title: piCorePlayer 3.00b
description:
date: 2016-08-14
author: pCP Team
weight: 1
pcpver: "3.00b"
toc: true
draft: false
---

Please try the new version [piCorePlayer 3.00](https://repo.picoreplayer.org/insitu/piCorePlayer3.00/piCorePlayer3.00.img).

### Changes

- Kernel updated to 4.4.15.
- Using openSSH instead of dropbear as ssh server.
- Added infrared (IR) remote control of Jivelite (LIRC support from the [Tweaks] page).
- Numerous bug fixes.
- Improved appearance.
- Added option to wake on LAN (WOL) of LMS server to [Tweaks] page.
- Support for new I2S DACs.
- Lots of internal improvements.

The pCP Team<br>
Paul, Greg, Ralphy and Steen
