---
title: piCorePlayer 1.14a
description:
date: 2014-03-22
author: pCP Team
weight: 1
pcpver: "1.14a"
toc: true
draft: false
---

**piCorePlayer 1.14a** is ready... get it from the [Downloads](/downloads/) section.

### Changes

- Wifi packages from Tiny Core improved (fixed problem with TP-Link WN725n (Realtek r8188eu firmware)) - thanks to bmarkus from the Tiny Core forum.
- Fixed that I forgot smsc95xx.turbo_mode=N in cmdline.txt (might improve USB-DAC problems with clicks and pops)
- Support for a new I2S DAC (Sabre 9023). Thanks to Jean Beauve and [Audiophonics](http://www.audiophonics.fr/) for making this possible - (up to 24bit 192kHz) - is playing very nicely on the piCorePlayer.

Audiophonics DAC Sabre ES9023 V2.2 I2S to Analogue 24bit/192kHz

[![](http://www.audiophonics.fr/images2/8396/8396_ES9023_I2S_DAC_4.jpg)](http://www.audiophonics.fr/audiophonics-dac-sabre-es9023-I2S-vers-analogique-24bit192khz-p-8396.html)

The pCP Team<br>
Paul, Greg, Ralphy and Steen
