---
title: Lyrion Music Server
description: Lyrion Music Server (LMS)
date: 2024-09-16T09:54:16.510Z
lastmod: 2024-09-16T09:54:18.337Z
author: pCP Team
weight: 30
pcpver: 9.0.0
toc: true
draft: false
categories:
    - Components
tags:
    - LMS
---

{{< lead >}}
Lyrion Music Server (LMS) is the server software that powers networked audio players from Logitech including Squeezebox, Radio, Boom, Receiver, Transporter and various third party hardware devices that use Squeezelite or Squeezeplay.

Lyrion Music Server (LMS) is Open Source Software written in Perl and it runs on pretty much any platform that Perl runs on, including Linux, Mac OSX, Solaris and Windows.

Lyrion Music Server (LMS) was previously known as Squeezebox Server, SqueezeCentre and SlimServer.
{{< /lead >}}

## Installation
Lyrion Music Server (LMS) can be installed on piCorePlayer via the [LMS] page on the piCorePlayer web interface.

{{< card style="info" >}}The default version of LMS will be 7.9.3. If you would like to move to 8.0.0. for example---see [Upgrade Lyrion Music Server](/how-to/upgrade_lms/){{< /card >}}

{{< card style="warning" >}}
Lyrion Music Server (LMS) is a server database application and needs to be shutdown properly:
- Do **not** just pull the power plug.
- Use [Main Page] > [Shutdown].{{< /card >}}


## More information

- [Logitech Music Server (LMS)](https://lyrion.org)
- [Upgrade Lyrion Music Server](/how-to/upgrade_lms/)
