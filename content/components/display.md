---
title: Display
description: Raspberry Pi 7" Touch Display
date: 2022-07-02T22:48:53.737Z
lastmod: 2022-09-25T21:30:35.870Z
author: pCP Team
weight: 90
pcpver: 8.1.0
toc: true
draft: false
categories:
  - Components
tags:
  - Display
---

{{< lead >}}
piCorePlayer supports the Raspberry Pi 7" Touch Display (and 100% compatibles).
{{< /lead >}}

![Raspberry Pi 7 Inch Display](/components/display.png)

{{< card style="info" >}}
For the other hundreds of displays, it has been left to the community to create the configuration scripts and offer support---see [Tag: display](/tags/display/).
{{< /card >}}


## More information

- [Raspberry Pi 7 Inch Display](https://www.raspberrypi.com/products/)
- [Tag: display](/tags/display/)
