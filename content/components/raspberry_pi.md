---
title: Raspberry Pi
description: null
date: 2024-04-03T02:41:46.852Z
lastmod: 2024-04-03T02:42:31.428Z
author: pCP Team
weight: 50
pcpver: 9.0.0
toc: true
draft: false
categories:
  - Components
tags:
  - RPi
---

{{< lead >}}
The [Raspberry Pi](https://www.raspberrypi.org/) (RPi) is a small and relatively cheap single board computer (SBC). First introduced in 2012, the range has regularly expanded, resulting in many models and options being available. The Raspberry Pi has proven to be very successful, selling millions every year.

This success, combined with [product availability](https://www.raspberrypi.org/products/) and abundant [support](https://www.raspberrypi.org/forums/), makes the Raspberry Pi the hardware platform of choice for piCorePlayer (pCP).
{{< /lead >}}

![Raspberry Pi 5](/components/raspberry_pi_5.jpg)


## Models

{{< rpi-models options="mod mem fre cor ker lan wif hea" >}}

<br>

For the current availability of Raspberry Pi products---see [Raspberry Pi - Products](https://www.raspberrypi.org/products/).

## Recommendation

piCorePlayer will "normally" work on all Raspberry Pi's.

The Raspberry Pi Trading has a policy of maintaining a US$35 price point, so the latest model with the minimum specs will probably be the best value for money.

Although the Raspberry Pi Zeros are the cheapest by far, but they are often in short supply and can be difficult to purchase in some markets. They usually have a purchase quantity restriction of one per order.

### Considerations

The faster the CPU:
- the quicker the pCP web GUI displays.
- the quicker LMS scans the media directories.
- the more power is consumed.
- the more heat is generated.

Other factors to consider:
- Local or LMS format encoding/decoding (ie. DSD256/DSD512).
- Local or LMS re-sampling.
- LMS plugins used.
- Number of players.
- Number of players synchronised.
- Size of music library.
- File transfer speed.
- USB devices.
- 32-bit vs 64-bit.

### Additional Hardware (optional)

- Cases
- Audio HATs
- USB DACs
- Screens

### Power consumption considerations

- See [Raspberry Pi hardware - Power Supply](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#power-supply).


## More information

- [Documentation - Raspberry Pi hardware](https://www.raspberrypi.org/documentation/hardware/raspberrypi/)
- [Raspberry Pi Foundation](https://www.raspberrypi.org/)
- [Raspberry Pi - wikipedia](https://en.wikipedia.org/wiki/Raspberry_Pi)
- [Raspberry Pi - Products](https://www.raspberrypi.org/products/)
- [Raspberry Pi - Forum](https://www.raspberrypi.org/forums/)
- [Raspberry Pi Revision Codes](https://github.com/raspberrypi/documentation/blob/develop/documentation/asciidoc/computers/raspberry-pi/revision-codes.adoc)
