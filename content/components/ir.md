---
title: Infrared
description: Infrared Remote Control
date: 2022-05-01T21:10:09.106Z
lastmod: 2022-09-25T21:30:52.352Z
author: pCP Team
weight: 100
pcpver: 8.1.0
toc: true
draft: false
categories:
  - Components
tags:
  - ir
---

{{< lead >}}
piCorePlayer supports Squeezebox and JustBoom IR remote controls. For other IR remote controls, some extra work is required the create and install the configuration files.

The Linux Kernel has basic IR support for some remote controls. Additionally, there is the `lirc` product.

Squeezelite has a limited number of IR actions builtin.

Jivelite uses IR remote to control cursor movements around the Jivelite screen. 
{{< /lead >}}


## Installation

IR can be installed via the [Tweaks] > "IR remote control" page via the piCorePlayer web interface.


## More information

- [Add an IR receiver](/projects/add-an-ir-receiver/)
- [LIRC RPi gpio IR support for piCorePlayer](https://forums.slimdevices.com/showthread.php?105117-lirc-rpi-gpio-IR-support-for-picoreplayer)
- [tcz-lirc - github](https://github.com/ralph-irving/tcz-lirc)
- [LIRC - Linux Infrared Remote Control](https://www.lirc.org/)
- [Howto create a linux kernel IR remote keytable for jivelite](https://forums.slimdevices.com/showthread.php?110640-Howto-create-a-linux-kernel-IR-remote-keytable-for-Jivelite-on-piCorePlayer-5&highlight=)
- [squeezelite / ir.c - github](https://github.com/ralph-irving/squeezelite/blob/master/ir.c)
- [Tag: ir](/tags/ir/)
