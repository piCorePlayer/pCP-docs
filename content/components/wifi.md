---
title: Wifi
description: null
date: 2022-01-19T22:10:36.819Z
lastmod: 2022-08-02T04:34:31.644Z
author: pCP Team
weight: 80
pcpver: 7.0.0
toc: true
draft: false
categories:
  - Components
tags:
  - wifi
---

{{< lead>}}
piCorePlayer's wifi objective is to have a wifi setup that:
- will work 99% of the time.
- is tolerant of typical user typos and formatting errors.
- has a headless option, so using a monitor and keyboard is not required.
- uses psk passphrases, so passwords are **not** stored as plain text.
- has a **manual** setup option for more advanced users. This means it is theoretical possible that any valid WPA configuration is supported.
{{< /lead>}}


## Three wifi setup methods

piCorePlayer has 3 wifi setup methods:

1. Using the piCorePlayer web GUI.
1. Adding `wpa_supplicant.conf` to the boot partition.
1. Using piCorePlayer setup CLI.


### 1 - piCorePlayer web GUI - the preferred method

To initially access piCorePlayer's [Wifi Settings] web page you will need wired ethernet access.

As the majority of Raspberry Pi's have a wired ethernet option, using `Option 1 - Using the piCorePlayer web GUI` is the preferred method---see [Setup Wifi](/how-to/setup_wifi/).

After inputting your wifi information into the [Wifi Settings] web page and clicking [Save], piCorePlayer will generate a nicely formatted `wpa_supplicant.conf` configuration file for you.

##### Basic format of wpa_supplicant.conf

{{< code >}}# Maintained by piCorePlayer
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=staff
country=CC
update_config=1

network={
        #enc_ssid="yourssid"
        ssid="yourssid"
        psk=6554d2e9bbf47ce37dd95fffc7a6d25d9ef54e477dbd04ed1aa7f724b7e6747f
        key_mgmt=WPA-PSK
        auth_alg=OPEN
}{{< /code >}}


### 2 - Adding wpa_supplicant.conf to the boot partition

It was decided rather than rely on the user to provide a 100% correctly formatted `wpa_supplicant.conf` configuration file, piCorePlayer would extract some of the settings from the existing `wpa_supplicant.conf` configuration file and generate a new, correctly formatted `wpa_supplicant.conf` configuration file.

The basic process:

1. Look for `wpa_supplicant.conf` configuration file on the boot partition.
1. If found, extract the ssid and psk.
1. Create a new `/usr/local/etc/pcp/wpa_supplicant.conf` configuration file.
1. Rename original `wpa_supplicant.conf` configuration file on boot partition to `used_wpa_supplicant.conf` configuration file.

{{< card style="danger" >}}`used_wpa_supplicant.conf` configuration file is left on the boot partition.{{< /card >}}

##### Basic format of wpa_supplicant.conf configuration file

Next, after you boot piCorePlayer your minimal `wpa_supplicant.conf` configuration file will be converted to a working `wpa_supplicant.conf` configuration file formatted like the example below:

{{< code >}}# Maintained by piCorePlayer
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=staff
update_config=1

network={
        #enc_ssid="yourssid"
        ssid="yourssid"
        psk="yourpassword"
        key_mgmt=WPA-PSK
        auth_alg=OPEN
}{{< /code >}}
<br>

Note there are two issues with this `wpa_supplicant.conf` configuration file:
- It still has the password in clear text.
- The country code is missing.


##### Minimal wpa_supplicant.conf configuration file

You can create a minimal `wpa_supplicant.conf` configuration file with just two lines, one line containing your ssid and the other your password. For example:

{{< code >}}ssid="yourssid"
psk="yourpassword"{{< /code >}}


##### GUI formatted wpa_supplicant.conf configuration file

It is advisable to use the [Wifi Settings] web page to complete the process of creating a `wpa_supplicant.conf` configuration file with a passphrase and a country code. The final format looks like this:

{{< code >}}# Maintained by piCorePlayer
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=staff
country=AU
update_config=1

network={
        #enc_ssid="yourssid"
        ssid="yourssid"
        psk=bd9abb43d0e2dd30bd92049f926debe09fc87c8eb18f1bdabe8e17453020bfa6
        key_mgmt=WPA-PSK
        auth_alg=OPEN
}{{< /code >}}


### 3 - Using piCorePlayer setup CLI

Requires a keyboard and monitor---see [Using setup with keyboard and monitor](/how-to/setup_wifi_on_pcp_without_ethernet/#using-setup-with-keyboard-and-monitor).


## Force piCorePlayer to ignore wpa_supplicant.conf configuration file

{{< card style="warning" >}}For advanced users.{{< /card >}}

This wifi setup method is used for complex wifi network setups that:
- have an unusual ssid.
- have an unusual password.
- have multiple networks.
- are corporate networks.

By simply adding `# Maintained by user` to the first line of the `wpa_supplicant.conf` configuration file (it must be exactly `# Maintained by user` including spaces and capitalisation) the `wpa_supplicant.conf` configuration file will be copied from the boot partition to `/usr/local/etc/pcp/` without piCorePlayer checking for, and correcting, errors.

This method requires the user to create the `wpa_supplicant.conf` configuration file 100% correctly otherwise wifi will not connect to the network.

The piCorePlayer GUI will not work with your custom `wpa_supplicant.conf` configuration file.  piCorePlayer setup tools will completely ignore your custom `wpa_supplicant.conf` configuration file.

See [Setup Wifi without ethernet - Maintained by user](/how-to/setup_wifi_on_pcp_without_ethernet/#maintained-by-user),

## Wifi AP

We have decided not to activate a Wifi Access Point (WAP) by default.

## More information

- [Setup Wifi](/how-to/setup_wifi/)
- [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/)
- [Wifi does not work](/faq/wifi_does_not_work/)
- [Example wpa_supplicant configuration file](http://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf)
- [WPA Supplicant](http://w1.fi/wpa_supplicant/)
- [The Short List - Superstar USB WiFi Adapters for Linux](https://github.com/morrownr/USB-WiFi/blob/main/home/The_Short_List.md)
- [Utility for compiling out of tree kernel modules for pCP Kernels](https://github.com/piCorePlayer/pCP-Kernels)
