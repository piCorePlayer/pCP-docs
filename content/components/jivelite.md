---
title: Jivelite
description: null
date: 2024-05-06T11:09:17.592Z
lastmod: 2024-05-06T11:09:21.780Z
author: pCP Team
weight: 40
pcpver: 5.0.0
toc: true
draft: false
categories:
  - Components
tags:
  - Jivelite
  - Display
---

{{< lead >}}
Jivelite is a cut down Squeezebox control application derived from Squeezebox Jive.

- Originally developed and released by Adrian Smith.
- Currently maintained by Ralph Irving.

piCorePlayer fully supports Jivelite on the Raspberry Pi's Official 7" Touch Display.
{{< /lead >}}

## Installation

Jivelite can be installed on piCorePlayer via the [Tweaks] page on the piCorePlayer web interface.


## Jivelite [Settings] > [piCorePlayer] screen


![Jivelite Settings Screen](/components/jivelite_settings.png)


## More information

- [Announce: JiveLite - cut down squeezebox control application - Triode](http://forums.slimdevices.com/showthread.php?98156)
- [Jivelite for piCorePlayer - Ralph Irving](http://forums.slimdevices.com/showthread.php?103330-Jivelite-for-piCorePlayer)
- [Latest Jivelite - Ralph Irving - github](https://github.com/ralph-irving/jivelite)
- [Jivelite for piCorePlayer - Ralph Irving - github](https://github.com/ralph-irving/tcz-jivelite)
