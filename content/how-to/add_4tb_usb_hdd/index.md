---
title: Add a 4TB USB HDD
description: Add a WD Elements SE 4TB hard disk drive
date: 2022-10-13T09:00:18.421Z
lastmod: 2022-11-22T08:45:23.745Z
author: pCP Team
weight: 105
pcpver: 8.2.0
toc: true
draft: false
categories:
  - Setup
  - How to
tags:
  - USB HDD
---

{{< lead >}}
How to add a WD Elements SE 4TB USB HDD (USB drive) to piCorePlayer.
{{< /lead >}}

{{< lead >}}
The object of this "How To" was to grab a standard 4TB USB HDD from the Electronics store and determine what are the steps required to get it mounted on piCorePlayer. To date, I have only be using 1TB and 2TB USB drives which were formatted [NTFS](https://en.wikipedia.org/wiki/NTFS) and used a [MBR](https://en.wikipedia.org/wiki/Master_boot_record). My understanding is for HDD's greater than 2TB, the [MBR](https://en.wikipedia.org/wiki/Master_boot_record) is replaced with a [GPT](https://en.wikipedia.org/wiki/GUID_Partition_Table). piCorePlayer being a minimalist distribution doesn't include support for NTFS or GPT by default.
{{< /lead >}}

![WD HDD](wd_hdd.jpg)


## Hardware

- WD Elements SE 4 TB USB HDD---see [Part#: WDBJRT0040BBK-WESN](https://www.westerndigital.com/en-au/products/portable-drives/wd-elements-se-usb-3-0-hdd#WDBJRT0040BBK-WESN).

I plugged the USB drive into my Windows 10 PC to check the formatting and capacity.

![HDD properties](properties.png)

{{< card style="info" >}}
USB HDD is formatted NTFS.
{{< /card >}}


## Software

- piCorePlayer 8.2.0


## Steps

##### Step 1 - Setup piCorePlayer

- Setup piCorePlayer---see [Getting started](/getting-started/).
- Click the [LMS] tab.
- Check the default LMS settings for future comparison.

![LMS01](lms01.jpg)

##### Step 2 - Shutdown piCorePlayer

- Click the [Main Page] > [Shutdown] button.
- Click the [OK] button when prompted "Shutdown piCorePlayer?".
- Wait for the Raspberry Pi to power down.
- Disconnect the power from the Raspberry Pi.

##### Step 3 - Attach the USB drive

- Plug the USB drive into one of the Raspberry Pi's USB3 ports.

##### Step 4 - Power on piCorePlayer

- Connect the power to the Raspberry Pi.
- Wait for the piCorePlayer to completely boot.

##### Step 5 - Load GPT partition table support

{{< card style="warning" >}}
If your HDD is greater than 2TB it will be using GPT instead of MBR, so GPT support will need to be loaded.
{{< /card >}}

- Click the [LMS] tab.
- Note the message: `Disk with GPT partition table Found! Install extension "util-linux.tcz" for compatibility`.

![LMS02](lms02.jpg)

- Click the [LMS] > "Pick from the following detected USB disks to mount" > [Install Support] button.
- piCorePlayer extensions will load.

##### Step 6 - Load NTFS support

- Note the message: `Please install extra file systems`.

![LMS03](lms03.jpg)

![LMS04](lms04.jpg)

- Click the [LMS] > "Install and Enable additional File Systems" > [Install] button.
- piCorePlayer extensions will load.

![LMS05](lms05.jpg)

- Note the `UUID` is now displayed.
- Type in Mount Point name ie. `music`
- Click on the Enabled checkbox.
- Click the [Set USB Mount] button.

![LMS06](lms06.jpg)

##### Step 7 - Reboot

- Click the [Main Page] > [Reboot] button.

##### Step 8 - Check the USB drive is mounted (optional)

- Click the [Main Page] > [Resize FS] button.
- Note `Disk /dev/sda:` details.

![LMS07](lms07.jpg)

##### Step 9 - Check the USB drive is mounted via ssh (optional)

- Determine your IP address---see [Determine your pCP IP address](/how-to/determine_your_pcp_ip_address/).
- Access piCorePlayer via ssh---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).
- Type `df | grep mnt` at the command prompt.
- Note `/dev/sda1` has been mounted as `/mnt/music`.

![LMS08](lms08.jpg)


## More information

- [WD Elements SE 4TB USB HDD](https://www.westerndigital.com/en-au/products/portable-drives/wd-elements-se-usb-3-0-hdd#WDBJRT0040BBK-WESN)
- [Getting started](/getting-started/)
- [Determine your pCP IP address](/how-to/determine_your_pcp_ip_address/)
- [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/)
