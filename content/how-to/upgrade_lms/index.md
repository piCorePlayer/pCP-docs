---
title: Upgrade LMS
description: Upgrade Lyrion Music Server (LMS)
date: 2024-09-16T09:53:48.551Z
lastmod: 2024-09-16T09:53:50.965Z
author: pCP Team
weight: 90
pcpver: 5.0.0
toc: true
draft: false
categories:
  - Setup
  - How to
tags:
  - LMS
---

{{< lead >}}
It's easy to install Lyrion Music Server on your piCorePlayer using it's settings menus. As of this writing, piCorePlayer is installing `Lyrion Music Server 8.2.0`. This is a release branch that does not get nightly updates. If you want to select `stable bugfix` or `development` branches, you need to follow these instructions.
{{< /lead >}}


## Steps

##### Step 1

Access piCorePlayer via ssh---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).

##### Step 2

\$ `cd /tmp`

##### Step 3

\$ `wget https://raw.githubusercontent.com/piCorePlayer/lms-update-script/Master/lms-update.sh`

##### Step 4

\$ `chmod 755 lms-update.sh`

##### Step 5

To select and install the **current release** branch of LMS.\
\$ `sudo ./lms-update.sh --release release -s -r -u`

**-or-**

To select the stable branch with **nightly bugfix** updates of LMS.\
\$ `sudo ./lms-update.sh --release stable -s -r -u`

**-or-**

To select the current unstable **development** branch with nightly updates.\
\$ `sudo ./lms-update.sh --release devel -s -r -u`


## Copy and paste commands

No waiting, no delay. If you are brave enough, you can cut and paste all the commands in one go. But you will need to press [Enter] to run the final `lms-update.sh` command.

##### Current release

{{< code lang="shell" >}}
cd /tmp
wget https://raw.githubusercontent.com/piCorePlayer/lms-update-script/Master/lms-update.sh
chmod 755 lms-update.sh
sudo ./lms-update.sh --release release -s -r -u
{{< /code >}}

##### Nightly bugfix

{{< code lang="shell" >}}
cd /tmp
wget https://raw.githubusercontent.com/piCorePlayer/lms-update-script/Master/lms-update.sh
chmod 755 lms-update.sh
sudo ./lms-update.sh --release stable -s -r -u
{{< /code >}}

##### Development

{{< code lang="shell" >}}
cd /tmp
wget https://raw.githubusercontent.com/piCorePlayer/lms-update-script/Master/lms-update.sh
chmod 755 lms-update.sh
sudo ./lms-update.sh --release devel -s -r -u
{{< /code >}}


## More information

- [Install LMS](/how-to/install_lms/)
