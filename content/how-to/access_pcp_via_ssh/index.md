---
title: Access pCP via ssh
description: Access piCorePlayer via ssh
date: 2024-04-03T22:58:39.771Z
lastmod: 2024-04-03T22:59:16.478Z
author: pCP Team
weight: 50
pcpver: 9.2.0
toc: true
draft: false
categories:
    - Setup
    - How to
tags:
    - ssh
---

{{< lead >}}
Starting with pCP 9.2.0, piCorePlayer has ssh disabled by default (It was enabled automaticall in prior versions).
If enabled, you can access piCorePlayer from any computer on the network with an ssh client.
{{< /lead >}}

## From the command line

### Steps

##### Step 1

- Type `ssh user@<IP address>` at the command prompt.

	\$ `ssh tc@192.168.1.111`

##### Step 2

- The first time you access the remote computer, type `yes` to establish host authenticity.
```text
The authenticity of host '<IP address> (<IP address>)' can't be established.
ECDSA key fingerprint is SHA256:Oj3eagEfJYeltdxRbmNsVmqPDF4SO4m7KmVl8+3KB5A.
Are you sure you want to continue connecting (yes/no)?
```
- Are you sure you want to continue connecting (yes/no)? `yes`

##### Step 3

- Type piCorePlayer's password when prompted.

    tc@\<IP address\>'s password: `<password>`

{{< card style="info" >}}
    This password is setup during the first connection to the pCP web pages.  Prior to pCP 9.2.0, the default password is `piCore`.
	This should be changed on the Security page from the Main web page.
{{< /card >}}

##### Step 4

- Type `exit` to close terminal.


## Windows PuTTY

### Steps

##### Step 1

- Start your ssh client software.

![PuTTY screen](putty.png)

##### Step 2

- Enter your **IP address**.

##### Step 3

- Set **Port** to `22`.

##### Step 4

- Set **Connection type** to `SSH`.

##### Step 5

- Click [Save] button.

##### Step 6

- Click [Open] button.

##### Step 7

- Login into your piCorePlayer.

	login as: `tc`<br>
	password: `<password>`

![piCore login terminal](putty_login.png)

##### Step 8

- Type `exit` or click `[X]` to close terminal.


## Enable ssh

- [Main Page] in [Beta] mode > [Security] > [Disable SSH].
- pCP 9: [Main Page] > [Security] > [SSH].

## More information

- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Determine your pCP IP address](/how-to/determine_your_pcp_ip_address/)
