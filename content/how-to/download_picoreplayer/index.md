---
title: Download pCP
description: Download piCorePlayer
date: 2024-06-24T10:36:35.404Z
lastmod: 2024-06-24T10:37:14.846Z
author: pCP Team
weight: 10
pcpver: 8.2.0
draft: false
categories:
  - Setup
  - How to
tags:
  - Download
---

{{< card style="info" >}}
The download process can vary depending on the operating system and web browser used on your computer.
{{< /card >}}


## Steps

##### Step 1

- Go to the piCorePlayer Downloads web page---go to [Downloads](/downloads/).

##### Step 2

- Click on the `piCorePlayer X.X.X - 32-bit Kernel` link to begin download.

##### Step 3 (optional)

- Wait for the download to begin---depends on browser.

##### Step 4 (optional)

- Click on the [Save] or [Save as] button if prompted---depends on browser.

##### Step 5

- Wait for the download to complete---about 20 or 30 seconds depending on your internet speed.

##### Step 6

- Go to your local download directory.

##### Step 7

- Unzip the piCorePlayer image.

{{< card style="warning" >}}
Some SD card creators can work directly with compressed files but the additional md5 checksum file may cause issues.
{{< /card >}}


## More information

- The download process can vary depending on operating system and web browser used.
- The downloaded zip file contains 2 files:
    - `piCorePlayerx.x.x.img`---this is the piCorePlayer image.
    - `piCorePlayerx.x.x.img.md5.txt`---this is the image's md5 checksum file.
    - Some operating systems hide the `.img` and `.txt` file extensions.
