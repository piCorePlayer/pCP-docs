---
title: Forgot SSH password
description: Forgot SSH Password
date: 2024-06-07T22:58:39.771Z
lastmod: 2024-06-07T22:59:16.478Z
author: pCP Team
weight: 53
pcpver: 9.2.0
toc: true
draft: false
categories:
    - Setup
    - How to
tags:
    - ssh
    - password
---

{{< lead >}}
If you forget the password for user `tc`. This proceedure is how you can reset the password.
{{< /lead >}}

{{< card style="info" >}}
You must have physical access to the device.  Without physical access, there is no way to set a new password.
{{< /card >}}
### Steps

##### Step 1

- Connect a keyboard and monitor to your Rasbperry Pi running pCP.

##### Step 2

- Power up the device and wait for boot to finish.
- When you see `Press [Enter] to access console.  Press the [Enter] key on your keyboard.

##### Step 3

- Type the following: `sudo passwd tc`
- Enter your new password when prompted.

##### Step 4

- Type `pcp bu` to backup your new password setting.
