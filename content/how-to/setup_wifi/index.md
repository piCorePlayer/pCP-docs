---
title: Setup Wifi
description: null
date: 2022-05-25T23:34:05.190Z
lastmod: 2022-09-24T06:14:01.045Z
author: SBP
weight: 60
pcpver: 7.0.0
toc: true
draft: false
categories:
  - Setup
  - How to
tags:
  - Wifi
---

{{< lead >}}
This how-to uses the piCorePlayer GUI to create a "standard" Wifi setup. To access the piCorePlayer GUI, it needs to be connected to the LAN using wired ethernet.
{{< /lead >}}

{{< card style="warning" >}}
If you have added `# Maintained by user` to the first line of your wpa_supplicant.conf file, the piCorePlayer GUI will completely ignore your configuration file. It can only be maintained by using a text editor.
{{< /card >}}


## Prerequisites

1. piCorePlayer has already been successfully setup.
1. piCorePlayer is connected to the LAN using wired ethernet.
1. The Wifi adapter is supported by piCorePlayer.
1. wpa_supplicant.conf does **not** exist.
1. wpa_supplicant.conf does **not** have `# Maintained by user` at the beginning of the first line.

{{< card style="warning" >}}
wpa_supplicant.conf will be overwritten each time you press [Save] on the [Wifi Settings] page.
{{< /card >}}


## Steps

##### Step 1 - Insert USB Wifi adapter (optional)

- If you are **not** using the built-in Wifi adapter, insert the USB Wifi adapter.

##### Step 2 - Go to [Wifi Settings] page

- Click on [Wifi Settings].

##### Step 3 - Turn Wifi on

- Select "Set Wifi configuration" > Wifi > "On" - (1).
- Click [Save] - (2).

![Initial setup](wifi1.jpg)

This will refresh the [Wifi Settings] page with all the wifi options enabled.

##### Step 4 - Get SSID

- Click "Wifi information" > [Scan] to scan for available networks.

![Scan for network](wifi2.jpg)

- Note the SSID of your Wifi network (or highlight with mouse and copy (ctrl-c)).

![Select Wifi](wifi3.jpg)

##### Step 5 - Fill in the "Set Wifi configuration" fields

- Type your "SSID" - (1).
- Type your "PSK Password" - (2).
- Type your "Country Code"---see [Country Code](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2). - (3).
- Click [Save] to save your Wifi settings - (4).

![Wifi settings](wifi4.jpg)

{{< card style="info" >}}Your "PSK Password" will be converted to a secure "PSK Passphrase". piCorePlayer does not save your password as plain text anywhere.{{< /card >}}

![Wifi settings](wifi5.jpg)

##### Step 6 - Connected to Wifi network

Confirm piCorePlayer has connected to your wifi network.

- Check "Wifi information" > "Wifi IP" for the IP address being used.

![Wifi IP address](wifi6.jpg)

##### Step 7 - Remove ethernet cable

- Click [Main Page].
- Click [Shutdown].
- Remove the wired ethernet cable.
- Apply power to boot.
- When piCorePlayer has booted, you should be connected to your Wifi network.

{{< card style="warning" >}}
When piCorePlayer changes from a wired network to a wifi network, a different MAC address will be used, resulting in an additional player instance being created by LMS. Also, as the network device has changed, your DHCP server will allocate a new IP address to the wifi device. This IP address **will be different** from the original wired network device's IP address.
{{< /card >}}

##### Step 8 - Reset Jivelite (optional)

{{< card style="warning" >}}
If you have previously setup Jivelite with a different network device, you will need to reset Jivelite, then setup Jivelite again on the current network device.
{{< /card >}}

- [Reset] Jivelite on the GUI [Tweaks] page.
- Setup Jivelite on the Jivelite screen.
- Click [Main Page] > [Backup].


## More information

- [Setup Wifi without ethernet](/how-to/setup_wifi_on_pcp_without_ethernet/)
- [Wifi does not work](/faq/wifi_does_not_work/)
- [Example wpa_supplicant configuration file](http://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf)
- [WPA Supplicant](http://w1.fi/wpa_supplicant/)
- [The Short List - Superstar USB WiFi Adapters for Linux](https://github.com/morrownr/USB-WiFi/blob/main/home/The_Short_List.md)
- [Utility for compiling out of tree kernel modules for pCP Kernels](https://github.com/piCorePlayer/pCP-Kernels)
