---
title: Edit config.txt or cmdline.txt
description: null
date: 2024-05-07T03:02:38.199Z
lastmod: 2024-05-07T03:07:09.710Z
author: pCP Team
weight: 40
pcpver: 9.0.0
toc: true
draft: false
categories:
  - Setup
  - How to
tags:
  - config.txt
  - cmdline.txt
---

{{< lead >}}
The `config.txt` or `cmdline.txt` is found on piCorePlayer's boot partition, which normally is `/mnt/mmcblk0p1`---see [The config.txt file](https://www.raspberrypi.com/documentation/computers/config_txt.html) or [The kernel command line](https://www.raspberrypi.com/documentation/computers/configuration.html#the-kernel-command-line).
{{< /lead >}}

{{< card style="warning" >}}
The boot partition is unmounted by default. So to edit `config.txt` or `cmdline.txt`, you must first mount the boot partition, then edit using `vi`.
{{< /card >}}

{{< card style="warning" >}}
You must reboot piCorePlayer after changing `config.txt` or `cmdline.txt` to activate changes.
{{< /card >}}


## Longhand method—using standard Linux commands

### Steps

##### Step 1 - Access via ssh

- Access piCorePlayer via ssh---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).

##### Step 2 - Mount boot partition

- $ `mount /mnt/mmcblk0p1`

##### Step 3 - Change directory to boot partition

- $ `cd /mnt/mmcblk0p1`

##### Step 4 - Edit config.txt or cmdline.txt

- $ `vi config.txt`

**-or-**

- $ `vi cmdline.txt`

##### Step 5 - Reboot

- $ `reboot`


## Shorthand method—using pCP aliases

### Steps

##### Step 1 - Access via ssh

- Access piCorePlayer via ssh---see [Access piCorePlayer via ssh](/how-to/access_pcp_via_ssh/).

##### Step 2 - Mount boot partition

- $ `m1`

##### Step 3 - Change directory to boot partition

- $ `c1`

##### Step 4 - Edit config.txt or cmdline.txt

- $ `vicfg`

**-or-**

- $ `vicmd`

##### Step 5 - Reboot

- $ `pcp rb`


## More information

- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [The config.txt file](https://www.raspberrypi.com/documentation/computers/config_txt.html)
- [The kernel command line](https://www.raspberrypi.com/documentation/computers/configuration.html#the-kernel-command-line)
