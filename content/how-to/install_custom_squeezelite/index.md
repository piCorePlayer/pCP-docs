---
title: Install a custom Squeezelite
description: Install a custom Squeezelite
date: 2024-06-23T23:27:55.865Z
lastmod: 2024-06-24T02:54:44.601Z
author: CJS
weight: 150
pcpver: 9.0.0
toc: true
draft: false
categories:
    - How to
tags:
    - squeezelite
---

{{< lead >}}
If you want to install a (newer) version of squeezelite on pCP that is not available in the repository of the pCP version that you run, this can be done as follows: These instructions are for people who are using a Windows PC to access pCP that is running on their RPi.
{{< /lead >}}

### Steps

##### Step 1 - Download

Download the version of squeezelite that you want to use from https://sourceforge.net/projects/lmsclients/files/squeezelite/linux/.

- For the 32-bit version of pCP download file `squeezelite-2.0.0.xxxx-armhf.tar.gz`
- For the 64-bit version of pCP download file `squeezelite-2.0.0.xxxx-aarch64.tar.gz`

##### Step 2 - Extract binary

Extract the squeezelite binary from the downloaded file.

- On Windows you can use `7-zip` to do this.

##### Step 3 - Rename file

Rename `squeezelite` to `squeezelite-custom`

##### Step 4 - Copy file to pCP

Use WinSCP to copy binary `squeezelite-custom` to folder `/mnt/mmcblk0p2/tce on pCP`.

##### Step 5 - Change permissions

Change permissions of file `squeezelite-custom` to `775`. This can be done by right-clicking the file in WinSCP, and selecting Properties. Then make the file readable and executable for owner, group and others.

Refer to screenshot below.

![Squeezelite Custom](squeezelite-custom1.png)

##### Step 6 - Open web browser

Open the pCP web interface in a web browser, and select page [Squeezelite Settings].

##### Step 7 - Set binary to custom Squeezelite

On the bottom of that page in section "Set Squeezelite Binary", select "Custom Squeezelite", and click "Set Binary".

![Squeezelite Custom](squeezelite-custom2.png)

##### Step 8 - Restart Squeezelite

Squeezelite will restart, and you have your custom squeezelite running. pCP will add a link called squeezelite to binary `squeezelite-custom` in folder `/mnt/mmcblk0p2/tce`. You don't need to add this link yourself. See screenshot made in an SSH session below. To get the new squeezelite version visible in the pCP footer, you need to reboot pCP.

![Squeezelite Custom](squeezelite-custom3.png)

##### Step 9 - Test

Test that your custom squeezelite is working.

##### Step 10

Have fun.
