---
title: USBImager
description: null
date: 2023-06-11T22:23:07.736Z
lastmod: 2023-06-11T22:23:16.051Z
author: pCP Team
weight: 10
pcpver: 8.0.0
toc: true
draft: false
hidden: true
categories:
  - Setup
  - How to
tags:
  - SD card
---

{{< lead >}}
[USBImager](https://gitlab.com/bztsrc/usbimager#usbimager) is a really simple GUI application that writes disk images to SD cards or USB drives. It also can be used to create SD card backups. Available platforms: Windows, MacOSX and Linux. It's interface is as simple as it gets, totally bloat-free.
{{< /lead >}}

## Steps

##### Step 1 - Insert SD card

- Insert SD card into PC.

##### Step 2 - Ignore all warnings

{{< card style="danger" >}}
Depending on software loaded on your PC, you may be get a series of messages when you plug in your SD card.
- **Ignore** Autoplay requests.
- **Ignore** formatting requests.
- **Ignore** anti-virus warnings.
- **Ignore** backups requests.
{{< /card >}}

- Click [Cancel] if prompted to format disk.

![Windows Format Drive](usbimager_format_disk.png)

##### Step 3 - Note the new drive letter

- Note the new drive letter that has been created (ie. D:).

##### Step 4 - Start USBImager

- Click [USBImager] button, icon or menu item.

![USBImager - Blank](usbimager_blank.png)

##### Step 5 - Select pCP Image

- Select pCP Image File (ie. piCorePlayer6.1.0.img).

![USBImager - Image File](usbimager_image_file.png)

##### Step 6 - Set Device

- Carefully set Device to your SD card.
  - **For example:** D: [8.4 GiB] VID:03 SL08G (PCP_BOOT).
  - **Note:** the PCP_BOOT partition label if the SD card is being reused. A new SD card will have a blank label.

{{< card style="danger" >}}
Ensure you have your SD card selected. You can accidentally overwrite your USB drive!
{{< /card >}}

![USBImager - Device](usbimager_device.png)

##### Step 7 - Write data on "Image file" to "Device"

{{< card style="warning" >}}
Ensure Target Device is correct.
{{< /card >}}

{{< card style="warning" >}}
Ensure Target Device label is correct.
{{< /card >}}

- Write data on "Image file" to "Device".
- Click [Write].

![USBImager - Write](usbimager_write.png)

##### Step 8 - Writing in progress

- Writing in progress.

![USBImager - In Progress](usbimager_in_progress.png)

##### Step 9 - Writing

- Writing should take from 4 to 24 seconds.
  - Exact time will depend on SD card specification.

![USBImager - Complete](usbimager_complete.png)

##### Step 10 - Exit

- Click \[X\] to exit program.

![USBImager - Exit](usbimager_exit.png)

##### Step 11 - Remove SD card

- Click [Safely Remove Hardware and Eject Media].
- Click [Eject SD Card].

![USBImager - Eject SD card](usbimager_eject_sd_card.png)


## More information

- [USBImager - GitLab](https://gitlab.com/bztsrc/usbimager#usbimager)
