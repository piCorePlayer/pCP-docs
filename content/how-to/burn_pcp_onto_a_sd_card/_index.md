---
title: Burn pCP onto a SD card
description: Burn piCorePlayer onto a SD card
date: '2021-09-06T11:02:43.942Z'
author: pCP Team
weight: 20
pcpver: 8.0.0
toc: true
draft: false
categories:
    - Setup
    - How to
tags:
    - SD card
lastmod: '2021-09-06T11:12:03.677Z'
---

{{< lead >}}
There are many methods of creating a piCorePlayer SD card.
{{< /lead >}}

## For Windows

You can use your favourite method or use one of the following:

- [USBImager](/how-to/burn_pcp_onto_a_sd_card/windows/usbimager/)
- [Win32 Disk Imager](/how-to/burn_pcp_onto_a_sd_card/windows/win32diskimager/)
- [Raspberry Pi Imager](https://www.raspberrypi.org/blog/raspberry-pi-imager-imaging-utility/)

## For Linux

You can use your favourite method or use one of the following:

- [USBImager - GitLab](https://gitlab.com/bztsrc/usbimager#usbimager)
- [Using dd command](/how-to/burn_pcp_onto_a_sd_card/linux/dd/)
- [Raspberry Pi Imager](https://www.raspberrypi.org/blog/raspberry-pi-imager-imaging-utility/)

## For Mac OSX

You can use your favourite method or use one of the following:

- [USBImager - GitLab](https://gitlab.com/bztsrc/usbimager#usbimager)
- [ApplePi-Baker](https://www.tweaking4all.com/hardware/raspberry-pi/applepi-baker-v2/)
- [Raspberry Pi Imager](https://www.raspberrypi.org/blog/raspberry-pi-imager-imaging-utility/)


## More information

- [RPF - Installing Operating System Images](https://www.raspberrypi.org/documentation/installation/installing-images/)
- [eLinux - RPi Easy SD Card Setup](http://elinux.org/RPi_Easy_SD_Card_Setup)
- [Win32 Disk Imager - Sourceforge](https://sourceforge.net/projects/win32diskimager/)
- [USBImager - GitLab](https://gitlab.com/bztsrc/usbimager#usbimager)
- [ApplePi-Baker](https://www.tweaking4all.com/hardware/raspberry-pi/applepi-baker-v2/)
- [Raspberry Pi Imager](https://www.raspberrypi.org/blog/raspberry-pi-imager-imaging-utility/)
- [Raspberry Pi Imager - Downloads](https://www.raspberrypi.org/downloads/)
- [SD Card Formatter](https://www.sdcard.org/downloads/formatter_4/)
- [Can\'t see the boot partition](/faq/cant_see_boot_partition/)
- [Can\'t see the root partition](/faq/cant_see_root_partition/)
