---
title: Using dd
description: Using dd command
date: 2020-11-07
author: pCP Team
weight: 30
pcpver: "6.1.0"
toc: true
draft: false
hidden: true
categories:
- Setup
- How to
tags:
- SD card
---

##### Step 1
 
{{< card style="danger" >}} Make sure you know what your doing here. `dd` can write to any device and wipe out your hard disk in a second!{{< /card >}}

Depending on your system, finding the card device can use different tools.

Use `lsblk` or `blkid` to identify your device. Typically `/dev/sd?` or `/dev/mmcblk?`.

Run the command before and after inserting SD card.

##### Step 2

Using `lsblk`, type:

$ `lsblk`

![dd - Find SD card](dd_find_sd_card.png)

In this case, the card is located at `/dev/sdf`.

##### Step 3

Using `blkid` on piCorePlayer, type:

$ `blkid`

![dd - Find SD card](dd_blkid_find_sd_card.png)

In this case, the card is located at `/dev/sdb`.

##### Step 4

Unmount all partitions of the SD card device.

Type the following commands:

$ `sudo umount /dev/sdf1`

$ `sudo umount /dev/sdf2`

##### Step 5

Write the new image using this command.

$ `sudo dd if=piCorePlayer6.1.0.img of=/dev/sdf bs=1M`

![dd - Write SD card](dd_write_sd_card.png)

##### Step 6

Remove SD card. Image is ready to use.


## More information

- [RPF - Installing Operating System Images](https://www.raspberrypi.org/documentation/installation/installing-images/)
- [eLinux - RPi Easy SD Card Setup](http://elinux.org/RPi_Easy_SD_Card_Setup)
