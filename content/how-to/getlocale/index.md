---
title: getlocale.sh
description: null
date: 2022-12-15T11:44:46.260Z
lastmod: 2023-01-22T05:45:20.784Z
author: pCP Team
weight: 1040
pcpver: 8.2.0
toc: true
draft: true
categories:
  - locale
  - getlocale
tags:
  - config.txt
---

{{< lead >}}
To avoid having one huge locale support extension, this script builds a customized one according to your selections.
- If you load this in the console, the script is called getlocale.sh.
- The new extension will be called mylocale.tcz.
{{< /lead >}}


### Steps

##### Step 1

- Click [Main Page] > [Extensions] > [Available].
- Select `getlocale.tcz` from "Available extensions in the piCorePlayer main repository".
- Click [Install].

{{< card style="warning" >}}
This will download the `getlocale.sh` script and numerous dependant extensions.


-----getlocale.tcz
     |-----glibc_gconv.tcz
     |-----glibc_i18n_locale.tcz
     |-----glibc_apps.tcz
     |     |-----bash.tcz
     |     |     |-----ncurses.tcz
     |-----dialog.tcz
     |     |-----ncurses.tcz
     |-----squashfs-tools.tcz
     |     |-----liblzma.tcz
     |     |-----lzo.tcz
     |     |-----zstd.tcz
     |     |     |-----liblzma.tcz
     |     |     |-----liblz4.tcz


{{< /card >}}


##### Step 2

- [Reboot]

##### Step 3



##### Step 4



##### Step 5






## More information

- [piCorePlayer aliases](/information/pcp_aliases/)
- [piCorePlayer CLI](/information/pcp_cli/)
- [Basic vi commands](https://www.cs.colostate.edu/helpdocs/vi.html)
- [The config.txt file](https://www.raspberrypi.com/documentation/computers/config_txt.html)
