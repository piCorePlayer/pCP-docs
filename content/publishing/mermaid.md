---
title: Mermaid
description: null
date: 2022-01-19T22:05:42.737Z
lastmod: 2022-09-25T21:24:32.792Z
author: pCP Team
weight: 90
pcpver: 7.0.0
toc: true
draft: false
categories:
  - Projects
tags:
  - Mermaid
---

{{< lead >}}
Mermaid example.
{{< /lead >}}

## Network Layout

<div class="mermaid">
	graph TD
		I[Internet] ==>          WR([Wifi/Router])
		WR          ==>          X[Switch]
		X           ==>          P1[pCP Player 1]
		P1          ---          P1S[Touch Screen]
		X           ==>          D[pCP LMS]
		D           ---     |CD| H[(USB HDD)]
		X           ==>          PC1[PC W10]
		PC1         ---    |CD2| PC1H[(USB HDD)]
		PC1         ---    |CD1| PC2H[(USB HDD)]
		WR          ===>  |WiFi| PC2[Ripper W10]
		PC2         ---    |CD1| PC2H[(USB HDD)]
		PC2         ---  |CDnew| PC3H[(INT HDD)]
		WR          ===>  |WiFi| P2[pCP Player 2]
		P2          -->    |USB| DAC[DAC]
		WR          ===>  |WiFi| P3[pCP Player 3]
		P3          ---   |HDMI| TV[TV Screen]
		WR          ===>  |WiFi| P4[pCP Player 4]
		P4          ---   |HDMI| TV[TV Screen]
</div>

## Mermaid markup

```text
<div class="mermaid">
	graph TD
		I[Internet] ==>          WR([Wifi/Router])
		WR          ==>          X[Switch]
		X           ==>          P1[pCP Player 1]
		P1          ---          P1S[Touch Screen]
		X           ==>          D[pCP LMS]
		D           ---     |CD| H[(USB HDD)]
		X           ==>          PC1[PC W10]
		PC1         ---    |CD2| PC1H[(USB HDD)]
		PC1         ---    |CD1| PC2H[(USB HDD)]
		WR          ===>  |WiFi| PC2[Ripper W10]
		PC2         ---    |CD1| PC2H[(USB HDD)]
		PC2         ---  |CDnew| PC3H[(INT HDD)]
		WR          ===>  |WiFi| P2[pCP Player 2]
		P2          -->    |USB| DAC[DAC]
		WR          ===>  |WiFi| P3[pCP Player 3]
		P3          ---   |HDMI| TV[TV Screen]
		WR          ===>  |WiFi| P4[pCP Player 4]
		P4          ---   |HDMI| TV[TV Screen]
</div>
```


## More information

- [Mermaid](https://mermaid-js.github.io/mermaid/#/)
