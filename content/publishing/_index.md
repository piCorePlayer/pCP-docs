---
title: Publishing
description: null
date: 2022-09-23T05:21:38.751Z
lastmod: 2022-09-23T05:22:37.856Z
author: pCP Team
weight: 65
pcpver: 6.1.0
toc: false
draft: false
hidden: false
---

## Publishing Tools
<div class="row py-3 mb-5">
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="h2 bi bi-speedometer2 text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Hugo
				</h5>
				<p class="card-text text-muted">
					Static files generated in a second. Local staging for testing. Source submitted to GitLab gets automatically pushed to production using Hugo.
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="h2 bi bi-markdown-fill text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Markdown
				</h5>
				<p class="card-text text-muted">
					Just enough formatting while remaining simple. User can submit markdown or text files.
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="h2 bi bi-git text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Git
				</h5>
				<p class="card-text text-muted">
					Source managed by git. There's no better solution.
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="h2 bi bi-bootstrap-fill text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Bootstrap
				</h5>
				<p class="card-text text-muted">
					Fully responsive using Bootstrap 4. Works on small screens.
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="h2 bi bi-file-earmark-fill text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					ACE documentation theme
				</h5>
				<p class="card-text text-muted">
					A great Hugo theme using Bootstrap 4. Ideal starting point for creating documentation.
				</p>
			</div>
		</div>
	</div>
</div>
{{< childpages >}}
