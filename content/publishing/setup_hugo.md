---
title: Setup Hugo
description: null
date: 2022-10-13T08:02:17.111Z
lastmod: 2022-10-13T08:07:15.361Z
author: pCP Team
weight: 50
pcpver: 8.2.0
toc: true
draft: false
categories:
  - Publishing
tags:
  - Hugo
---

{{< lead >}}
Instructions for setting up Hugo {{< hugo-version >}} for Windows 10. Similar process for Linux.
{{< /lead >}}


## Hugo setup - Windows 10

See [Installing Hugo](https://gohugo.io/getting-started/installing).

## Steps

##### Step 1 - Create directory structure

```txt
C:.
└── Hugo
    ├── bin
    └── pCP-docs
```

\$ `cd \`

\$ `mkdir Hugo`

\$ `cd Hugo`

\$ `mkdir bin`

\$ `mkdir pCP-docs` 


##### Step 2 - Add c:\Hugo\bin to $PATH

Edit the system environment variables.

Click [Environment Variables].

##### Step 3 - Download Hugo

Download the appropriate Hugo executable---see [Download Hugo](https://github.com/gohugoio/hugo/releases/).

ie. hugo_extended_0.103.1_windows-amd64.zip

##### Step 4 - Copy hugo.exe to c:\Hugo\bin\

\$ `copy hugo.exe c:\Hugo\bin\`

##### Step 5 - Clone documentation repository from git

Clone from https://gitlab.com/piCorePlayer/pCP-docs.git to `c:\Hugo\pCP-docs\`


## More information

- [Installing Hugo](https://gohugo.io/getting-started/installing/)
- [Download Hugo](https://github.com/gohugoio/hugo/releases/)
- [piCorePlayer Documentation source](https://gitlab.com/piCorePlayer/)
