---
title: HTML to Markdown
description: Convert HTML to Markdown
date: 2022-07-11T22:18:28.254Z
lastmod: 2022-09-25T21:22:12.623Z
author: pCP Team
weight: 60
pcpver: 6.1.0
toc: true
draft: false
categories:
  - Publishing
tags:
  - HTML
  - Markdown
---

{{< lead >}}
The piCorePlayer Documentation source is mostly written in Markdown. So sometimes it is necessary to convert existing HTML pages into Markdown using an online service for inclusion in the documentation.
{{< /lead >}}

## Steps

##### Step 1 - Copy HTML source

- In the web page, select `View source` (`ctrl+u` in Edge)
- Copy the HTML code by:
	- Clicking on the HTML window to set focus.
	- Highlight all source (`ctrl+a`).
	- Copy highlighted source (`ctrl+c`).

##### Step 2 - Convert HTML to Markdown

- Open Turndown---click [Turndown - HTML to Markdown](https://mixmark-io.github.io/turndown/).
	- Set `Heading style` to `atx`.
	- Set `Bullet` to `-`.
	- Set `Code block style` to `fenced`.
- Paste source in left HTML window (`ctrl+v`).
- Copy result from right MARKDOWN window (`ctrl+a`, `ctrl+c`).

##### Step 3 - Tidy up Markdown

- Paste into text editor (`ctrl+c`).
- Remove head and trailing rubbish.


 ## More information

- [Turndown - HTML to Markdown](https://github.com/mixmark-io/turndown)
